from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

from re import match

import mock3

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.results'] = load_source('avi.results', '../avi/results.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')
modules['avi.data_source'] = load_source('avi.data_source', '../avi/data_source.py')
modules['avi.views'] = load_source('avi.urls', '../avi/views.py')
modules['avi.urls'] = load_source('avi.urls', '../avi/urls.py')

from avi.urls import urlpatterns

from avi.views import index
from avi.views import searchAsteroids
from avi.views import computationParameters
from avi.views import ownComputations
from avi.views import sharedComputations
from avi.views import results
from avi.views import dashboard
from avi.views import viz2d
from avi.views import share
from avi.views import download
from avi.views import reveal
from avi.views import viz3d
from avi.views import viz3d_solution


class TestUrls(TestCase):

    def test_url_prefix(self):
        self.assertTrue(urlpatterns[0] == '')

    def test_pattern_delimiters(self):
        for ptrn in urlpatterns[1:]:
            self.assertTrue(ptrn[0][0] == '^')
            self.assertTrue(ptrn[0][-1] == '$')

    def test_patterns(self):
        patterns = [
            ('', index),
            ('search-asteroids', searchAsteroids),
            ('computation-parameters', computationParameters),
            ('own-computations', ownComputations),
            ('shared-computations', sharedComputations),
            ('results', results),
            ('dashboard', dashboard),
            ('jobs/d34db33f/viz2d/ais', viz2d),
            ('jobs/d34db33f/viz2d/aes', viz2d),
            ('jobs/d34db33f/viz2d/acs', viz2d),
            ('jobs/d34db33f/share', share),
            ('jobs/d34db33f/download', download),
            ('jobs/d34db33f/reveal', reveal),
            ('jobs/d34db33f/viz3d/', viz3d),
            ('jobs/d34db33f/viz3d/solutions/42', viz3d_solution),
        ]
        for ptrn, handler in patterns:
            for urlptrn in urlpatterns[1:]:
                if match(urlptrn[0], ptrn):
                    self.assertTrue(urlptrn[1] == handler)



if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()

