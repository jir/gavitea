from sys import modules

def _plainfunc(cls):
    def _wrap(m):
        return cls.__dict__[m.__name__]

class gavip_avi(object):
    class decorators(object):
        @staticmethod
        def require_gavip_role(f):
            def _wrap(f):
                return f
            return _wrap

modules['gavip_avi'] = gavip_avi
modules['gavip_avi.decorators'] = gavip_avi.decorators


class django(object):
    class core(object):
        class serializers(object):
            pass
    class conf(object):
        class settings(object):
            OUTPUT_PATH = 'DJANGO_OUTPUT_PATH'
            class GAVIP_ROLES(object):
                OP = 'OP'
                SC = 'SC'
                OR = 'OR'
                DV = 'DV'

        class urls(object):
            @staticmethod
            def patterns(*args, **kwargs):
                return args
            @staticmethod
            def url(*args, **kwargs):
                return args

    class db(object):
        class models(object):    
            class BigIntegerField(object):
                def __init__(self, *args, **kwargs):
                    pass
            class BooleanField(object):
                def __init__(self, *args, **kwargs):
                    pass
            class CharField(object):
                def __init__(self, *args, **kwargs):
                    pass
            class FloatField(object):
                def __init__(self, *args, **kwargs):
                    pass
            class Model(object):
                def __init__(self, *args, **kwargs):
                    pass
            class TextField(object):
                def __init__(self, *args, **kwargs):
                    pass

    class http(object):
        class HttpResponse(object):
            pass
        class Http404(BaseException):
            def __init__(self, *args, **kwargs):
                pass

    class shortcuts(object):
        def get_object_or_404(*args, **kwargs):
            return args, kwargs
        def render(*args, **kwargs):
            return args, kwargs

    class views(object):
        class decorators(object):
            class http(object):
                @staticmethod
                def require_http_methods(*args, **kwargs):
                    def _wrap(f):
                        return f
                    return _wrap

modules['django'] = django
modules['django.core'] = django.core
modules['django.views'] = django.views
modules['django.views.decorators'] = django.views.decorators
modules['django.views.decorators.http'] = django.views.decorators.http
modules['django.shortcuts'] = django.shortcuts
modules['django.http'] = django.http
modules['django.db'] = django.db
modules['django.db.models'] = django.db.models
modules['django.conf'] = django.conf
modules['django.conf.urls'] = django.conf.urls
    



    

class pipeline(object):
    class classes(object):    
        class AviTask(object):
            @staticmethod
            def event_handler(f):
                def _wrap(f):
                    return f
                return _wrap
        class AviLocalTarget(object):
            pass
        class AviParameter(object):
            pass
    class models(object):
        class AviJob(object):
            class objects(object):
                def all(self):
                    return [1, 2, 3] #!!
            class request(object):
                class created(object):
                    @staticmethod
                    def strftime(*args):
                        return args[0]
                class pipeline_state(object):
                    @staticmethod
                    def get_percentage_complete():
                        return 50.0
                    state = 'PIPELINE_STATE'
        class AviJobRequest(object):
            pass
        class PipeState(object):
            pass

modules['pipeline'] = pipeline
modules['pipeline.classes'] = pipeline.classes
modules['pipeline.models'] = pipeline.models


class plugins(object):
    class util(object):
        def generate_dataproduct_share_link(*args, **kwargs):
            return args, kwargs
        def generate_userspace_download_link(*args, **kwargs):
            return args, kwargs
        def generate_userspace_view_link(*args, **kwargs):
            return args, kwargs

modules['plugins'] = plugins
modules['plugins.util'] = plugins.util

class rest_framework(object):
    class response(object):
        class Response(object):
            pass
    class status(object):
        HTTP_201_CREATED = 'HTTP_201_CREATED'
        HTTP_204_NO_CONTENT = 'HTTP_204_NO_CONTENT'
        HTTP_400_BAD_REQUEST = 'HTTP_400_BAD_REQUEST'
        HTTP_404_NOT_FOUND = 'HTTP_404_NOT_FOUND'
    class views(object):
        class APIView(object):
            @staticmethod
            def as_view(*args, **kwargs):
                return args, kwargs

modules['rest_framework'] = rest_framework
modules['rest_framework.response'] = rest_framework.response
modules['rest_framework.status'] = rest_framework.status
modules['rest_framework.views'] = rest_framework.views

class luigi(object):
    class event(object):
        class Event(object):
            FAILURE = 'FAILURE'

modules['luigi'] = luigi
modules['luigi.event'] = luigi.event


class astropy(object):
    class io(object):
        class ascii(object):
            def write(*args, **kwargs):
                pass
        class votable(object):
            def from_table(*args, **kwargs):
                pass
            def writeto(*args, **kwargs):
                pass
    class table(object):
        class Table(object):
            pass

modules['astropy'] = astropy
modules['astropy.io'] = astropy.io
modules['astropy.io.ascii'] = astropy.io.ascii
modules['astropy.io.votable'] = astropy.io.votable
modules['astropy.table'] = astropy.table


class bokeh(object):
    class resources(object):
        INLINE = 'INLINE'
    class plotting(object):
        class figure(object):
            pass
    class io(object):
        @staticmethod
        def output_file(*args, **kwargs):
            return args
        def save(*args, **kwargs):
            return args
    class models(object):
        class ColumnDataSource(object):
            pass
        class Legend(object):
            pass
        class OpenURL(object):
            pass
        class TapTool(object):
            pass
        class CustomJS(object):
            pass
        class tools(object):
            class HelpTool(object):
                pass
            class ResetTool(object):
                pass
        class widgets(object):
            class Tabs(object):
                pass
            class Panel(object):
                pass
            class CheckboxGroup(object):
                pass
            class Button(object):
                pass
            class tables(object):
                class TableColumn(object):
                    pass
                class StringFormatter(object):
                    pass
                class DataTable(object):
                    pass
    class layouts(object):
        #widgetbox,column,row,gridplot
        def widgetbox():
            pass
        def column():
            pass
        def row():
            pass
        def gridplot():
            pass

modules['bokeh'] = bokeh
modules['bokeh.resources'] = bokeh.resources
modules['bokeh.plotting'] = bokeh.plotting
modules['bokeh.io'] = bokeh.io
modules['bokeh.models'] = bokeh.models
modules['bokeh.models.tools'] = bokeh.models.tools
modules['bokeh.models.widgets'] = bokeh.models.widgets
modules['bokeh.models.widgets.tables'] = bokeh.models.widgets.tables
modules['bokeh.layouts'] = bokeh.layouts

