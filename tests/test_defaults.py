
from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')

from avi.defaults import ALL_DEFAULTS

class TestDefaults(TestCase):
    def test_all_defaults(self):
        keys = [
            'acs',
            'acsdchi',
            'acslmax',
            'acslmin',
            'acsniter',
            'acsnmc',
            'acsnmcmc',
            'acsntr',
            'acssigmanu',
            'acssigmar',
            'aes',
            'aesamtol',
            'aescar',
            'aesdchi',
            'aesnmc',
            'aesnmcmc',
            'aessigmanu',
            'aesvsc',
            'ais',
            'aisamtol',
            'aiscar',
            'aisnmaxper',
            'aisntrper',
            'aisntrpole',
            'aispmax',
            'aispmin',
            'aisscper',
            'astdsg',
            'astname',
            'astno',
            'base_md5',
            'csv',
            'search',
            'vot',
        ]
        for key in keys:
            self.assertTrue(key in ALL_DEFAULTS)



if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()

