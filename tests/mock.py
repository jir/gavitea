from sys import modules

class django(object):
    class conf(object):
        class settings(object):
            pass
        class urls(object):
            def patterns(*args, **kwargs):
                return args, kwargs
            def url(*args, **kwargs):
                return args, kwargs

    class db(object):
        class models(object):    
            class BigIntegerField(object):
                pass
            class BooleanField(object):
                pass
            class CharField(object):
                pass
            class FloatField(object):
                pass
            class Model(object):
                pass
            class TextField(object):
                pass

    class http(object):
        class HttpResponse(object):
            pass
        class Http404(object):
            pass

    class shortcuts(object):
        def get_object_or_404(*args, **kwargs):
            return args, kwargs
        def render(*args, **kwargs):
            return args, kwargs

    class views(object):
        class decorators(object):
            class http(object):
                def require_http_methods(*args, **kwargs):
                    return args, kwargs
                    

modules['django'] = django
modules['django.views'] = django.views
modules['django.views.decorators'] = django.views.decorators
modules['django.views.decorators.http'] = django.views.decorators.http
modules['django.shortcuts'] = django.shortcuts
modules['django.http'] = django.http
modules['django.db'] = django.db
modules['django.db.models'] = django.db.models
modules['django.conf'] = django.conf
modules['django.conf.urls'] = django.conf.urls
    



    

class pipeline(object):
    class classes(object):    
        class AviTask(object):
            pass
        class AviLocalTarget(object):
            pass
        class AviParameter(object):
            pass
    class models(object):
        class AviJob(object):
            pass
        class AviJobRequest(object):
            pass
        class PipeState(object):
            pass

modules['pipeline'] = pipeline
modules['pipeline.classes'] = pipeline.classes
modules['pipeline.models'] = pipeline.models


class plugins(object):
    class util(object):
        def generate_dataproduct_share_link(*args, **kwargs):
            return args, kwargs
        def generate_userspace_download_link(*args, **kwargs):
            return args, kwargs
        def generate_userspace_view_link(*args, **kwargs):
            return args, kwargs

modules['plugins'] = plugins
modules['plugins.util'] = plugins.util

class rest_framework(object):
    class response(object):
        class Response(object):
            pass
    class status(object):
        HTTP_201_CREATED = 'HTTP_201_CREATED'
        HTTP_204_NO_CONTENT = 'HTTP_204_NO_CONTENT'
        HTTP_400_BAD_REQUEST = 'HTTP_400_BAD_REQUEST'
        HTTP_404_NOT_FOUND = 'HTTP_404_NOT_FOUND'
    class views(object):
        class APIView(object):
            pass

modules['rest_framework'] = rest_framework
modules['rest_framework.response'] = rest_framework.response
modules['rest_framework.status'] = rest_framework.status
modules['rest_framework.views'] = rest_framework.views

class luigi(object):
    class event(object):
        class Event(object):
            pass

modules['luigi'] = luigi
modules['luigi.event'] = luigi.event

