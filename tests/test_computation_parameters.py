from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3


modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')

from avi.defaults import ALL_DEFAULTS, AIS_DEFAULTS, AES_DEFAULTS, ACS_DEFAULTS
from avi.defaults import get_lower_limits
from avi.defaults import get_upper_limits

from avi.models import validate

ALGORITHM_DEFAULTS = {}
ALGORITHM_DEFAULTS.update(AIS_DEFAULTS)
ALGORITHM_DEFAULTS.update(AES_DEFAULTS)
ALGORITHM_DEFAULTS.update(ACS_DEFAULTS)

LOWER_LIMITS = {}
LOWER_LIMITS.update(get_lower_limits(1))

UPPER_LIMITS = {}
UPPER_LIMITS.update(get_upper_limits())


class TestComputationParameters(TestCase):

    def test_defaults(self):
        parameters = ALL_DEFAULTS.copy()
        self.assertTrue(validate(parameters)['is_valid'])

    def test_lower_limits(self):
        parameters = ALL_DEFAULTS.copy()
        parameters.update(get_lower_limits(1))
        self.assertTrue(validate(parameters)['is_valid'])

    def test_upper_limits(self):
        parameters = ALL_DEFAULTS.copy()
        parameters.update(get_upper_limits())
        self.assertTrue(validate(parameters)['is_valid'])

    def test_under_lower_limits(self):
        for p in LOWER_LIMITS:
            parameters = ALL_DEFAULTS.copy()
            parameters[p] = LOWER_LIMITS[p] - 1
            self.assertFalse(validate(parameters)['is_valid'])

    def test_over_upper_limits(self):
        for p in UPPER_LIMITS:
            parameters = ALL_DEFAULTS.copy()
            parameters[p] = UPPER_LIMITS[p] + 1
            self.assertFalse(validate(parameters)['is_valid'])

    def test_invalid_input(self):
        for p in ALGORITHM_DEFAULTS:
            if p in 'ais aes acs':
                continue  # any input will anyway be a valid boolean
            parameters = ALL_DEFAULTS.copy()
            parameters[p] = '!"#$%&&/()=' # non-numeric string
            self.assertFalse(validate(parameters)['is_valid'])

if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()


