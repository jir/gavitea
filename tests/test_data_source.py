from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3

# data source is currently a mock version, and
# its real requirements remain unknown so far;
# hence, no tests are provided

if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()
