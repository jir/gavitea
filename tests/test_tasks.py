from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3 

from os.path import normpath
from os.path import join

# mock relevant constant values
AVI_PARAMETER = 'AVI_PARAMETER'
OUTPUT_PATH = 'OUTPUT_PATH'
AVI_ROOT = 'AVI_ROOT'
MOCK_PATH = 'MOCK_PATH'
AIS_BIN = 'AIS_BIN'
AES_BIN = 'AES_BIN'
ACS_BIN = 'ACS_BIN'
FAILURE = 'FAILURE'



# mock importing
#modules['luigi'] = luigi
#modules['luigi.event'] = luigi.event
#modules['pipeline'] = pipeline
#modules['pipeline.classes'] = pipeline.classes

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')
modules['avi.data_source'] = load_source('avi.data_source', '../avi/data_source.py')
modules['avi.data_preprocessing'] = load_source('avi.data_preprocessing', '../avi/data_preprocessing.py')
modules['avi.visualization'] = load_source('avi.visualization', '../avi/visualization.py')
modules['avi.tasks'] = load_source('avi.tasks', '../avi/tasks.py')



# import avi.tasks into fixture
from avi.tasks import Task


class TestTask(TestCase):
    def setUp(self):
        self.task = Task()

    def test_interface(self):
        self.assertTrue(type(self.task.md5) == str)
        self.assertTrue(callable(self.task._cwd))
        self.assertTrue(callable(self.task._do))
        self.assertTrue(callable(self.task._dependencies))
        self.assertTrue(callable(self.task.requires))
        self.assertTrue(callable(self.task.run))
        self.assertTrue(callable(self.task.output))

    def test_cwd(self):
        self.assertTrue(self.task._cwd('foo', 'bar') == 'OUTPUT_PATH/AVI_PARAMETER/foo/bar')
        self.assertTrue(self.task._cwd('foo/..', 'bar') == 'OUTPUT_PATH/AVI_PARAMETER/bar')

    def test_requires(self):
        self.assertTrue(False) # not implemented

    def test_run(self):
        self.assertTrue(False) # not implemented

    def test_output(self):
        self.assertTrue(self.task.output() == 'OUTPUT_PATH/AVI_PARAMETER/.Task')


class TestRun(TestCase): pass
class TestMaybeCSV(TestCase): pass
class TestMaybeVOTable(TestCase): pass
class TestMaybeAIS(TestCase): pass
class TestMaybeAES(TestCase): pass
class TestMaybeACS(TestCase): pass
class TestMaybeReuse(TestCase): pass
class TestCSV(TestCase): pass
class TestVOTable(TestCase): pass
class TestAIS(TestCase): pass
class TestAES(TestCase): pass
class TestACS(TestCase): pass
class TestReuse(TestCase): pass
class TestFresh(TestCase): pass
class TestPlotAIS(TestCase): pass
class TestPlotAES(TestCase): pass
class TestPlotACS(TestCase): pass
class TestComputeAIS(TestCase): pass
class TestComputeAES(TestCase): pass
class TestComputeACS(TestCase): pass
class TestCreateAISInputParametersTxtFile(TestCase): pass
class TestCreateAESInputParametersTxtFile(TestCase): pass
class TestCreateACSInputParametersTxtFile(TestCase): pass
class TestPreprocessInputData(TestCase): pass
class TestRetrieveInputData(TestCase): pass
class TestCopyBfDatFile(TestCase): pass

class TestInitialize(TestCase): pass

if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()
