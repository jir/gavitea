
from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')

from avi.models import Job

'''#
from os.path import normpath
from os.path import join
# mock relevant constant values
AVI_PARAMETER = 'AVI_PARAMETER'
OUTPUT_PATH = 'OUTPUT_PATH'
AVI_ROOT = 'AVI_ROOT'
MOCK_PATH = 'MOCK_PATH'
AIS_BIN = 'AIS_BIN'
AES_BIN = 'AES_BIN'
ACS_BIN = 'ACS_BIN'
FAILURE = 'FAILURE'
#'''


AIS_PER_PROGRESS_PATH = ''
AIS_POLE_PROGRESS_PATH = ''
AES_IS_PROGRESS_PATH = ''
AES_LSV_PROGRESS_PATH = ''
AES_MCMC_PROGRESS_PATH = ''
ACS_IS_PROGRESS_PATH = ''
ACS_LSV_PROGRESS_PATH = ''
ACS_MCMC_PROGRESS_PATH = ''


class TestModels(TestCase):

    def test_get_algorithm_stage_progress(self):
        self.assertTrue(False)

    def test_validate_one(self):
        self.assertTrue(False)

    def test_validate(self):
        self.assertTrue(False)

    def test_Job(self):
        job = Job()
        self.assertTrue(hasattr(job, 'pipeline_task'))
        self.assertTrue(hasattr(job, 'md5'))
        self.assertTrue(hasattr(job, 'base_md5'))
        self.assertTrue(hasattr(job, 'timestamp'))
        self.assertTrue(hasattr(job, 'search'))
        self.assertTrue(hasattr(job, 'astno'))
        self.assertTrue(hasattr(job, 'astdsg'))
        self.assertTrue(hasattr(job, 'astname'))
        self.assertTrue(hasattr(job, 'ais'))
        self.assertTrue(hasattr(job, 'aispmin'))
        self.assertTrue(hasattr(job, 'aispmax'))
        self.assertTrue(hasattr(job, 'aisntrper'))
        self.assertTrue(hasattr(job, 'aisntrpole'))
        self.assertTrue(hasattr(job, 'aisamtol'))
        self.assertTrue(hasattr(job, 'aisscper'))
        self.assertTrue(hasattr(job, 'aisnmaxper'))
        self.assertTrue(hasattr(job, 'aiscar'))
        self.assertTrue(hasattr(job, 'aes'))
        self.assertTrue(hasattr(job, 'aescar'))
        self.assertTrue(hasattr(job, 'aesamtol'))
        self.assertTrue(hasattr(job, 'aesnmc'))
        self.assertTrue(hasattr(job, 'aessigmanu'))
        self.assertTrue(hasattr(job, 'aesnmcmc'))
        self.assertTrue(hasattr(job, 'aesdchi'))
        self.assertTrue(hasattr(job, 'aesvsc'))
        self.assertTrue(hasattr(job, 'acs'))
        self.assertTrue(hasattr(job, 'acsnmc'))
        self.assertTrue(hasattr(job, 'acsnmcmc'))
        self.assertTrue(hasattr(job, 'acslmin'))
        self.assertTrue(hasattr(job, 'acslmax'))
        self.assertTrue(hasattr(job, 'acsntr'))
        self.assertTrue(hasattr(job, 'acsniter'))
        self.assertTrue(hasattr(job, 'acssigmanu'))
        self.assertTrue(hasattr(job, 'acsdchi'))
        self.assertTrue(hasattr(job, 'acssigmar'))
        self.assertTrue(hasattr(job, 'csv'))
        self.assertTrue(hasattr(job, 'vot'))

    def test_Job_get_progress(self):
        job = Job()
        self.assertTrue(False)

    def test_Job_get_path(self):
        job = Job()
        job.md5 = 'mockmd5'
        job_path = job.get_path()
        self.assertTrue(path(OUTPUT_PATH, job.md5))
        self.assertTrue(False)




if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()



