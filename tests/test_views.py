from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3

from django.http import Http404

modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')
modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')
modules['avi.data_source'] = load_source('avi.data_source', '../avi/data_source.py')
modules['avi.results'] = load_source('avi.results', '../avi/results.py')

modules['avi.defaults'] = load_source('avi.defaults', '../avi/defaults.py')
modules['avi.models'] = load_source('avi.models', '../avi/models.py')
modules['avi.views'] = load_source('avi.views', '../avi/views.py')

from avi.views import _resolve_results
from avi.views import _job_mapper
from avi.views import _job_cmp
from avi.views import _normalize
from avi.views import _shared_job_mapper

from avi.models import Job

from avi.defaults import ALL_DEFAULTS
from avi.defaults import TIMESTAMP_FORMAT

class TestViews(TestCase):
    def test_resolve_result(self):
        md5 = 'nosuchmd5'
        with self.assertRaises(Http404):
            _resolve_results(md5)

    def test_normalize(self):
        for key1 in ALL_DEFAULTS:
            unnormalized = ALL_DEFAULTS.copy()
            unnormalized[key1] = '!"#$%&/()=' # unparseable string
            normalized = _normalize(unnormalized)
            for key2 in normalized:
                if (key1 == key2):
                    self.assertTrue(normalized[key2] == None or True) # bool(<any string>) is always True
                else:
                    self.assertTrue(type(normalized[key2]) == type(ALL_DEFAULTS[key2]))

    def test_job_cmp(self):
        data = [
            {'x': 1},
            {'x': 3},
            {'x': -1},
            {'x': 2},
        ]
        ascending = _job_cmp('x', 'ascending')
        descending = _job_cmp('x', 'descending')
        unknown = _job_cmp('x', 'nosuchorder')

        self.assertTrue(sorted(data, ascending) == [
            {'x': -1},
            {'x': 1},
            {'x': 2},
            {'x': 3},
        ])

        self.assertTrue(sorted(data, descending) == [
            {'x': 3},
            {'x': 2},
            {'x': 1},
            {'x': -1},
        ])

        self.assertTrue(sorted(data, unknown) == [
            {'x': -1},
            {'x': 1},
            {'x': 2},
            {'x': 3},
        ])

    def test_job_mapper(self):
        job = Job()
        job.md5 = 'JOB_MD5'
        job.astno = 'JOB_ASTNO'
        job.astname = 'JOB_ASTNAME'
        job.astdsg = 'JOB_ASTDSG'

        self.assertTrue(_job_mapper(job) == {
            'md5': 'JOB_MD5',
            'timestamp': TIMESTAMP_FORMAT, # testing a correct format spec is passed
            'astno': 'JOB_ASTNO',
            'astname': 'JOB_ASTNAME',
            'astdsg': 'JOB_ASTDSG',
            'progress': 50,
            'status': 'PIPELINE_STATE',
        })


    def test_job_paging(self):
        pass

    def test_shared_job_mapper(self):
        metadata = {
            'md5': "metadata['md5']",
            'timestamp': "metadata['timestamp']",
            'astno': "metadata['astno']",
            'astname': "metadata['astname']",
            'astdsg': "metadata['astdsg']",
            'ais': "metadata['ais']",
            'aes': "metadata['aes']",
            'acs': "metadata['acs']",
            'nosuchkey': "metadata['acs']",
        }
        mapped = _shared_job_mapper(metadata)
        self.assertTrue('md5' in mapped)
        self.assertTrue('timestamp' in mapped)
        self.assertTrue('astno' in mapped)
        self.assertTrue('astname' in mapped)
        self.assertTrue('astdsg' in mapped)
        self.assertTrue('ais' in mapped)
        self.assertTrue('aes' in mapped)
        self.assertTrue('acs' in mapped)
        self.assertFalse('nosuchkey' in mapped)


    def test_shared_job_paging(self):
        pass

    def test_viz3d_mapper(self):
        pass

    def test_viz3d_cmp(self):
        pass

    def test_viz3d_paging(self):
        pass


if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()


