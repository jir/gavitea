
from logging import getLogger
logger = getLogger(__name__)


from sys import modules
from imp import load_source
from unittest import TestCase
from unittest import main

import mock3


modules['avi'] = load_source('avi', '../avi/__init__.py')
modules['avi.utilities'] = load_source('avi.utilities', '../avi/utilities.py')
modules['avi.settings'] = load_source('avi.settings', '../avi/settings.py')


class TestSettings(TestCase):

    def test_version(self):
        self.assertTrue(False)
    def test_fw_version(self):
        self.assertTrue(False)

    def test_output_path(self):
        self.assertTrue(False)
    def test_avi_root(self):
        self.assertTrue(False)
    def test_ais_bin(self):
        self.assertTrue(False)
    def test_aes_bin(self):
        self.assertTrue(False)
    def test_acs_bin(self):
        self.assertTrue(False)

    def test_ais_per_progress(self):
        self.assertTrue(False)
    def test_ais_pole_progress(self):
        self.assertTrue(False)
    def test_aes_is_progress(self):
        self.assertTrue(False)
    def test_aes_lsv_progress(self):
        self.assertTrue(False)
    def test_aes_mcmc_progress(self):
        self.assertTrue(False)
    def test_acs_is_progress(self):
        self.assertTrue(False)
    def test_acs_lsv_progress(self):
        self.assertTrue(False)
    def test_acs_mcmc_progress(self):
        self.assertTrue(False)

    def test_data_url(self):
        self.assertTrue(False)



if __name__ == '__main__':
    logger.info('running {}'.format(__name__))
    main()



