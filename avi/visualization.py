"""
GAVITEA 2D plots
"""

from django.conf import settings

from bokeh.resources import INLINE
from bokeh.plotting import figure
from bokeh.io import output_file,save
from bokeh.models import ColumnDataSource,Legend,OpenURL,TapTool,CustomJS
from bokeh.models.tools import HelpTool,ResetTool
from bokeh.models.widgets import Tabs,Panel,CheckboxGroup,Button
from bokeh.models.widgets.tables import TableColumn,StringFormatter,DataTable
from bokeh.layouts import widgetbox,column,row,gridplot
import itertools
         
from numpy import loadtxt,savetxt,array,shape,min,where,zeros

def create_ais_plot(output_dir):

	# A-IS

	period_data = loadtxt(output_dir+'/ais_PeriodScan.txt',skiprows=1)
	pole_data = loadtxt(output_dir+'/ais_PoleScan.txt',skiprows=1)
	best_fit=loadtxt(output_dir+'/ais_FitAll.txt',skiprows=1)
	output_file(output_dir+"/ais_plot.html")

# For some reason "reset" and "save" tools do not both appear
	tools0 = "box_zoom,reset,save"
	tools1 = "box_zoom,box_select,reset,save"
	plot1 = figure(title="A-IS scanned rotation periods",
               x_axis_label="Rotation period (h)",y_axis_label="root-mean-square",
			   width=380, plot_height=250,
			   tools=tools0)
	plot2 = figure(title="A-IS acanned spin axis directions",
                x_axis_label='Longitude (deg)',y_axis_label='Latitude (deg)',
				x_range=[0,360],y_range=[-90,90],
				width=380, plot_height=250,tools=tools1)
	plot3 = figure(title="A-IS best-fit ellipsoid axis ratios",
                x_axis_label="Axis ratio b/a",y_axis_label="Axis ratio c/b",
				x_range=[0,1],y_range=[0,1],
				width=380, plot_height=250,tools=tools1)
# Link to the user manual output section
	plot1.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="%smanual.html#AISplot" % settings.STATIC_URL))
	plot2.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="%smanual.html#AISplot" % settings.STATIC_URL))
	plot3.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="%smanual.html#AISplot" % settings.STATIC_URL))

# Define symbol colours for spin and shape plots
	rms=pole_data[:,12]
	if max(rms)-min(rms) == 0:
		crms_is=0
	else:
		crms_is=250*(rms-min(rms))/(max(rms)-min(rms))
	col_is=["#%02x%02x%02x" % (int(r),250-abs(2*int(r)-250),250-int(r)) for r in crms_is]

# Define plot for period scanning for A-IS
	p1=plot1.circle(period_data[:,0], period_data[:,12])
# Check for possible divisions by zero. If b axis ==0, replace by -1
	ind=0
	for i in pole_data[:,5]:
		if i==0:
			pole_data[ind,5]=-1
		ind=ind+1
# Define plots for spin and shape from A-IS
	source_is=ColumnDataSource(data=dict(x2=pole_data[:,1],x3=pole_data[:,5],y2=pole_data[:,2],y3=[a / b for a, b in zip(pole_data[:,6], pole_data[:,5])]))
	plot2.circle('x2','y2',source=source_is,fill_color=col_is,line_color=col_is)
	p2bf=plot2.circle_x(best_fit[1],best_fit[2],fill_color="yellow",line_color="indigo")
	plot3.circle('x3','y3',source=source_is,fill_color=col_is,line_color=col_is)
	p3bf=plot3.circle_x(best_fit[5],best_fit[6]/best_fit[5],fill_color="yellow",line_color="indigo")
	p31=plot3.circle(-1,-1,fill_color="blue",line_color="blue")
	p32=plot3.circle(-1,-1,fill_color="red",line_color="red")
	p33=plot3.circle_x(-1,-1,fill_color="yellow",line_color="indigo")
	glyph2=p2bf.glyph
	glyph2.size=15
	glyph3=p3bf.glyph
	glyph3.size=15

	sh_is = shape(pole_data)

	bf_is_ar = where(pole_data[:,sh_is[1]-1] == min(pole_data[:,sh_is[1]-1]))
	bf_row_is=bf_is_ar[0]

	_rms = rms[bf_row_is]


# Add a common legend to spin and shape plots
	legend3 = Legend(legends=[("small rms",[p31]),("large rms",[p32]),("best fit (rms %.2g)" % _rms,[p33]),], location=(0,-30))
	plot3.add_layout(legend3, 'left')

# Write html file for A-IS results
	pall_ais=row(plot1, plot2, plot3)
	save(pall_ais)

def create_aes_plot(output_dir):
# A-ES
	infilefile_es=open(output_dir+"/aes_outf_name.txt",'r')
	infile_es=infilefile_es.readline()
	data_aes=loadtxt(output_dir+"/"+infile_es.strip(),skiprows=1)
	output_file(output_dir+"/aes_plot.html")

	tools1 = "box_zoom,box_select,reset,save"
	
	plot5 = figure(title="A-ES spin axis directions",
                x_axis_label="Longitude (deg)",y_axis_label="Latitude (deg)",
				x_range=[0,360],y_range=[-90,90],
				width=380, plot_height=250,tools=tools1)
	plot6 = figure(title="A-ES ellipsoid axis ratios",
                x_axis_label="Axis ratio b/a",y_axis_label="Axis ratio c/b",
				x_range=[0,1],y_range=[0,1],
				width=380, plot_height=250,tools=tools1)
# Link to the user manual output section
	plot5.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="%smanual.htm#AESCSplot" % settings.STATIC_URL))
	plot6.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="%smanual.htm#AESCSplot" % settings.STATIC_URL))

# Define colouring of points in spin and shape plots
	rms=data_aes[:,12]
	if max(rms)-min(rms) == 0:
		crms_es=0
	else:
		crms_es=250*(rms-min(rms))/(max(rms)-min(rms))
	col_es = ["#%02x%02x%02x" % (int(r),250-abs(2*int(r)-250),250-int(r)) for r in crms_es]

# Check for possible divisions by zero. If b axis ==0, replace by -1
	ind=0
	for i in data_aes[:,5]:
		if i==0:
			data_aes[ind,5]=-1
		ind=ind+1
# Define plotted spin and shape results for A-ES
	source_es=ColumnDataSource(data=dict(x2=data_aes[:,1],x3=data_aes[:,5],y2=data_aes[:,2],y3=[a / b for a, b in zip(data_aes[:,6], data_aes[:,5])]))
	sh_es=shape(data_aes)
	bf_es_ar = where(data_aes[:,sh_es[1]-1] == min(data_aes[:,sh_es[1]-1]))
	bf_row_es=bf_es_ar[0]

	_rms = rms[bf_row_es]
	
	plot5.circle('x2','y2',source=source_es,fill_color=col_es,line_color=col_es)
	plot6.circle('x3','y3',source=source_es,fill_color=col_es,line_color=col_es)
	p5bf=plot5.circle_x(data_aes[bf_row_es[0],1],data_aes[bf_row_es[0],2],source=source_es,fill_color="yellow",line_color="indigo")
	p6bf=plot6.circle_x(data_aes[bf_row_es[0],5],data_aes[bf_row_es[0],6]/data_aes[bf_row_es[0],5],source=source_es,fill_color="yellow",line_color="indigo")
	p61=plot6.circle(-1,-100,fill_color="blue",line_color="blue")
	p62=plot6.circle(-1,-100,fill_color="red",line_color="red")
	p63=plot6.circle_x(-1,-100,fill_color="yellow",line_color="indigo")
	glyph5=p5bf.glyph
	glyph5.size=12
	glyph6=p6bf.glyph
	glyph6.size=12

# Add a common legend showing the color range
	legend6 = Legend(legends=[("small rms",[p61]),("large rms",[p62]),("best fit (rms %.2g)" % _rms,[p63])],location=(0,-30))
	plot6.add_layout(legend6, 'left')

# Define html tabs for A-CS and A-ES plots
	pall_aes=row(plot5, plot6)

	save(pall_aes)
	#save(row(plot5,plot6)) #??

def create_acs_plot(output_dir):

# A-CS

	infilefile_cs=open(output_dir+"/acs_outf_name.txt",'r')
	infile_cs=infilefile_cs.readline()
	data_acs=loadtxt(output_dir+"/"+infile_cs.strip(),skiprows=1)
	output_file(output_dir+"/acs_plot.html")
	
	tools2 = "box_zoom,reset,save"
	plot4=figure(title="A-CS spin axis directions",
             x_axis_label="Longitude (deg)",y_axis_label="Latitude (deg)",
			 x_range=[0,360],y_range=[-90,90],
			 width=380, plot_height=250,tools=tools2)
# Link to the user manual output section
	plot4.add_tools(HelpTool(help_tooltip="More info on the plot",redirect="%smanual.htm#AESCSplot" % settings.STATIC_URL))

# Define colouring of points in the spin plot
	rms=data_acs[:,12]
	if max(rms)-min(rms) == 0:
		crms_cs=0
	else:
		crms_cs=250*(rms-min(rms))/(max(rms)-min(rms))
	col_cs=["#%02x%02x%02x" % (int(r),250-abs(2*int(r)-250),250-int(r)) for r in crms_cs]

# Plot the A-CS spin solution
	x=[]
	for i in range(0,len(data_acs[:,2])):
		x.append(str(i))
	source_cs=ColumnDataSource(data=dict(x2=data_acs[:,1],y2=data_acs[:,2],ind=x))
	sh_cs=shape(data_acs)
	bf_row_ar = where(data_acs[:,sh_cs[1]-1] == min(data_acs[:,sh_cs[1]-1]))
	bf_row=bf_row_ar[0]

	_rms = rms[bf_row]

	plot4.circle('x2','y2',source=source_cs,fill_color=col_cs,line_color=col_cs)
	p4bf=plot4.circle_x(data_acs[bf_row[0],1],data_acs[bf_row[0],2],fill_color="yellow",line_color="indigo")
	p41=plot4.circle(-1,-1,fill_color="blue",line_color="blue")
	p42=plot4.circle(-1,-1,fill_color="red",line_color="red")
	p43=plot4.circle_x(-1,-1,fill_color="yellow",line_color="indigo")
	glyph4=p4bf.glyph
	glyph4.size=12

# Add a common legend to spin and shape plots
	legend4 = Legend(legends=[("small rms",[p41]),("large rms",[p42]),("best fit (rms %.2g)" % _rms,[p43])],location=(0,-30))
	plot4.add_layout(legend4, 'left')

	save(plot4)
	
