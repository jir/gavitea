"""
GAVITEA HTTP endpoints
"""

from logging import getLogger
logger = getLogger(__name__)

from django.conf.urls import patterns, url

from avi.views import index
from avi.views import searchAsteroids
from avi.views import computationParameters
from avi.views import ownComputations
from avi.views import sharedComputations
from avi.views import results
from avi.views import viz2d
from avi.views import viz3d
from avi.views import viz3d_solution
from avi.views import dashboard

from avi.views import share
from avi.views import download
from avi.views import reveal

from avi.views import ListSearchResults
from avi.views import ListOrCreateJobs

from avi.views import RetrieveOrDestroyJob
from avi.views import RetrieveJobProgress
from avi.views import ListViz3DSolutions

urlpatterns = patterns(
    '',
    url(r'^$', index, name='index'),
    url(r'^search-asteroids$', searchAsteroids, name='search-asteroids'),
    url(r'^computation-parameters$', computationParameters, name='computation-parameters'),
    url(r'^own-computations$', ownComputations, name='own-computations'),
    url(r'^shared-computations$', sharedComputations, name='shared-computations'),
    url(r'^results$', results, name='results'),
    url(r'^dashboard$', dashboard, name='dashboard'),

    url(r'^search$', ListSearchResults.as_view(), name='search'),

    url(r'^jobs/$', ListOrCreateJobs.as_view(), name='jobs'),
    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/$', RetrieveOrDestroyJob.as_view(), name='job'),

    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/progress$', RetrieveJobProgress.as_view(), name='job-progress'),
    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/viz2d/(?P<algorithm>ais|aes|acs)$', viz2d, name='job-viz2d'),

    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/share$', share, name='job-share'),
    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/download$', download, name='job-download'),
    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/reveal$', reveal, name='job-reveal'),

    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/viz3d/$', viz3d, name='job-viz3d'),
    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/viz3d/solutions/$', ListViz3DSolutions.as_view(), name='job-viz3d-solutions'),
    url(r'^jobs/(?P<md5>[a-fA-F0-9]+)/viz3d/solutions/(?P<solution>[0-9]+)$', viz3d_solution, name='job-viz3d-solution'),

)

logger.info('{} loaded'.format(__name__))
