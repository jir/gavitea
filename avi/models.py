
"""
Django model for a GAVITEA computation job
"""

from logging import getLogger
logger = getLogger(__name__)

from pipeline.models import AviJob
from django.core import serializers
from django.db.models import Model
from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import FloatField
from django.db.models import BigIntegerField

from avi.defaults import ALL_DEFAULTS
from avi.defaults import get_lower_limits
from avi.defaults import get_upper_limits

from avi.settings import OUTPUT_PATH
from avi.settings import AIS_PER_PROGRESS_PATH
from avi.settings import AIS_POLE_PROGRESS_PATH
from avi.settings import AES_IS_PROGRESS_PATH
from avi.settings import AES_LSV_PROGRESS_PATH
from avi.settings import AES_MCMC_PROGRESS_PATH
from avi.settings import ACS_IS_PROGRESS_PATH
from avi.settings import ACS_LSV_PROGRESS_PATH
from avi.settings import ACS_MCMC_PROGRESS_PATH

from avi.utilities import path, is_file, get_size, load, glob

# helpers

def _get_algorithm_stage_progress(stage_progress_path):
    try:
        with open(stage_progress_path) as f:
            maximum = int(f.readline())
            size = get_size(stage_progress_path) - 13 # first line plus newline size
            return min(size, maximum), maximum
    except:
        return 0, 0

def _validate_one(key, value):

    # check for bad value syntax
    cons = type(ALL_DEFAULTS[key])
    try:
        value = cons(value)
    except Exception as exc:
        return False

    lower_limits = get_lower_limits(1)
    upper_limits = get_upper_limits()

    # check for out of range (lower limit)
    if key in lower_limits:
        if value < lower_limits[key]:
            return False

    # check for out of range (upper limit)
    if key in upper_limits:
        if value > upper_limits[key]:
            return False

    return True

def validate(parameters):
    logger.debug('validating {}', repr(parameters))
    results = {'is_valid': True}
    for key in ALL_DEFAULTS:
        if key in parameters:
            results[key] = _validate_one(key, parameters[key])
            if results[key] == False:
                results['is_valid'] = False
    logger.debug('validation results {}', repr(results))
    return results

class Job(AviJob):

    pipeline_task = 'Run'

    # job bookkeeping
    md5 = CharField(max_length=100)
    base_md5 = CharField(max_length=100)
    timestamp = CharField(max_length=20)

    # data retrieval
    search = BooleanField()
    astno = CharField(max_length=100)
    astdsg = CharField(max_length=100)
    astname = CharField(max_length=100)

    # A-IS
    ais = BooleanField()
    aispmin = FloatField()
    aispmax = FloatField()
    aisntrper = BigIntegerField()
    aisntrpole = BigIntegerField()
    aisamtol = FloatField()
    aisscper = FloatField()
    aisnmaxper = BigIntegerField()
    aiscar = FloatField()

    # A-ES
    aes = BooleanField()
    aescar = FloatField()
    aesamtol = FloatField()
    aesnmc = BigIntegerField()
    aessigmanu = FloatField()
    aesnmcmc = BigIntegerField()
    aesdchi = FloatField()
    aesvsc = FloatField()

    # A-CS
    acs = BooleanField()
    acsnmc = BigIntegerField()
    acsnmcmc = BigIntegerField()
    acslmin = BigIntegerField()
    acslmax = BigIntegerField()
    acsntr = BigIntegerField()
    acsniter = BigIntegerField()
    acssigmanu = FloatField()
    acsdchi = FloatField()
    acssigmar = FloatField()

    # format conversions
    csv = BooleanField()
    vot = BooleanField()

    def get_progress(self):
        base = path(OUTPUT_PATH, self.md5)
        return {
            'aisper': _get_algorithm_stage_progress(path(base, AIS_PER_PROGRESS_PATH)),
            'aispole': _get_algorithm_stage_progress(path(base, AIS_POLE_PROGRESS_PATH)),
            'aesis': _get_algorithm_stage_progress(path(base, AES_IS_PROGRESS_PATH)),
            'aeslsv': _get_algorithm_stage_progress(path(base, AES_LSV_PROGRESS_PATH)),
            'aesmcmc': _get_algorithm_stage_progress(path(base, AES_MCMC_PROGRESS_PATH)),
            'acsis': _get_algorithm_stage_progress(path(base, ACS_IS_PROGRESS_PATH)),
            'acslsv': _get_algorithm_stage_progress(path(base, ACS_LSV_PROGRESS_PATH)),
            'acsmcmc': _get_algorithm_stage_progress(path(base, ACS_MCMC_PROGRESS_PATH)),
            'is-completed': is_file(path(base, '.Run')),
        }

    def get_path(self):
        return path(OUTPUT_PATH, self.md5)

logger.info('{} loaded'.format(__name__))

