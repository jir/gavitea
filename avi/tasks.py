"""
GAVITEA backend computation pipeline
"""

from logging import getLogger
logger = getLogger(__name__)

from textwrap import dedent

from luigi.event import Event

from pipeline.classes import AviTask, AviLocalTarget, AviParameter

from avi.data_source import request_data
from avi.data_preprocessing import preprocess
from avi.utilities import path, save, mkdir, chdir, save_txt, cmd, glob, cp, cpdir
from avi.utilities import txt_to_csv, txt_to_vot, splitext
from avi.visualization import create_ais_plot, create_aes_plot, create_acs_plot

from avi.settings import OUTPUT_PATH, AVI_ROOT, MOCK_PATH
from avi.settings import AIS_BIN, AES_BIN, ACS_BIN

## utility class

class Task(AviTask):
    """ Boilerplate parent class for pipeline tasks """
    md5 = AviParameter()
    
    def _cwd(self, *segments):
        return path(OUTPUT_PATH, self.md5, *segments)
    
    def _do(self):
        pass
    
    def _dependencies(self):
        return None
    
    def requires(self):
        dependencies = self._dependencies()
        if dependencies:
            if hasattr(dependencies, '__iter__'):
                return [self.task_dependency(d) for d in dependencies]
            return self.task_dependency(dependencies)
        return super(Task, self).requires()
    
    def run(self):
        self._do()
        save(self._cwd('.{}'.format(self.__class__.__name__)))
    
    def output(self):
        return AviLocalTarget(self._cwd('.{}'.format(self.__class__.__name__)))

##

class Run(Task):
    
    def _dependencies(self):
        return MaybeCSV, MaybeVOTable, MaybeAIS, MaybeAES, MaybeACS, MaybeReuse   

##

class MaybeCSV(Task):
    csv = AviParameter()
    
    def _dependencies(self):
        if self.csv:
            return CSV
        return BottomOut

class MaybeVOTable(Task):
    vot = AviParameter()
    
    def _dependencies(self):
        if self.vot:
            return VOTable
        return BottomOut

class MaybeAIS(Task):
    ais = AviParameter()
    base_md5 = AviParameter()
    
    def _dependencies(self):
        if not self.base_md5: # require running A-IS if this is a fresh run
            return AIS
        if self.ais:
            return AIS
        return BottomOut

class MaybeAES(Task):
    aes = AviParameter()
    
    def _dependencies(self):
        if self.aes:
            return AES
        return BottomOut

class MaybeACS(Task):
    acs = AviParameter()
    
    def _dependencies(self):
        if self.acs:
            return ACS
        return BottomOut

class MaybeReuse(Task):
    base_md5 = AviParameter()
    
    def _dependencies(self):
        if self.base_md5:
            return Reuse
        return Fresh

###

class CSV(Task):
    
    def _dependencies(self):
        return MaybeAIS, MaybeAES, MaybeACS
    
    def _do(self):
        txt_paths = glob(self._cwd('Output', '*.txt'))
        for txt_path in txt_paths:
            prefix, _ = splitext(txt_path)
            try:
                txt_to_csv(txt_path, prefix + '.csv')
            except:
                pass # silently fail

class VOTable(Task):
    
    def _dependencies(self):
        return MaybeAIS, MaybeAES, MaybeACS
    
    def _do(self):
        txt_paths = glob(self._cwd('Output', '*.txt'))
        for txt_path in txt_paths:
            prefix, _ = splitext(txt_path)
            try:
                txt_to_vot(txt_path, prefix + '.vot')
            except:
                pass # silently fail

class AIS(Task):
    
    def _dependencies(self):
        return PlotAIS

class AES(Task):
    
    def _dependencies(self):
        return PlotAES

class ACS(Task):
    
    def _dependencies(self):
        return PlotACS

class Reuse(Task):
    base_md5 = AviParameter()
    
    def _dependencies(self):
        return BottomOut
    
    def _do(self):
        cpdir(path(OUTPUT_PATH, self.base_md5, 'Input'), self._cwd('Input'))
        cpdir(path(OUTPUT_PATH, self.base_md5, 'Output'), self._cwd('Output'))
        cp(path(AVI_ROOT, 'bf.dat'), self._cwd('bf.dat'))

class Fresh(Task):
    
    def _dependencies(self):
        return PreprocessInputData, CopyBfDatFile

##

class PlotAIS(Task):
    
    def _dependencies(self):
        return ComputeAIS
    
    def _do(self):
        create_ais_plot(self._cwd('Output'))

class PlotAES(Task):
    
    def _dependencies(self):
        return ComputeAES
    
    def _do(self):
        create_aes_plot(self._cwd('Output'))

class PlotACS(Task):
    
    def _dependencies(self):
        return ComputeACS
    
    def _do(self):
        create_acs_plot(self._cwd('Output'))

##

class ComputeAIS(Task):
    
    def _dependencies(self):
        return CreateAISInputParametersTxtFile
    
    def _do(self):
        cmd(AIS_BIN, 'Input', 'Output', cwd=self._cwd())
        cp(self._cwd('Output', 'ais_FitAll.txt'), self._cwd('Input', 'ais_FitAll.txt'))

class ComputeAES(Task):
    
    def _dependencies(self):
        return MaybeAIS, CreateAESInputParametersTxtFile
    
    def _do(self):
        cmd(AES_BIN, 'Input', 'Output', cwd=self._cwd())

class ComputeACS(Task):
    
    def _dependencies(self):
        return MaybeAIS, CreateACSInputParametersTxtFile
    
    def _do(self):
        cmd(ACS_BIN, 'Input', 'Output', cwd=self._cwd())

@ComputeAIS.event_handler(Event.FAILURE)

def report_algorithm_error(task, exception):
    """ Will be called directly after a failed execution """
    logger.info('{} failed with exception {}'.format(task.__class__.__name__, repr(exception)))

@ComputeAES.event_handler(Event.FAILURE)

def report_algorithm_error(task, exception):
    """ Will be called directly after a failed execution """
    logger.info('{} failed with exception {}'.format(task.__class__.__name__, repr(exception)))

@ComputeACS.event_handler(Event.FAILURE)

def report_algorithm_error(task, exception):
    """ Will be called directly after a failed execution """
    logger.info('{} failed with exception {}'.format(task.__class__.__name__, repr(exception)))

##

class CreateAISInputParametersTxtFile(Task):
    """Create A-IS input parameters file"""
    aispmin = AviParameter()
    aispmax = AviParameter()
    aisntrper = AviParameter()
    aisntrpole = AviParameter()
    aisamtol = AviParameter()
    aisscper = AviParameter()
    aisnmaxper = AviParameter()
    aiscar = AviParameter()
    
    def _dependencies(self):
        return MaybeReuse
    
    def _do(self):
        parameters = dedent(
            '''\
            {aispmin}
            {aispmax}
            {aisntrper}
            {aisntrpole}
            {aisamtol}
            {aisscper}
            {aisnmaxper}
            {aiscar}
            '''
        ).format(**self.__dict__)
        save(self._cwd('Input', 'ais_inputParameters.txt'), data=parameters)

class CreateAESInputParametersTxtFile(Task):
    """Create A-ES input parameters file"""
    aescar = AviParameter()
    aesamtol = AviParameter()
    aesnmc = AviParameter()
    aessigmanu = AviParameter()
    aesnmcmc = AviParameter()
    aesdchi = AviParameter()
    aesvsc = AviParameter()

    def _dependencies(self):
        return MaybeReuse
    
    def _do(self):
        parameters = dedent(
            '''\
            {aescar}
            {aesamtol}
            {aesnmc}
            {aessigmanu}
            {aesnmcmc}
            {aesdchi}
            {aesvsc}
            '''
        ).format(**self.__dict__)
        save(self._cwd('Input', 'aes_inputParameters.txt'), data=parameters)

class CreateACSInputParametersTxtFile(Task):
    """Create A-CS input parameters file"""
    acsnmc = AviParameter()
    acsnmcmc = AviParameter()
    acslmin = AviParameter()
    acslmax = AviParameter()
    acsntr = AviParameter()
    acsniter = AviParameter()
    acssigmanu = AviParameter()
    acsdchi = AviParameter()
    acssigmar = AviParameter()

    def _dependencies(self):
        return MaybeReuse
    
    def _do(self):
        parameters = dedent(
            '''\
            {acsnmc}
            {acsnmcmc}
            {acslmin}
            {acslmax}
            {acsntr}
            {acsniter}
            {acssigmanu}
            {acsdchi}
            {acssigmar}
            '''
        ).format(**self.__dict__)
        save(self._cwd('Input', 'acs_inputParameters.txt'), data=parameters)

## initial steps

class PreprocessInputData(Task):
    
    def _dependencies(self):
        return RetrieveInputData
    
    def _do(self):
        preprocess(self._cwd())

class RetrieveInputData(Task):
    astno = AviParameter()
    astname = AviParameter()
    astdsg = AviParameter()
    
    def _dependencies(self):
        return Initialize
    
    def _do(self):
        gaiadata, elements = request_data(self.astno, self.astname, self.astdsg)
        save(self._cwd('Input', 'gaiadata.txt'), data=gaiadata) # just dummy now
        save(self._cwd('Input', 'elements.txt'), data=elements) # ...likewise

class CopyBfDatFile(Task):
    
    def _dependencies(self):
        return Initialize
    
    def _do(self):
        cp(path(AVI_ROOT, 'bf.dat'), self._cwd('bf.dat'))

class Initialize(AviTask):
    """ Initial task for jobs, creates a job workspace """
    md5 = AviParameter()
    
    def _cwd(self, *segments):
        return path(OUTPUT_PATH, self.md5, *segments)
    
    def run(self):
        mkdir(self._cwd('Input'))
        mkdir(self._cwd('Output'))
        save(self._cwd('.{}'.format(self.__class__.__name__)))
    
    def output(self):
        return AviLocalTarget(self._cwd('.{}'.format(self.__class__.__name__)))

class BottomOut(AviTask):
    """ Dummy task for not-taken maybe-tasks """
    
    def output(self):
        return AviLocalTarget('/') #??

logger.info('{} loaded'.format(__name__))

