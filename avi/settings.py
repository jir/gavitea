"""
Various configurable settings for GAVITEA
"""

from logging import getLogger
logger = getLogger(__name__)

from django.conf import settings

from avi.utilities import path
from sys import modules
from os.path import dirname

VERSION = '1.1.1-rc6'
FW_VERSION = '1.0.0'

### paths to file system

OUTPUT_PATH = settings.OUTPUT_PATH
SHARE_PATH = '/data/shared' # hardcoded, currently no GAVIP config variable available
AVI_ROOT = dirname(modules[__name__].__file__)
AIS_BIN = path(AVI_ROOT, 'bin/ais')
AES_BIN = path(AVI_ROOT, 'bin/aes')
ACS_BIN = path(AVI_ROOT, 'bin/acs')

### progress tracking files

AIS_PER_PROGRESS_PATH = 'ais_per_progress.txt'
AIS_POLE_PROGRESS_PATH = 'ais_pole_progress.txt'
AES_IS_PROGRESS_PATH = 'aes_is_progress.txt'
AES_LSV_PROGRESS_PATH = 'aes_lsv_progress.txt'
AES_MCMC_PROGRESS_PATH = 'aes_mcmc_progress.txt'
ACS_IS_PROGRESS_PATH = 'acs_is_progress.txt'
ACS_LSV_PROGRESS_PATH = 'acs_lsv_progress.txt'
ACS_MCMC_PROGRESS_PATH = 'acs_mcmc_progress.txt'

METADATA_PATH = 'metadata.json'

### data source

DATA_URL = "http://gaia.esac.esa.int/tap-server/tap"
USE_MOCK_DATA = True
MOCK_PATH = path(AVI_ROOT, 'mock')

logger.info('{} loaded'.format(__name__))

