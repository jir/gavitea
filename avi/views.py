"""
HTTP request handling
"""

from logging import getLogger
logger = getLogger(__name__)

from hashlib import md5 as hash_md5
from datetime import datetime
from json import loads, dumps

from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from django.shortcuts import render

from django.http import HttpResponse, Http404
from django.conf import settings

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.status import HTTP_404_NOT_FOUND

from pipeline.models import AviJobRequest, PipeState

from avi.utilities import suffix
from avi.utilities import mkdir
from avi.utilities import rmdir
from avi.utilities import path
from avi.utilities import update_existing
from avi.utilities import get_dir_size
from avi.utilities import is_dir
from avi.utilities import glob
from avi.utilities import save
from avi.utilities import load

from avi.data_source import search
from avi.models import Job, validate
from avi.settings import OUTPUT_PATH, SHARE_PATH, VERSION, FW_VERSION, METADATA_PATH

from avi.defaults import DEFAULT_SEARCH_QUERY
from avi.defaults import DEFAULT_SEARCH_ORDER_BY
from avi.defaults import DEFAULT_SEARCH_ORDER
from avi.defaults import DEFAULT_JOBS_QUERY
from avi.defaults import DEFAULT_JOBS_ORDER_BY
from avi.defaults import DEFAULT_JOBS_ORDER
from avi.defaults import DEFAULT_PAGE_NO
from avi.defaults import DEFAULT_PAGE_SIZE
from avi.defaults import ALL_DEFAULTS
from avi.defaults import TIMESTAMP_FORMAT

from avi.results import Results

from plugins.util import generate_userspace_view_link
from plugins.util import generate_userspace_download_link
from plugins.util import generate_dataproduct_share_link

from gavip_avi.decorators import require_gavip_role  # use this to restrict access to views in an AVI
ROLES = settings.GAVIP_ROLES


### helpers
def _resolve_results(md5):
    if is_dir(path(OUTPUT_PATH, md5)):
        return Results(md5)
    if is_dir(path(SHARE_PATH, md5)):
        return Results(md5, shared=True)
    raise Http404('Job not found {}'.format(md5))

def _normalize(raw_parameters):
    """
    Normalize computation job parameters for validation and job creation
    """

    # start with defaults
    parameters = ALL_DEFAULTS.copy()

    # only perform tasks explicitly marked as true
    parameters['ais'] = False
    parameters['aes'] = False
    parameters['acs'] = False
    parameters['vot'] = False
    parameters['csv'] = False
    update_existing(parameters, raw_parameters)

    # parse parameters, obtain appropriate type from ALL_DEFAULTS
    for key in ALL_DEFAULTS:
        try:
            parameters[key] = type(ALL_DEFAULTS[key])(parameters[key])
        except:
            parameters[key] = None
    return parameters

def _job_cmp(order_by, order):
    def _ascending(a, b):
        return cmp(a[order_by], b[order_by])
    def _descending(a, b):
        return -1 * cmp(a[order_by], b[order_by])
    if order == 'descending':
        return _descending
    return _ascending

def _job_mapper(job):
    """
    Convert job list item to a format
    accepted by the HMI.
    """
    pipe_state = job.request.pipeline_state
    context = {}
    context.update({
        'md5': job.md5,
        'timestamp': job.request.created.strftime(TIMESTAMP_FORMAT),
        'astno': job.astno,
        'astname': job.astname,
        'astdsg': job.astdsg,
        'progress': int(pipe_state.get_percentage_complete()),
        'status': pipe_state.state,
    })
    return context

def _job_paging(query, order_by, order, page_no, page_size):
    """
    Extract a result page and paging info from a job queryset
    """
    # get queryset
    queryset = Job.objects.all()
    queryset = map(_job_mapper, queryset)
    queryset.sort(_job_cmp(order_by, order))

    # compute paging info and slice page
    items_total = len(queryset)
    pages_total = (items_total + page_size - 1) // page_size
    page_no = max(min(page_no, pages_total), 1)
    page = queryset[(page_no - 1) * page_size:page_no * page_size]
    return page, page_no, pages_total

def _shared_job_mapper(metadata):
    """
    Convert job list item to a format
    accepted by the HMI

    Essentially just filter properties.
    """
    context = {}
    context.update({
        'md5': metadata['md5'],
        'timestamp': metadata['timestamp'],
        'astno': metadata['astno'],
        'astname': metadata['astname'],
        'astdsg': metadata['astdsg'],
        'ais': metadata['ais'],
        'aes': metadata['aes'],
        'acs': metadata['acs'],
    })
    return context

def _shared_job_paging(query, order_by, order, page_no, page_size):
    """
    Extract a result page and paging info from a job queryset
    """

    # get queryset
    queryset = map(loads, map(load, glob(path(SHARE_PATH, '*', METADATA_PATH))))
    queryset = map(_shared_job_mapper, queryset)
    queryset.sort(_job_cmp(order_by, order))

    # compute paging info and slice page
    items_total = len(queryset)
    pages_total = (items_total + page_size - 1) // page_size
    page_no = max(min(page_no, pages_total), 1)
    page = queryset[(page_no - 1) * page_size:page_no * page_size]
    return page, page_no, pages_total

def _viz3d_mapper(viz3d):
    return viz3d

def _viz3d_cmp(order_by, order):
    def _ascending(a, b):
        return cmp(a[order_by], b[order_by])
    def _descending(a, b):
        return -1 * cmp(a[order_by], b[order_by])
    if order == 'descending':
        return _descending
    return _ascending

def _viz3d_paging(query, order_by, order, page_no, page_size):
    queryset = get_object_or_404(Job, md5=md5).get_viz3d_solutions()
    queryset = map(_viz3d_mapper, queryset)
    queryset.sort(_viz3d_cmp(order_by, order))

    # compute paging info and slice page
    items_total = len(queryset)
    pages_total = (items_total + page_size - 1) // page_size
    page_no = max(min(page_no, pages_total), 1)
    page = queryset[(page_no - 1) * page_size:page_no * page_size]

    return page, page_no, pages_total

### handlers

@require_http_methods(['GET'])
def index(request):
    context = {
        'version': VERSION,
        'fwversion': FW_VERSION,
    }
    return render(request, 'avi/introduction.html', context=context)

@require_http_methods(['GET'])
def searchAsteroids(request):
    return render(request, 'avi/search-asteroids.html')

@require_http_methods(['GET'])
@require_gavip_role([ROLES.OP, ROLES.SC, ROLES.OR, ROLES.DV])
def computationParameters(request):
    context = ALL_DEFAULTS.copy()
    update_existing(context, request.GET)
    return render(request, 'avi/computation-parameters.html', context=context)

@require_http_methods(['GET'])
def ownComputations(request):
    return render(request, 'avi/own-computations.html')

@require_http_methods(['GET'])
def sharedComputations(request):
    return render(request, 'avi/shared-computations.html')

@require_http_methods(['GET'])
def results(request):
    md5 = request.GET.get('md5', '')
    context = ALL_DEFAULTS.copy()
    context['md5'] = md5
    update_existing(context, _resolve_results(md5).retrieve_metadata())
    return render(request, 'avi/results.html', context=context)

@require_http_methods(['GET'])
def viz2d(request, md5, algorithm):
    viz2d = _resolve_results(md5).retrieve_viz2d(algorithm)
    return HttpResponse(viz2d, content_type='text/html')

class ListSearchResults(APIView):

    def get(self, request):

        query = str(request.query_params.get('query', DEFAULT_SEARCH_QUERY))
        order_by = str(request.query_params.get('order-by', DEFAULT_SEARCH_ORDER_BY))
        order = str(request.query_params.get('order', DEFAULT_SEARCH_ORDER))
        page_no = int(request.query_params.get('page-no', DEFAULT_PAGE_NO))
        page_size = int(request.query_params.get('page-size', DEFAULT_PAGE_SIZE))

        page, page_no, pages_total = search(query, order_by, order, page_no, page_size)

        return Response({
            'query': query,
            'order-by': order_by,
            'order': order,
            'page-no': page_no,
            'pages-total': pages_total,
            'page': page,
        })

class ListOrCreateJobs(APIView):

    def get(self, request):
        query = request.query_params.get('query', DEFAULT_JOBS_QUERY)
        order_by = request.query_params.get('order-by', DEFAULT_JOBS_ORDER_BY)
        order = request.query_params.get('order', DEFAULT_JOBS_ORDER)
        page_no = int(request.query_params.get('page-no', DEFAULT_PAGE_NO))
        page_size = int(request.query_params.get('page-size', DEFAULT_PAGE_SIZE))
        page = None

        if query == 'own-computations':
            page, page_no, pages_total = _job_paging(query, order_by, order, page_no, page_size)
        if query == 'shared-computations':
            page, page_no, pages_total = _shared_job_paging(query, order_by, order, page_no, page_size)
        if page == None:
            return Response(status=HTTP_400_BAD_REQUEST)

        return Response({
            'query': query,
            'order-by': order_by,
            'order': order,
            'page-no': page_no,
            'pages-total': pages_total,
            'page': page,
        })

    @require_gavip_role([ROLES.OP, ROLES.SC, ROLES.OR, ROLES.DV])
    def post(self, request):
        # clean up and validate input parameters
        parameters = _normalize(request.POST)
        validation_info = validate(parameters)

        # if just validating, return now
        if request.META['QUERY_STRING'] == 'validation':
            return Response(validation_info)

        # if invalid parameters, return now
        if not validation_info['is_valid']:
            return Response(validation_info)

        # otherwise proceed...

        # job bookkeeping (md5 and JSON metadata)
        timestamp = datetime.utcnow().strftime(TIMESTAMP_FORMAT)
        hash = hash_md5()
        hash.update(timestamp)
        hash.update(dumps(parameters))
        md5 = hash.hexdigest()
        parameters['md5'] = md5
        parameters['timestamp'] = timestamp

        # create and setup job directory
        mkdir(path(OUTPUT_PATH, md5))
        save(path(OUTPUT_PATH, md5, METADATA_PATH), data=dumps(parameters))

        # create job instance
        Job.objects.create(**parameters)

        logger.info('Created a new job: {}'.format(md5))
        return Response(validation_info, status=HTTP_201_CREATED)

class RetrieveOrDestroyJob(APIView):
    
    def get(self, request, md5):
        job_info = _resolve_results(md5).retrieve_metadata()
        logger.info('Retrieved job info for {}'.format(md5))
        return Response(_wrap(job_info))

    def delete(self, request, md5):
        get_object_or_404(Job, md5=md5).delete()
        if is_dir(path(OUTPUT_PATH, md5)):
            rmdir(path(OUTPUT_PATH, md5), True)
        logger.info('Deleted job {}'.format(md5))
        return Response(status=HTTP_204_NO_CONTENT)

class RetrieveJobProgress(APIView):   
    
    def get(self, request, md5):
        progress = get_object_or_404(Job, md5=md5).get_progress()
        logger.info('Retrieved progress for job {}'.format(md5))
        return Response(progress)

# 3D visualization handlers
@require_http_methods(['GET'])
def viz3d(request, md5):
    context = _resolve_results(md5).retrieve_viz3d()
    return render(request, 'avi/3d-viewer.html', context=context)

class ListViz3DSolutions(APIView):
    
    def get(self, request, md5):
        query = str(request.query_params.get('query', DEFAULT_SEARCH_QUERY))
        order_by = str(request.query_params.get('order-by', DEFAULT_SEARCH_ORDER_BY))
        order = str(request.query_params.get('order', DEFAULT_SEARCH_ORDER))
        page_no = int(request.query_params.get('page-no', DEFAULT_PAGE_NO))
        page_size = int(request.query_params.get('page-size', DEFAULT_PAGE_SIZE))

        queryset = _resolve_results(md5).retrieve_viz3d_solutions()
        page = queryset[:50]
        pages_total = 0 #!!
        return Response(page)

@require_http_methods(['GET'])
def viz3d_solution(request, md5, solution):
    context = _resolve_results(md5).retrieve_viz3d_solution(solution)
    return render(request, 'avi/viz3d-solution.html', context=context)

# portal interactions
@require_http_methods(['GET'])
def share(request, md5):
    job = get_object_or_404(Job, md5=md5)
    file_path = job.get_path()
    description = 'GAVITEA results'
    tags = 'gavitea' # TODO add tags
    size = get_dir_size(file_path)
    link = generate_dataproduct_share_link(file_path, description, tags, size)
    return HttpResponse(link, content_type='text/plain')

@require_http_methods(['GET'])
def download(request, md5):
    job = get_object_or_404(Job, md5=md5)
    file_path = job.get_path()
    link = generate_userspace_download_link(file_path)
    return HttpResponse(link, content_type='text/plain')

@require_http_methods(['GET'])
def reveal(request, md5):
    job = get_object_or_404(Job, md5=md5)
    file_path = job.get_path()
    link = generate_userspace_view_link(file_path)
    return HttpResponse(link, content_type='text/plain')

# dashboard
@require_http_methods(['GET'])
def dashboard(request):
    context = {'counts': {}}
    for job in list(Job.objects.all()):
        pipe_state = job.request.pipeline_state
        context['counts'][pipe_state.state] = context['counts'].get(pipe_state.state, 0) + 1
    return render(request, 'avi/dashboard.html', context=context)

logger.info('{} loaded'.format(__name__))
