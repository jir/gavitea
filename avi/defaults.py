"""
Various default values
"""

from logging import getLogger
logger = getLogger(__name__)

# factory defaults for algorithm parameters
# different algorithms have different defaults 

SEARCH_DEFAULTS = {
    'search': True,
    'astno': '0',
    'astname': 'None',
    'astdsg': '0000XX0',
}

AIS_DEFAULTS = {
    'ais': True,
    'aispmin': 4.95,
    'aispmax': 5.05,
    'aisntrper': 2,
    'aisntrpole': 6,    
    'aisamtol': 0.00001,
    'aisscper': 0.5,
    'aisnmaxper': 400,
    'aiscar': 0.4,
}

AES_DEFAULTS = {
    'aes': True,
    'aescar': 0.4,
    'aesamtol': 0.00001,
    'aesnmc': 500,
    'aessigmanu': 0.0001,
    'aesnmcmc': 500,
    'aesdchi': 100.0,
    'aesvsc': 5.0,
}

ACS_DEFAULTS = {
    'acs': True,
    'acsnmc': 300,
    'acsnmcmc': 300,
    'acslmin': 1,
    'acslmax': 5,
    'acsntr': 8,
    'acsniter': 30,
    'acssigmanu': 0.0001,
    'acsdchi': 1000.0,
    'acssigmar': 1.0,
}

OUTPUT_DEFAULTS = {
    'csv': True,
    'vot': True,
}

# collected factory defaults
ALL_DEFAULTS = {
    'base_md5': '',
}
ALL_DEFAULTS.update(SEARCH_DEFAULTS)
ALL_DEFAULTS.update(AIS_DEFAULTS)
ALL_DEFAULTS.update(AES_DEFAULTS)
ALL_DEFAULTS.update(ACS_DEFAULTS)
ALL_DEFAULTS.update(OUTPUT_DEFAULTS)

# upper and lower limits for algorithm
# parameters, currently equal for all algorithms

def get_ais_lower_limits():
    return {
        'aispmin': 0.0,
        'aispmax': 0.0,
        'aisntrper': 1,
        'aisntrpole': 1,

        'aisscper': 0.0,
        'aisamtol': 0.0,
        'aisnmaxper': 1,
        'aiscar': 0.0,
    }

def get_ais_upper_limits():
    return {
        'aisntrper': 8,
        'aisntrpole': 30,
        'aisnmaxper': 3000,
        'aiscar': 1.0,
    }

def get_aes_lower_limits():
    return {
        'aesnmc': 1,
        'aesnmcmc': 1,
        'aesamtol': 0.0,
        'aescar': 0.0,
        'aessigmanu': 0.0,
        'aesdchi': 0.0,
        'aesvsc': 0.0,
    }

def get_aes_upper_limits():
    return {
        'aesnmc': 10000,
        'aesnmcmc': 10000,
        'aescar': 1.0,
    }

def get_acs_lower_limits(lmin):
    return {
        'acsnmc': 1,
        'acsnmcmc': 1,
        'acslmin': 1,
        'acslmax': lmin,
        'acsntr': 0,
        'acssigmanu': 0.0,
        'acsdchi': 0.0,
        'acssigmar': 0.0,
        'acsniter': 5,
    }

def get_acs_upper_limits():
    return {
        'acsnmc': 10000,
        'acsnmcmc': 10000,
        'acslmin': 5,
        'acslmax': 10,
        'acsntr': 30,
    }

# collected limits

def get_lower_limits(lmin):
    lower_limits = {}
    lower_limits.update(get_ais_lower_limits())
    lower_limits.update(get_aes_lower_limits())
    lower_limits.update(get_acs_lower_limits(lmin))
    return lower_limits

def get_upper_limits():
    upper_limits = {}
    upper_limits.update(get_ais_upper_limits())
    upper_limits.update(get_aes_upper_limits())
    upper_limits.update(get_acs_upper_limits())
    return upper_limits

# ordering for listing job states
JOB_STATES_COLLATION = {
    'PENDING': 0,
    'STARTED': 10,
    'SUCCESS': 20,
    'FAILURE': 30,
}

# various query defaults
DEFAULT_SEARCH_QUERY = ''
DEFAULT_SEARCH_ORDER_BY = 'astno'
DEFAULT_SEARCH_ORDER = 'ascending'

DEFAULT_JOBS_QUERY = 'own-computations'
DEFAULT_JOBS_ORDER_BY = 'status'
DEFAULT_JOBS_ORDER = 'ascending'
DEFAULT_PAGE_NO = 1
DEFAULT_PAGE_SIZE = 50

TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M'

logger.info('{} loaded'.format(__name__))
