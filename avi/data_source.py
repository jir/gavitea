"""
Mock data source for GAVITEA
"""

from logging import getLogger
logger = getLogger(__name__)

from random import randint, choice
from string import ascii_lowercase
from avi.settings import USE_MOCK_DATA

def _randdsg(begin, end):
    """Generate random asteroid designation"""
    year = randint(begin, end)
    first_letter = choice(ascii_lowercase)
    second_letter = choice(ascii_lowercase)
    cycle = randint(2, 1000)
    return '{}{}{}{}'.format(year, first_letter, second_letter, cycle)

_MOCK_NAMES = [
    'accumsan', 'adipiscing', 'aenean', 'aliquam',
    'amet', 'ante', 'arcu', 'auctor', 'augue',
    'bibendum', 'blandit', 'commodo', 'condimentum',
    'congue', 'consectetur', 'consequat', 'cras',
    'cubilia', 'curabitur', 'curae', 'cursus',
    'dapibus', 'diam', 'dictum', 'dictumst',
    'dignissim', 'dolor', 'donec', 'efficitur',
    'egestas', 'eget', 'elementum', 'etiam',
    'facilisis', 'faucibus', 'felis', 'fermentum',
    'feugiat', 'finibus', 'fringilla', 'gravida',
    'habitasse', 'hendrerit', 'imperdiet', 'integer',
    'interdum', 'ipsum', 'justo', 'lacus', 'laoreet',
    'lectus', 'leo', 'libero', 'ligula', 'lobortis',
    'lorem', 'luctus', 'magna', 'malesuada', 'massa',
    'mattis', 'mauris', 'maximus', 'metus',
    'molestie', 'mollis', 'morbi', 'neque', 'nibh',
    'nisi', 'non', 'nulla', 'nullam', 'nunc', 'odio',
    'orci', 'ornare', 'pellentesque', 'pharetra',
    'phasellus', 'placerat', 'platea', 'porttitor',
    'posuere', 'praesent', 'pretium', 'primis',
    'proin', 'pulvinar', 'purus', 'quam', 'quis',
    'rhoncus', 'risus', 'rutrum', 'sagittis',
    'sapien', 'scelerisque', 'semper', 'sodales',
    'sollicitudin', 'suspendisse', 'tellus',
    'tempus', 'tincidunt', 'tristique', 'turpis',
    'ullamcorper', 'ultrices', 'urna', 'vehicula',
    'velit', 'venenatis', 'vestibulum', 'vitae',
    'vivamus', 'viverra', 'volutpat', 'vulputate',
]

_MOCK_NUMBERS = [str(randint(10000, 1000000)) for _ in _MOCK_NAMES]
_MOCK_DESIGNATIONS = [str(_randdsg(1990, 2015)) for _ in _MOCK_NAMES]
_MOCK_SEARCH_RESULTS = zip(_MOCK_NUMBERS, _MOCK_NAMES, _MOCK_DESIGNATIONS)

def _matches(prefix, item):
    """Check if mock asteroid name, number or designation matches prefix"""
    return item[0].startswith(prefix) or item[1].startswith(prefix) or item[2].startswith(prefix)

def _compare(order_by, order='ascending'):
    """Comparison function factory for mock asteroid data search results"""

    def _cmp(a, b):
        """Comparison kernel, compares by appropriate property depending on order_by"""
        if order_by == 'astno':
            return cmp(int(a['astno']), int(b['astno']))
        if order_by == 'astname':
            return cmp(a['astname'], b['astname'])
        if order_by == 'astdsg':
            return cmp(a['astdsg'], b['astdsg'])
        return cmp(a, b)

    def _ascending(a, b):
        """Default order (ascending) comparison results"""
        return _cmp(a, b)

    def _descending(a, b):
        """Reverse order (descending) for comparison results"""
        return -1 * _cmp(a, b)

    # determine which comparison function to return
    if order == 'ascending':
        return _ascending
    if order == 'descending':
        return _descending
    return _ascending

def request_data(astno, astname, astdsg):
    """
    Querying mock database for asteroid data

    Currently returns empty data; actual mock data is provided by
    the data preprocessor (avi.data_preprocessing.preprocess).
    """
    if not USE_MOCK_DATA:
        logger.warning('Attempt to request non-mock data but none is available')
        raise NotImplementedError('Only mock data available')
    return '', ''

def search(query, order_by, order, page_no, page_size):
    """
    Search mock database for asteroids

    Query database by a general prefix & ordering and paging spec
    Returns a page of results, if any, along with page number and total page count
    """

    if not USE_MOCK_DATA:
        logger.warning('Attempt to request non-mock data but none is available')
        raise NotImplementedError('Only mock data available')

    prefix = ''.join(query.split()).lower() # normalize query string
    queryset = []

    if prefix:
        # perform mock query
        queryset = [{
            'astno': item[0],
            'astname': item[1].title(),
            'astdsg': item[2].upper(),
        } for item in _MOCK_SEARCH_RESULTS if _matches(prefix, item)]
        queryset.sort(_compare(order_by, order=order))

    # compute paging info and slice page
    items_total = len(queryset)
    pages_total = (items_total + page_size - 1) // page_size
    page_no = max(min(page_no, pages_total), 0)
    page = queryset[page_no * page_size - page_size:page_no * page_size]

    return page, page_no, pages_total

logger.info('{} loaded'.format(__name__))
