
function WebUtilities() {
    'use strict';

    function escapeHTML(s) {
        return String(s).replace(/['&"<>]/g, function(m) {
            if (m === '"') return '&#34;';
            if (m === '&') return '&#38;';
            if (m === "'") return '&#39;';
            if (m === '<') return '&#60;';
            if (m === '>') return '&#62;';
        });
    }

    // simple client-side HTML template renderer, a bit hacky but works
    function renderHTML(template, data) {
        return template.replace(/(.)(\${\s*([_a-z][_a-zA-Z0-9]*)\s*})/g, function (m, m1, m2, key, i, s) {
            if (m1 === '$') return m2;
            return s[i] + escapeHTML(options(getEnumerableOwn(data, key), ''));
        });
    }

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }


    function getQueryParameter(key) {
        var re = new RegExp(format('[&?]%s=([^&#]*)', key));
        var m = re.exec(location.search);
        if (m != null) return m[1];
        return '';
    }

    function hasQueryParameter(key) {
        var re = new RegExp(format('[&?]%s([^=&#]*)', key)); //??
        var m = re.exec(location.search);
        return m != null;
    }

    // ad hoc form serialization
    function toXWWWFormURLEncoded(form) {
        var segments = [];
        forEach.call(form.elements, function (element) {
            var name = encodeURIComponent(element.name);
            var value = encodeURIComponent(element.value);
            if (element.disabled) return;
            if (element.type === 'text') {
                return void segments.push(format('%s=%s', name, value));
            }
            if (element.type === 'hidden') {
                return void segments.push(format('%s=%s', name, value));
            }
            if (element.type === 'checkbox') {
                if (!element.checked) return;
                return void segments.push(format('%s=%s', name, value));
            }
        });
        return segments.join('&');
    }

    function listen(root, eventType, selector, callback) {
        root.addEventListener(eventType, function _eventListener(event) {
            var node = event.target;
            for (;;) {
                // if current node matches given selector, call callback 
                if (node.matches && node.matches(selector)) {
                    callback.call(node, event);
                }
                if (node === root) return; // if listening root reached, stop
                node = node.parentNode; // else bubble up
            }
        }, false);
    }


    // -- BEGIN --
    // Console-polyfill. MIT license.
    // https://github.com/paulmillr/console-polyfill
    // Make it safe to do console.log() always.
    (function(global) {
      'use strict';
      if (!global.console) {
        global.console = {};
      }
      var con = global.console;
      var prop, method;
      var dummy = function() {};
      var properties = ['memory'];
      var methods = ('assert,clear,count,debug,dir,dirxml,error,exception,group,' +
         'groupCollapsed,groupEnd,info,log,markTimeline,profile,profiles,profileEnd,' +
         'show,table,time,timeEnd,timeline,timelineEnd,timeStamp,trace,warn').split(',');
      while (prop = properties.pop()) if (!con[prop]) con[prop] = {};
      while (method = methods.pop()) if (typeof con[method] !== 'function') con[method] = dummy;
      // Using `this` for web workers & supports Browserify / Webpack.
    })(typeof window === 'undefined' ? this : window);
    // -- END --



    return {

        escapeHTML: escapeHTML,
        renderHTML: renderHTML,
        
        getQueryParameter: getQueryParameter,
        hasQueryParameter: hasQueryParameter,
        getCookie: getCookie,
        //createXMLHTTPRequest: createXMLHTTPRequest,
        
        toXWWWFormURLEncoded: toXWWWFormURLEncoded,

        //ajax: ajax,

        listen: listen,

        // mainly for manipulating DOM Array-likes 
        forEach: Array.prototype.forEach,
        map: Array.prototype.map,
        reduce: Array.prototype.reduce,
        filter: Array.prototype.filter
        
    };

}








