

function ES5Utilities() {

    'use strict';

    function options(a, b) {
        var i, c;
        if (a != null) return a;
        if (b != null) return b;
        for (i = 2; i < arguments.length; i++) {
            c = arguments[i];
            if (c != null) return c;
        }
        throw TypeError('All options nil');
    }

    function peek(arr, i) {
        i = options(i, -1);
        if (i < 0) {
            i = arr.length + i;
        }
        return arr[i];
    }

    function arrayify() {
        var array = new Array(arguments.length);
        var i;
        for (i = 0; i < arguments.length; i++) {
            array[i] = arguments[i];
        }
        return array;
    }
    
    function hasEnumerableOwn(obj, key) {
        if (obj == null) return false;
        if (!obj.hasOwnProperty(key)) return false;
        if (!obj.propertyIsEnumerable(key)) return false;
        return true;
    }

    function getEnumerableOwn(obj, key) {
        if (hasEnumerableOwn(obj, key)) return obj[key];
        return void 0;
    }

    function forEnumerableOwn(obj, callback) {
        Object.keys(obj).forEach(function (key) {
            callback(obj[key], key, obj);
        });
    }

    function reassign(dst) {
        var i, src;
        dst = Object(dst);
        for (i = 1; i < arguments.length; i++) {
            src = arguments[i];
            forEnumerableOwn(dst, function (val, key) {
                if (src.hasOwnProperty(key)) {
                    dst[key] = src[key];
                }
            });
        }
        return dst;
    }

    // strip leading, trailing and multiple whitespace characters
    function normalizeWhitespace(s) {
        return s.replace(/[\s\uFEFF\xA0]+/g, ' ').trim();
    }

    // naive string formatter
    function format(spec) {
        var args = arrayify.apply(null, arguments);
        var i = 1;
        return spec.replace(/%[snjb%]/g, function (m) {
            if (i >= args.length) return '';
            if (m === '%s') return String(args[i++]);
            if (m === '%n') return Number(args[i++]);
            if (m === '%j') return JSON.stringify(args[i++]);
            if (m === '%b') return Boolean(args[i++]);
            if (m === '%%') return '%';
            throw SyntaxError(format('Bad specifier: %s', m));
        });
    }

    function once(fn) {
        var called = false;
        return function _once() {
            if (!called) {
                called = true;
                return fn.apply(this, arguments);
            }
        };
    }
    function throttle(fn, t) {
        var throttling = false;
        return function _throttled() {
            if (!throttling) {
                throttling = true;
                setTimeout(function _unthrottle() {
                    throttling = false;
                }, t);
                return fn.apply(this, arguments);
            }
        };
    }

    function debounce(fn, t) {
        var timeoutID = 0;
        return function _debounced() {
            var args = arrayify.apply(null, arguments);
            clearTimeout(timeoutID);
            timeoutID = setTimeout(function _bounce() {
                timeoutID = 0;
                return fn.apply(this, args);
            }, t);
        };
    }

    function identity(a) {
        return a;
    }

    function dummy() {}
    
    function NotImplementedError() {
        Error.apply(this, arguments);
    }
    
    NotImplementedError.prototype = Object.create(Error.prototype);
    NotImplementedError.prototype.constructor = NotImplementedError;

    return {
    
    
        options: options,
        
        peek: peek,
        arrayify: arrayify,
        
        hasEnumerableOwn: hasEnumerableOwn,
        getEnumerableOwn: getEnumerableOwn,
        forEnumerableOwn: forEnumerableOwn,
        reassign: reassign,
        
        normalizeWhitespace: normalizeWhitespace,
        format: format,
        
        once: once,
        throttle: throttle,
        debounce: debounce,
        identity: identity,
        dummy: dummy,
        
        NotImplementedError: NotImplementedError
    };
}


