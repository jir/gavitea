
------------------------------------------------------------------------------
                     NUMERICAL DATA PRODUCTS FROM GAVITEA
------------------------------------------------------------------------------
------------------------------------------------------------------------------

GAVITEA data product package contains one or more of the top level folders
A-IS, A-ES and A-CS, corresponding to the GAVITEA algorithms. Each top level
folder contains a subfolder called TXT and optionally subfolders CSV and/or
VOT, depending on which formats the user has chosen to have as output. Each
subfolder contains the same data products in different formats: 

- .txt files in the TXT folder are space-separated tables
- .csv files in the CSV folder are comma-separated tables
- .vot files in the VOT folder are VOTable format (xml) files.


------------------------------------------------------------------------------
A-IS FOLDER
------------------------------------------------------------------------------

The A-IS data products consist of three files (ais_PeriodScan, ais_PoleScan
and ais_FitAll), all containing one header row and one or more rows for model
parameter vector solutions. The model parameters in files are arranged in
columns in the following order:

1.  Rotation period
2.  Spin axis longitude
3.  Spin axis latitude
4.  Rotational phase at zero epoch
5.  The longest model ellipsoid's axis
6.  The middle model ellipsoid's axis
7.  The shortest model ellipsoid's axis
8.  Geometric albedo
9.  G1 scattering parameters
10. G2 scattering parameter (obsolete in this version)
11. Model ellipsoid size
12. Zero epoch

All the angles are given in the input data's reference frame.
 
A-IS output file contents are the following, with the residual of the fit
added to the end of each parameter vector:
 
  --------------
  ais_PeriodScan
  --------------
  The best-fit model parameters from the period scanning part of A-IS. Each
  row corresponds to one scanned period value, the rest of the model
  parameters being fitted.
  
  ------------
  ais_PoleScan
  ------------
  The best-fit model parameters from the pole scanning part of A-IS. Each row
  corresponds to one scanned pole direction, the rest of the model parameters
  being fitted.
  
  ----------
  ais_FitAll
  ----------
  The best-fit model parameters from the all-parameter fit. This file contains
  only one best-fit solution vector, solved using the initial values from
  period and pole scanning.


------------------------------------------------------------------------------
A-ES FOLDER
------------------------------------------------------------------------------

The A-ES data products consist one of the parameter vector files "aes_lsv",
"aes_is" or "aes_mcmc" containing one header row and a number of rows for
model parameter vector solutions. Along the aes_is and aes_mcmc files, there
also exists a weights file providing the weight for each parameter vector
solution. In aes_lsv, aes_is and aes_mcmc files the model parameters are
arranged in  columns in the following order:

1.  Rotation period
2.  Spin axis longitude
3.  Spin axis latitude
4.  Rotational phase at zero epoch
5.  The longest model ellipsoid's axis
6.  The middle model ellipsoid's axis
7.  The shortest model ellipsoid's axis
8.  Geometric albedo
9.  G1 scattering parameters
10. G2 scattering parameter (obsolete in this version)
11. Model ellipsoid size
12. Zero epoch

All the angles are given in the input data's reference frame.
 
A-ES output file contents are the following, with the residual and chi square
of the fit provided as two additional columns in the end of each parameter
vector:
 
  -------
  aes_lsv
  -------
  The best-fit model parameters for the set of virtual observations
  
  ------
  aes_is
  ------
  The accepted solutions from the importance sampler
  
  --------
  aes_is_w
  --------
  The weights for the accepted solutions from the importance sampler
 
  --------
  aes_mcmc
  --------
  The accepted solutions from the Markov chain Monte Carlo sampling

  ----------
  aes_mcmc_w
  ----------
  The weights for the accepted solutions from the Markov chain Monte Carlo
  sampling

  
A-CS FOLDER
------------------------------------------------------------------------------
The A-CS data products consist one of the parameter vector files "acs_lsv",
"acs_is" or "acs_mcmc" containing one header row and a number of rows for
model parameter vector solutions. Along the acs_is and acs_mcmc files, there
also exists a weights file providing the weight for each parameter vector
solution. In acs_lsv, acs_is and acs_mcmc files the model parameters are
arranged in columns in the following order:

1.  Rotation period
2.  Spin axis longitude
3.  Spin axis latitude
4.  Rotational phase at zero epoch
5.  Geometric albedo
6.  G1 scattering parameters
7.  G2 scattering parameter (obsolete in this version)
8.  Model ellipsoid size
9.  Zero epoch
10. Coefficients of the spherical harmonics series representing the shape

All the angles are given in the input data's reference frame.
 
A-ES output file contents are the following, with the residual and chi square
of the fit provided as two additional columns in the end of each parameter
vector:
 
  -------
  aes_lsv
  -------
  The best-fit model parameters for the set of virtual observations
  
  ------
  aes_is
  ------
  The accepted solutions from the importance sampler
  
  --------
  aes_is_w
  --------
  The weights for the accepted solutions from the importance sampler
  
  --------
  aes_mcmc
  --------
  The accepted solutions from the Markov chain Monte Carlo sampling

  ----------
  aes_mcmc_w
  ----------  
  The weights for the accepted solutions from the Markov chain Monte Carlo
  sampling
  
  
