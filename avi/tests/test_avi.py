"""
Build verification tests for GAVITEA
"""

from logging import getLogger
logger = getLogger(__name__)

from unittest import TestCase

from os import access
from os import X_OK, R_OK, W_OK

from sys import version_info

from avi.settings import VERSION
from avi.settings import AIS_BIN
from avi.settings import AES_BIN
from avi.settings import ACS_BIN
from avi.settings import OUTPUT_PATH

from bokeh import __version__ as BOKEH_VERSION
from django import get_version as get_django_version
from rest_framework import VERSION as DRF_VERSION
from numpy import __version__ as NUMPY_VERSION
from astropy import __version__ as ASTROPY_VERSION

class AVITest(TestCase):

    def test_python_version(self):
        self.assertTrue(version_info.major == 2)
        self.assertTrue(version_info.minor >= 7)
        self.assertTrue(version_info.releaselevel == 'final')

    def test_avi_version(self):
        self.assertTrue(VERSION == '1.1.1-rc6')

    def test_framework_version(self):
        self.assertTrue(True) # not available programmatically

    def test_bokeh_version(self):
        major, minor, patch = map(int, BOKEH_VERSION.split('.'))
        self.assertTrue(major == 0)
        self.assertTrue(minor >= 12)

    def test_astropy_version(self):
        major, minor, patch = map(int, ASTROPY_VERSION.split('.'))
        self.assertTrue(major == 1)
        self.assertTrue(minor >= 2)

    def test_django_version(self):
        major, minor, patch = map(int, get_django_version().split('.'))
        self.assertTrue(major == 1)
        self.assertTrue(minor >= 9)

    def test_drf_version(self):
        major, minor, patch = map(int, DRF_VERSION.split('.'))
        self.assertTrue(major == 3)
        self.assertTrue(minor >= 3)

    def test_numpy_version(self):
        major, minor, patch = map(int, NUMPY_VERSION.split('.'))
        self.assertTrue(major == 1)
        self.assertTrue(minor >= 11)

    def test_ais_bin(self):
        self.assertTrue(access(AIS_BIN, X_OK))

    def test_aes_bin(self):
        self.assertTrue(access(AES_BIN, X_OK))

    def test_acs_bin(self):
        self.assertTrue(access(ACS_BIN, X_OK))

    def test_output_dir(self):
        self.assertTrue(access(OUTPUT_PATH,R_OK|W_OK|X_OK))

logger.info('{} loaded'.format(__name__))
