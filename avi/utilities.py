"""
Various cross-cutting utilities
"""

from logging import getLogger
logger = getLogger(__name__)

from os.path import normpath
from os.path import join
from os.path import basename
from os.path import split
from os.path import splitext
from os.path import isfile as is_file
from os.path import isdir as is_dir
from os.path import getsize as get_size
from os import mkdir
from os import chdir
from os import listdir
from os import walk
from subprocess import call
from shutil import copyfile as cp
from shutil import copytree as cpdir
from shutil import rmtree as rmdir
from shutil import move as mv
from glob import glob

from numpy import loadtxt as load_txt
from numpy import savetxt as save_txt
from astropy.io.ascii import write
from astropy.table import Table
from astropy.io.votable import from_table, writeto

def update_existing(dst, src):
    """Update values of whitelisted (i.e. already existing) keys only""" 
    for key in dst:
        if key in src:
            dst[key] = src[key]

def path(*segments):
    """Normalize and join given path segments into a single path"""
    return normpath(join(*map(str, segments)))

def suffix(path):
    """Return filename suffix"""
    return splitext(path)[1]

def load(path, mode='r'):
    """Load file from given path"""
    with open(path, mode=mode) as file:
        return file.read()

def save(path, data='', mode='w'):
    """Save file under given path"""
    with open(path, mode=mode) as file:
        return file.write(data)

def ls(path):
    """List regular files in a directory"""
    fnames = [fname for fname in listdir(path) if is_file(join(path, fname))]
    return fnames

def cmd(*argv, **kwargs):
    """Run an external command in a child process"""
    try:
        status = call(argv, **kwargs)
        if status:
            raise Exception('%s failed with status: %d' % (' '.join(argv), status))
    except Exception as exc:
        raise Exception('%s failed: %s' % (' '.join(argv), exc))

def save_csv(path, data):
    """Save tabular data as CSV"""
    write(data, path, format='csv')

def save_vot(path, data):
    """Save tabular data as a single-table TABLEDATA VOTable"""
    table = Table(data=data)
    votable = from_table(table)
    writeto(votable, path, tabledata_format='tabledata')

def txt_to_csv(src, dst, dtype=float):
    """File format conversion utility .txt -> .csv"""
    data = load_txt(src, dtype=dtype)
    save_csv(dst, data)

def txt_to_vot(src, dst, dtype=float):
    """File format conversion utility .txt -> .vot"""
    data = load_txt(src, dtype=dtype)
    save_vot(dst, data)

def get_dir_size(start):
    total_size = 0
    for dirpath, dirnames, filenames in walk(start):
        for f in filenames:
            total_size += get_size(join(dirpath, f))
    return total_size

logger.info('{} loaded'.format(__name__))
