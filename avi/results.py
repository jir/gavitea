

from avi.utilities import path, is_file, get_size, load, glob
from avi.settings import OUTPUT_PATH, SHARE_PATH, METADATA_PATH
from json import loads

from logging import getLogger
logger = getLogger(__name__)

class Results:

    def __init__(self, md5, shared=False):
        self.md5 = md5
        self.shared = shared
        self.base = OUTPUT_PATH
        if shared:
            self.base = SHARE_PATH

    def _path(self, *segments):
        return path(self.base, self.md5, *segments)

    def retrieve_metadata(self):
        metadata = loads(load(self._path(METADATA_PATH)))
        return metadata

    def retrieve_viz2d(self, algorithm):
        viz2d = 'No plot available.'
        try:
            viz2d = load(path(OUTPUT_PATH, self.md5, 'Output/{}_plot.html'.format(algorithm)))
        except:
            pass
        return viz2d 

    def retrieve_viz3d(self):
        metadata = self.retrieve_metadata()
        return {
            'md5': metadata['md5'],
            'astno': metadata['astno'],
            'astname': metadata['astname'],
            'astdsg': metadata['astdsg'],
            'indexes': self.retrieve_viz3d_solutions(),
        }

    def retrieve_viz3d_solutions(self):
        solutions = []
        for filename in glob(self._path('Output/acs_facets*.txt')):
            if get_size(filename) > 1024: # small file size indicates "unrealistic shape"
                solutions.append(int(filename.split('acs_facets')[1].split('.txt')[0]))
        solutions.sort()
        return solutions

    def retrieve_viz3d_solution(self, solution):
        facets = load(self._path('Output/acs_facets{}.txt'.format(solution)))
        vertices = load(self._path('Output/acs_vertices{}.txt'.format(solution)))
        return {'index': solution, 'facets': facets, 'vertices': vertices}

logger.info('{} loaded'.format(__name__))

