export FC=f95
export FCFLAGS='-fprofile-arcs -ftest-coverage'
cd Libf90

# Test for test case GAVITEA-SVT_01342

echo "\n-----------------------"
echo "Running test case 01342"
echo "Unit tests for modeules HG1G2 and Randev"
echo "-----------------------\n"

rm -f HG1G2_fun*
rm -f HG1G2.o
rm -f HG1G2.mod
funit HG1G2
gcov HG1G2.f90

rm -f Randev_fun*
rm -f Randev.o
rm -f Randev.mod
funit Randev
gcov Randev.f90

cd ../

# Test for test case GAVITEA-SVT_01302

echo "\n-----------------------"
echo "Running test case 01302"
echo "Each simulation (i.e., test run) takes less than a minute."
echo "Tests algorithm performance for 17 datasets with constant input parameters"
echo "-----------------------\n"

f95 Libf90/flush_progress.f90 Libf90/Randev.f90 Libf90/Voper.f90 Libf90/HG1G2.f90 Libf90/Viewgeo.f90 Libf90/Le.f90 A_ES.f90 SVT_01302.f90 -o SVT_01302.o
./SVT_01302.o

# Test for test case GAVITEA-SVT_01312

echo "\n-----------------------"
echo "Running test case 01312"
echo "Each simulation (i.e., test run) may take some minutes."
echo "Tests algorithm performance for 1 dataset with varying input parameters"
echo "-----------------------\n"

f95 Libf90/flush_progress.f90 Libf90/Randev.f90 Libf90/Voper.f90 Libf90/HG1G2.f90 Libf90/Viewgeo.f90 Libf90/Le.f90 A_ES.f90 SVT_01312.f90 -o SVT_01312.o
./SVT_01312.o
