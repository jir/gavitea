module A_ES

use Le
use HG1G2
use Randev
use flush_progress
implicit none
integer,parameter,private::tark=selected_real_kind(12,30),maxobs=10000,maxpar=20,npar=12
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0),tp=2.*pi,rd=pi/180.0d0,sd=24.0d0*3600.0d0

contains

subroutine ellipsoid(ecode)
 
! A_ES computes photometric brightnesses for asteroids modeled as triaxial
! ellipsoids and solves the lightcurve inversion problem using optimization,
! virtual observations, virtual-observation Markov-Chain Monte-Carlo method (MCMC)
! and virtual-observation MCMC importance sampling. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-28

integer::nobs,PID(maxpar),nmc,nmcmc,ndim,j0,j1,j2,nd1,nd2,nd3
real(kind=tark)::P(maxpar),PD(maxpar),lobsmn,RILL(3,maxobs),ROBS(3,maxobs),&
       lsigrv,dchisqc,car,amtol,vsc,dphi1a,dphi1b,dphi2a,dphi2b,&
       dphi3a,dphi3b,clat
real(kind=tark),dimension(maxobs)::LOBS,LSIG,TMJD
real(kind=tark),dimension(3,maxobs)::XOBS,XSUN
real(kind=tark),dimension(0:360)::PHID1,PHID2,PHID3,AD1,AD2,AD3
real(kind=tark),dimension(361)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
character(len=70) header,dumc
integer,intent(out)::ecode
character(len=200)::indir,outdir

! Input observations, and the illumination and observation geometry:
call get_command_argument(1,indir)
call get_command_argument(2,outdir)
open(1,file=trim(indir)//"/inputData.txt")
read(1,*) nobs
read(1,*) header
do j0=1,nobs
   read(1,*) TMJD(j0),LOBS(j0),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
end do
close(1)

do j1=1,nobs
   LOBSMN=LOBSMN+LOBS(j1)
end do
LOBSMN=LOBSMN/nobs
do j1=1,nobs
   LOBS(j1)=LOBS(j1)/LOBSMN
   ROBS(1,j1)=sqrt(XOBS(1,j1)**2+XOBS(2,j1)**2+XOBS(3,j1)**2)
   ROBS(3,j1)=asin(XOBS(3,j1)/ROBS(1,j1))
   clat=cos(ROBS(3,j1))
   ROBS(2,j1)=acos(XOBS(1,j1)/(ROBS(1,j1)*clat))
   if (XOBS(2,j1)/clat.lt.0.0d0) ROBS(2,j1)=2.0d0*pi-ROBS(2,j1)

   RILL(1,j1)=sqrt(XSUN(1,j1)**2+XSUN(2,j1)**2+XSUN(3,j1)**2)
   RILL(3,j1)=asin(XSUN(3,j1)/RILL(1,j1))
   clat=cos(RILL(3,j1))
   RILL(2,j1)=acos(XSUN(1,j1)/(RILL(1,j1)*clat))
   if (XSUN(2,j1)/clat .lt.0.0d0) RILL(2,j1)=2.0d0*pi-RILL(2,j1)
end do

! Model parameters from the user
open(1,file=trim(indir)//"/aes_inputParameters.txt")
read(1,*) car
read(1,*) amtol
read(1,*) nmc
read(1,*) lsigrv
read(1,*) nmcmc
read(1,*) dchisqc
read(1,*) vsc
close(1)

! Initial parameters from spin scanning
open(1,file=trim(outdir)//'/ais_FitAll.txt')
read(1,*) dumc
read(1,*) (P(j0),j0=1,npar)
close(1)
P(1)=p(1)*3600.
do j0=2,4
   P(j0)=P(j0)*rd
end do
if (P(2).gt.tp)    P(2)=P(2)-tp*(int((P(2)-tp)/(tp))+1)
if (P(2).lt.0.0d0) P(2)=P(2)+tp*(int((tp-P(2))/(tp)))
if (P(4).gt.tp)    P(4)=P(4)-tp*(int((P(4)-tp)/(tp))+1)
if (P(4).lt.0.0d0) P(4)=P(4)+tp*(int((tp-P(4))/(tp)))
! MODEL PARAMETERS

! Input parameters for spin, shape, scattering, and size:

! P(1)  = rotation period           (s;   given in d in ptd.in) 
! P(2)  = pole longitude            (rad; given in deg in ptd.in)
! P(3)  = pole latitude             (rad; given in deg in ptd.in)
! P(4)  = rotational phase          (rad; given in deg in ptd.in)
! P(5)  = ellipsoid axis a          (relative units)
! P(6)  = ellipsoid axis b          (relative units)
! P(7)  = ellipsoid axis c          (relative units) 
! P(8)  = geometric albedo
! P(9)  = G1 parameter
! P(10) = G2 parameter
! P(11) = ellipsoid axis a          (km)
! P(12) = epoch of rotational phase (MJD)

! Initial parameter deviations for the preconditioning of the
! simplex least-squares iteration:
! These could be taken from spin scanning, but are they too small ??
! Maybe better to take new ones
PD(1)=0.5d-2*P(1)**2/(sd*(TMJD(nobs)-TMJD(1)))
PD(2)=1.0d-1*rd
PD(3)=1.0d-1*rd
PD(4)=1.0d-1*rd
PD(5)=1.0d-3
PD(6)=1.0d-3
PD(7)=1.0d-3
PD(8)=1.0d-4
PD(9)=1.0d-4
PD(10)=1.0d-4
PD(11)=1.0d-3

! Initialization of the H, G1, G2 phase function:

dphi3b=0.0d0
call PHIINI(PHID1,PHID2,PHID3,dphi1a,dphi1b,dphi2a,dphi2b,dphi3a,AD1,AD2,AD3,nd1,nd2,nd3)
call LSPLINEDI(PHID1,dphi1a,dphi1b,AD1,XL1,YL12,YL2_12,nd1)
call LSPLINEDI(PHID2,dphi2a,dphi2b,AD2,XL2,YL22,YL2_22,nd2)
call LSPLINEDI(PHID3,dphi3a,dphi3b,AD3,XL3,YL32,YL2_32,nd3)
! VIRTUAL LEAST SQUARES SOLUTION

do j0=1,npar
   PID(j0)=1
end do
PID(5)=0
PID(8)=0
PID(10)=0
PID(12)=0
ndim=0
do j0=1,npar
   if (PID(j0).gt.0) ndim=ndim+1
end do

!print*,'------------------------------------------'
!print*,'BEGIN THE GENERATION OF VIRTUAL SOLUTIONS'
!print*,'------------------------------------------'
call LSV(P,PD,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
    YL2_32,dphi1a,dphi2a,lsigrv,vsc,car,amtol,nobs,PID,ndim,nmc,nd1,nd2,nd3,outdir)
!print*,'Completed the generation of virtual solutions'
!print*,''

!print*,'------------------------'
!print*,'BEGIN IMPORTANCE SAMPLING'
!print*,'------------------------'
! IMPORTANCE SAMPLING
ecode=0
call IS(P,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
   dphi1a,dphi2a,dchisqc,car,nobs,PID,ndim,nmcmc,nmc,nd1,nd2,nd3,ecode,outdir)
if (ecode==0) then
!   print*,'Completed importance sampling'
   ecode=-70
   open(1,file=trim(outdir)//"/aes_outf_name.txt")
   write(1,*) "aes_is.txt"
   close(1)
   open(10,file="./aes_mcmc_progress.txt")
   write(10,*) 0
   close(10)
   return
endif

!print*,'-------------------'
!print*,'BEGIN MCMC SAMPLING'

! MCMC sampling
ecode=0
call MCMC(P,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
     dphi1a,dphi2a,car,nobs,PID,ndim,nmcmc,nmc,nd1,nd2,nd3,ecode,outdir)
if (ecode==0) then
!   print*,'Completed MCMC sampling'
   ecode=-60
   open(1,file=trim(outdir)//"/aes_outf_name.txt")
   write(1,*) "aes_mcmc.txt"
   close(1)
   return
endif
open(1,file=trim(outdir)//"/aes_outf_name.txt")
write(1,*) "aes_lsv.txt"
close(1)
end


subroutine LSV(P0,PD0,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
          YL32,YL2_32,dphi1a,dphi2a,lsigrv,vsc,car,amtol,nobs,PID,ndim,nmc,nd1,nd2,nd3,outdir)
 
! Downhill simplex optimization for virtual least-squares 
! triaxial-ellipsoid models.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-28

integer,intent(in)::nobs,PID(maxpar),ndim,nmc,nd1,nd2,nd3
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark),dimension(maxpar),intent(in)::PD0
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),car,amtol,vsc,lsigrv,&
       dphi1a,dphi2a
real(kind=tark),dimension(maxpar),intent(inout)::P0
integer::j0,j1,j2,j3,jmc,NP(maxpar)
real(kind=tark),dimension(maxobs)::APHS,VOBS,LOBSV,LSIGV,LCOM
real(kind=tark)::RILL1(3),ROBS1(3),SS(4),EILL(3,3),EOBS(3,3),&
       SPIN(4),lobsvmn,lcommn,VC(0:2,100,1000),LDIC(0:2,100,1000),rg,&
       lsigrv1,chisq,rms,rmsls
real(kind=tark),dimension(maxpar)::PLS,PLSV,P,P1,PD1
character(len=200)::outdir

! Parameters to be optimized:
NP=0
j0=0
do j1=1,npar
   if (PID(j1).gt.0) then
      j0=j0+1
      NP(j0)=j1
   endif
end do
! Least-squares solution:
PD1=0
call ELSI(P0,PD1,PD0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,&
     YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,nobs,NP,ndim,nd1,nd2,nd3)
call ELS(PLS,P0,PD1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,&
    YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,amtol,nobs,NP,ndim,nd1,nd2,nd3)

rmsls=rms
!print*,'LSV:',rms,LSIG(1)
! Virtual least-squares solutions:

open (1,file=trim(outdir)//'/aes_lsv.txt')
write(1,*) "P lon lat phi_0 a b c alb g1 g2 size epoch_0 rms"
open(10,file="./aes_lsv_progress.txt")
write(10,*) nmc
do jmc=1,nmc
   call flush_()
90 lobsvmn=0.0d0
   lsigrv1=lsigrv/rms
   do j0=1,nobs
      call RANG2(rg)
      LOBSV(j0)=LOBS(j0)*(1.0d0+rg*lsigrv1)
      lobsvmn=lobsvmn+LOBSV(j0)
      LSIGV(j0)=sqrt(LSIG(j0)**2+lsigrv1**2)
   end do
   lobsvmn=lobsvmn/nobs
   do j0=1,nobs
      LOBSV(j0)=LOBSV(j0)/lobsvmn
   end do

   do j1=1,npar
      P1(j1)=PLS(j1)
   end do
   PLSV=0
   call ELS(PLSV,P1,PD1,LOBSV,LSIGV,LCOM,LCOMMN,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,&
       YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,amtol,nobs,NP,ndim,nd1,nd2,nd3)

   if (PLSV(2).gt.tp) PLSV(2)=PLSV(2)-tp*(int((PLSV(2)-tp)/(tp))+1)
   if (PLSV(2).lt.0.0d0) PLSV(2)=P(2)+tp*(int((tp-PLSV(2))/(tp)))
   if (PLSV(4).gt.tp)    PLSV(4)=P(4)-tp*(int((PLSV(4)-tp)/(tp))+1)
   if (PLSV(4).lt.0.0d0) PLSV(4)=P(4)+tp*(int((tp-PLSV(4))/(tp)))
   if (rms.gt.vsc*sqrt(rmsls**2+lsigrv**2)) goto 90

   write (1,'(10000(E20.14,2X))') PLSV(1)/3600.0d0,(PLSV(j1)/rd,j1=2,4),(PLSV(j1),j1=5,npar),rms
end do
close (1)
close(10)
do j1=1,npar
   P0(j1)=PLS(j1)
end do
end


subroutine MCMC(P0,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
      YL32,YL2_32,dphi1a,dphi2a,car,nobs,PID,ndim,nmcmc,nmc,nd1,nd2,nd3,ecode,outdir)
 
! Virtual-observation MCMC for triaxial-ellipsoid models.
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-28

integer,intent(in)::nobs,PID(maxpar),ndim,nmc,nmcmc,nd1,nd2,nd3
integer,intent(inout)::ecode
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),P0(maxpar),car,dphi1a,dphi2a
integer::NP(maxpar),j0,j1,j2,j3,jmc,jmcmc
real(kind=tark),dimension(maxobs)::LCOM,W
real(kind=tark)::P1(maxpar),P2(maxpar),Q1(maxpar),Q2(maxpar),lcommn,PV(maxpar,100000),chisq1,&
       chisq2,pdfr,rms1,rms2
character(len=1) header
character(len=200)::outdir

! Parameters to be sampled:

j0=0
do j1=1,npar
   if (PID(j1).gt.0) then
      j0=j0+1
      NP(j0)=j1
   endif
end do
! Input virtual least-squares solutions:
open(1,file=trim(outdir)//'/aes_lsv.txt')
read(1,*) header
do jmc=1,nmc
   read (1,*) (PV(j1,jmc),j1=1,npar)
   PV(1,jmc)=PV(1,jmc)*3600.0d0
   PV(2,jmc)=PV(2,jmc)*rd
   PV(3,jmc)=PV(3,jmc)*rd
   PV(4,jmc)=PV(4,jmc)*rd
end do
close(1)

! Virtual-observation MCMC; reset weights, set initial parameters 
! and compute the corresponding chisq:

do jmcmc=1,nmcmc
        W(jmcmc)=0.0d0
end do

do j1=1,npar
        P1(j1)=P0(j1)
end do
do j1=1,ndim
        Q1(j1)=P0(NP(j1))
end do
call LECHI2(chisq1,rms1,Q1,P1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,&
       YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,NP,ndim,nd1,nd2,nd3)
! Output initial parameters as the first sample (strictly: burn-in phase)

open(1,file=trim(outdir)//'/aes_mcmc.txt')
write(1,*) "P lon lat phi_0 a b c alb g1 g2 size epoch_0 rms chisq"

jmcmc=1
W(jmcmc)=1.0d0
write (1,'(10000(E20.14,2X))') P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),(P1(j1),j1=5,npar),&
                rms1,chisq1-nobs

! Markov chain Monte Carlo:
j0=0
open(10,file="./aes_mcmc_progress.txt")
write(10,*) nmcmc
do while (1==1)
   if (jmcmc.ge.nmcmc) then
      close(unit=1)

! Output weights:
      open(unit=1,file=trim(outdir)//'/aes_mcmc_w.txt')
      do j1=1,nmcmc
         write (1,'(I12,1X,E12.6)') j1,W(j1)
      end do
      close(unit=1)
	  close(10)
      return
   endif

! Proposal and its chisq:
   call EPROPOSE(P2,P1,PV,car,NP,ndim,nmc)
   do j1=1,ndim
           Q2(j1)=P2(NP(j1))
   end do
   call LECHI2(chisq2,rms2,Q2,P2,LOBS,LSIG,LCOM,LCOMMN,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,&
          YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,NP,ndim,nd1,nd2,nd3)
! Accept or reject:

   pdfr=exp(-0.5d0*(chisq2-chisq1))
   if (RAN2().le.pdfr) then
      chisq1=chisq2
      rms1=rms2
      do j1=1,npar
         P1(j1)=P2(j1)
      end do
      jmcmc=jmcmc+1
      W(jmcmc)=1.0d0
      write (1,'(10000(E20.14,2X))') P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),&
                     (P1(j1),j1=5,npar),rms1,chisq1-nobs
	  call flush_()
   else
      W(jmcmc)=W(jmcmc)+1.0d0
	  if(W(jmcmc)>100000) then
	     ecode=60
		 close(1)
!		 call flush_(3,0,0)
		 close(10)
		 return
	  endif
   endif
end do
close(10)
end


subroutine IS(P0,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
         YL2_32,dphi1a,dphi2a,dchisqc,car,nobs,PID,ndim,nmcmc,nmc,nd1,nd2,nd3,ecode,outdir)
 
! Importance sampler with MCMC for triaxial-ellipsoid models.
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-28
             
integer,intent(in)::nobs,PID(maxpar),ndim,nmc,nmcmc,nd1,nd2,nd3
integer,intent(inout)::ecode
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),car,&
       dchisqc,dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark),dimension(maxpar),intent(in)::P0
integer::NP(maxpar),j0,j1,j2,j3,jmc,jmcmc,nsamp
real(kind=tark),dimension(maxobs)::LCOM,W
real(kind=tark)::lcommn,PV(maxpar,100000),chisq1,chisq2,dchisq1,dchisq2,rms1,rms2
real(kind=tark),dimension(maxpar)::P1,P2,Q1,Q2
character(len=1) header
character(len=200)::outdir
! Parameters to be sampled:

do j1=1,npar
   NP(j1)=0
end do
j0=0
do j1=1,npar
   if (PID(j1).gt.0) then
      j0=j0+1
      NP(j0)=j1
   endif
end do

! Input virtual least-squares solutions:

open(1,file=trim(outdir)//'/aes_lsv.txt')
read(1,*) header
do jmc=1,nmc
   read (1,*) (PV(j1,jmc),j1=1,npar)
   PV(1,jmc)=PV(1,jmc)*3600.0d0
   PV(2,jmc)=PV(2,jmc)*rd
   PV(3,jmc)=PV(3,jmc)*rd
   PV(4,jmc)=PV(4,jmc)*rd
end do
close(1)

! Importance sampling with virtual-observation MCMC for mapping phase space; 
! reset weights, set initial parameters and compute the corresponding chisq:

do jmcmc=1,nmcmc
   W(jmcmc)=0.0d0
end do

do j1=1,npar
   P1(j1)=P0(j1)
end do
do j1=1,ndim
   Q1(j1)=P0(NP(j1))
end do
call LECHI2(chisq1,rms1,Q1,P1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,&
       YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,NP,ndim,nd1,nd2,nd3)

! Output initial parameters as the first sample (strictly: burn-in phase)

open(1,file=trim(outdir)//'/aes_is.txt')
write(1,*) "P lon lat phi_0 a b c alb g1 g2 size epoch_0 rms chisq"

jmcmc=1
nsamp=0
dchisq1=chisq1-nobs
W(jmcmc)=exp(-0.5d0*dchisq1)
write (1,'(10000(E20.14,2X))') P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),&
                (P1(j1),j1=5,npar),rms1,dchisq1

! Markov chain Monte Carlo for phase-space mapping:
open(10,file="./aes_is_progress.txt")
write(10,*) nmcmc
do while(1==1)
   if (jmcmc.ge.nmcmc) then
      close(1)

! Output weights:

      open(unit=1,file=trim(outdir)//'/aes_is_w.txt')
      do j1=1,nmcmc
         write (1,'(I12,1X,E12.6)') j1,W(j1)
      end do
      close(1)
	  close(10)
      return
   endif

! Proposal and its chisq:

   call EPROPOSE(P2,P1,PV,car,NP,ndim,nmc)
   do j1=1,ndim
      Q2(j1)=P2(NP(j1))
   end do
   call LECHI2(chisq2,rms2,Q2,P2,LOBS,LSIG,LCOM,LCOMMN,TMJD,RILL,ROBS,XL1,XL2,&
          XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,&
          NP,ndim,nd1,nd2,nd3)

! Accept or reject:
   dchisq2=chisq2-nobs
   if (dchisq2.le.dchisqc) then
      chisq1=chisq2
      rms1=rms2
      dchisq1=dchisq2
      do j1=1,npar
         P1(j1)=P2(j1)
      end do
      jmcmc=jmcmc+1
	  nsamp=0
      W(jmcmc)=exp(-0.5d0*dchisq1)
      write (1,'(10000(E20.14,2X))') P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),&
                 (P1(j1),j1=5,npar),rms1,dchisq1
	  call flush_()
   else
      W(jmcmc)=W(jmcmc)+exp(-0.5d0*dchisq1)
	  nsamp=nsamp+1
	  if(nsamp>100) then
	     ecode=70
! 		 call flush_(2,0,0)
		 close(10)
		 close(1)
		 return
	  endif
   endif
end do
close(10)
end

end module