program callell
! This is just to generate test input data.
! It is assumed that the data is treated as one single lightcurve.
! Here the the time of the first observation is NOT 0, but T(0)-240000.5
! Observations must be in chronological order.

! Compile:
! f95 Libf90/flush_progress.f90 Libf90/Randev.f90 Libf90/Voper.f90 Libf90/HG1G2.f90 Libf90/Viewgeo.f90 Libf90/Le.f90 A_ES.f90 SVT_01302.f90 -o SVT_01302.o

use Randev
use A_ES
implicit none
integer,parameter::tark=selected_real_kind(12,30),maxobs=10000,maxpar=20,npar=12
real(kind=tark),parameter::pi=4.0d0*atan(1.0d0),tp=2.*pi,rd=pi/180.0d0
real(kind=tark),dimension(maxobs)::LOBS,LSIG,TDT
real(kind=tark),dimension(3,maxobs)::XOBS,XSUN,RILL,ROBS
real(kind=tark)::LOBSMN,lsigmn,car,amtol,lsigr,dchisqc,lsigrv,vsc,clat,&
       P(maxpar),P0(maxpar),tspan,csig,cpui,cpuf,fline(14)
integer::nmc,nmcmc,j0,j1,j2,j3,nobsi,isim,nsim1,nsim2,pas1,pas2,npas1,npas2,ipas1,ipas2,&
         ecode,iini
character(len=6) setnr,strsim,dum
character(len=45) outdir,output
logical :: dir_e

! For testing purposes, output files are coded according to the test case

! Choose which lightcurve is used for the tests. The following parameter must be in agreement with the 
! parameter used in A-IS algorithm testing, since the output files from A-IS are used in A-ES, 
! and named based on this parameter
nsim1=1
nsim2=22
setnr='01302'

! These are provided by the user in the parameter GUI, partly the same as for spin scanning:
! car      ! Lower/upper bound for ellipsoid axial ratio.
! Advanced input from GUI:
! nmcmc      ! number of MCMC solutions
! nmc      ! Number of virtual observation solutions
! amtol      ! Downhill simplex tolerance.
! lsigrv   ! Virtual error for virtual least squares solutions
! vsc      ! rms acceptance threshold for virt ls solutions
! dchisqc   ! limiting delta chi2 for the importance sampler
car=0.3
amtol=1.d-5
nmc=100 ! n virtual
nmcmc=100 ! n mcmc
lsigrv=0.001
vsc=1.0
dchisqc=1000.

open(1,file="Input/aes_inputParameters.txt")
! Basic input
write(1,10) car,"Lower limit for ellipsoid axis ratios"
! Advanced input
write(1,10) amtol,"Simplex tolerance"
write(1,20) nmc,"Number of virtual MCMC solutions"
write(1,10) lsigrv,"Virtual error"
write(1,20) nmcmc,"Number of MCMC solutions"
write(1,10) dchisqc
write(1,10) vsc
close(1)
10 format(F14.6,5X,A60)
20 format(I5,5X,A60)

! Define the id for the test output files
outdir='Output/Set'//trim(adjustl(setnr))
outdir=adjustl(outdir)

open(unit=2,file=trim(outdir)//'/'//trim(setnr)//'_test.out')
open(unit=4,file=trim(outdir)//'/'//trim(setnr)//'_testTable.out')
write(4,*) "nobsi,cpuf-cpui"
npas1=0
npas2=0
open(7,file="Input/ais_FitAllList.txt")
do iini=1,nsim1-1
	read(7,*) P(1:12)
end do

do isim=nsim1,nsim2 ! Loop over simulated lightcurves
   if(isim.ne.4.and.isim.ne.10.and.isim.ne.14.and.isim.ne.20.and.isim.ne.22) then
   else
		goto 30
   endif
   write(strsim,'(I4)') isim
   strsim=adjustl(strsim)
   output=trim(outdir)//"/"//trim(setnr)//"_T"//trim(strsim)//"_"
! Input observations, and the illumination and observation geometry. Write file inputData.txt:
   open(unit=1,file='../TestSet/lc'//trim(strsim)//'.out')
   read (1,*) nobsi
   do j0=1,nobsi
      read(1,*) TDT(j0),LOBS(j0),XSUN(1:3,j0),XOBS(1:3,j0)
      LSIG(j0)=0.1
      TDT(j0)=TDT(j0)-2400000.5d0 ! Just to make the number smaller
   end do
   tspan=maxval(TDT(1:nobsi))-minval(TDT(1:nobsi))
   if (isim<=10) then ! ellipsoid simulations
      read(1,*) dum
      read(1,*) P0(1:12)
      P0(1)=P0(1)/3600.
      P0(2)=P0(2)/rd
      P0(3)=P0(3)/rd
   else ! nonconvex shape simulations
      read(1,*) dum
      read(1,*) dum
      read(1,5) dum,P0(5:7)
      read(1,1) dum,P0(8)
      read(1,2) dum,P0(10)
      read(1,3) dum,P0(1)
      read(1,4) dum,P0(3)
      read(1,4) dum,P0(2)
   endif
   close(1)
1   format(A6,F4.2)
2   format(A8,F6.2)
3   format(A16,F8.6)
4   format(A13,F6.2)
5   format(A17,F1.0,1X,F4.2,1X,F4.2)

   open(1,file="Input/inputData.txt")
   write(1,('(I4)')) nobsi
   write(1,('(A50)')) "time,br,br_err,sun_x,sum_y,sum_z,obs_x,obs_y,obs_z"
   do j0=1,nobsi
      csig=ran2()
      csig=csig*(-1)**int(csig*100)
      write(1,*) TDT(j0),LOBS(j0)*(1.+LSIG(j0)*csig),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
   end do
   close(1)
      
! Start the AVI ellipsoid MCMC workflow:
   open(1,file="Input/ais_FitAll.txt")
   read(7,*) P(1:12)
   write(1,*) P(1:12)
   close(1)
   call cpu_time(cpui)
   call ellipsoid(ecode)
   call cpu_time(cpuf)
   write(4,40) nobsi,cpuf-cpui
40 format(I4,F12.5)
	write(2,*) "--------------------------------------------------------------------------------"
	write(2,*) "Simulated dataset: ",isim,"npts:",nobsi,"T range (years):",tspan/360.
	if (isim<11) write(2,*) "Ellipsoid shape model"
	if (isim>=11) write(2,*) "Nonconvex, spacecraft/radar derived shape model"
	write(2,*) "CPU:",cpuf-cpui
	write(2,'(8(1X,A6))') "car","amtol","nmc","lsigrv","vsc","nmcmc","dchisqc"
	write(2,'(F6.2,1X,E8.2,1X,I5,1X,F6.2,1X,F6.2,1X,I5,1X,F8.2)') car,amtol,nmc,lsigrv,vsc,nmcmc,dchisqc

! Write simulation-wise output files
   if(ecode==60) then
         open (unit=1,file='Output/aes_lsv.txt')
         open (unit=3,file=trim(output)//'_aes_lsv.txt')
   elseif(ecode==-60) then
         open (unit=1,file='Output/aes_mcmc.txt')
         open (unit=3,file=trim(output)//'_aes_mcmc.txt')
   elseif(ecode==-70) then
         open (unit=1,file='Output/aes_is.txt')
         open (unit=3,file=trim(output)//'_aes_is.txt')
   else
     print*,"No solution accepted."
	 goto 30
   endif
   ipas1=0
   ipas2=0
   do while(1==1)
        read(1,*,END=50) fline
        write(3,('(13(F14.6,1X),F10.6)')) fline
		pas1=1
		pas2=1
		do j0=1,12
			if(j0<4.or.(j0>4.and.j0<8)) then
				if (j0>1.and.j0<4) then
					if (isim<=10) then
						if ((abs(P0(j0)-fline(j0))<10.or.abs(abs(P0(j0)-fline(j0))-180.)<10.or.abs(abs(P0(j0)-fline(j0))-360.)<10.)) then
						else
							pas1=0
						endif
					else
						if ((abs(P0(j0)-fline(j0))<15.or.abs(abs(P0(j0)-fline(j0))-180.)<15.or.abs(abs(P0(j0)-fline(j0))-360.)<15.)) then
						else
							pas2=0
						endif				
					endif
				else
					if (isim<=10.and.(j0==6.or.j0==7)) then
						if (abs(P0(j0)-fline(j0))/P0(j0)>0.05) then
							pas1=0
						endif		
					else if (isim>10.and.(j0==6.or.j0==7)) then
						if (abs(P0(j0)-fline(j0))/P0(j0)>0.4) then
							pas2=0
						endif
					else if (j0==1) then
						if (abs(P0(j0)-fline(j0))/P0(j0)>0.1) then
							if(isim<=10) pas1=0
							if(isim>10) pas2=0
						endif
					endif
				endif
			endif
		end do
		if (isim<=10.and.pas1==1) ipas1=1
		if (isim>10.and.pas2==1) ipas2=1
   end do
50 close(1)
   close(3)
   if (isim<=10.and.ipas1==1) then
		npas1=npas1+1
		write(2,*) "PASSED TEST"
   elseif (isim>10.and.ipas2==1) then
		npas2=npas2+1
		write(2,*) "PASSED TEST"
	else
		print*,"Simulation",isim,"Failed"
	endif
!   print*,"npas1,npas2: ",npas1,npas2
30 end do ! Loop over simulations, i.e., lightcurves         
!print*,"npas1 of 10:",npas1
!print*,"npas2 of 12:",npas2
if(npas1==10.and.npas2==12) then
	print*,"Test case SVT-01302 PASSED"
else
	print*,"Test case SVT-01302 FAILED"
end if
close(2)
close(4)
close(7)
end