program callell
! This is just to generate test input data.
! It is assumed that the data is treated as one single lightcurve.
! Here the the time of the first observation is NOT 0, but T(0)-240000.5
! Observations must be in chronological order.

use Randev
use A_ES
implicit none
integer,parameter::tark=selected_real_kind(12,30),maxobs=10000,maxpar=20,npar=12
real(kind=tark),parameter::pi=4.0d0*atan(1.0d0),tp=2.*pi,rd=pi/180.0d0
real(kind=tark),dimension(maxobs)::LOBS,LSIG,TDT
real(kind=tark),dimension(3,maxobs)::XOBS,XSUN,RILL,ROBS
real(kind=tark)::LOBSMN,lsigmn,car,amtol,lsigr,dchisqc,lsigrv,vsc,clat,&
       P(maxpar),P0(maxpar),tspan,csig,cpui,cpuf,fline(14)
integer::nmc,nmcmc,j0,j1,j2,j3,nobsi,isim,nsim1,nsim2,intr1,intr2,&
      mintri1,maxtri1,step1,mintri2,maxtri2,step2
character(len=4) setnr,strsim,dum,isntr1,isntr2
character(len=40) outdir,output
logical :: dir_e

! For testing purposes, output files are coded according to the test case

! Choose which lightcurves are used for the tests
nsim1=8
nsim2=13
setnr='Test'
! Parameters that are sampled in a_is testing: number of triangulation rows
! in period (1) and pole scanning (2)
mintri1=2
maxtri1=4
step1=2
mintri2=6
maxtri2=12
step2=6

! These are provided by the user in the parameter GUI, partly the same as for spin scanning:
! car      ! Lower/upper bound for ellipsoid axial ratio.
! Advanced input from GUI:
! nmcmc      ! number of MCMC solutions
! nmc      ! Number of virtual observation solutions
! amtol      ! Downhill simplex tolerance.
! lsigrv   ! Virtual error for virtual least squares solutions
! vsc      ! rms acceptance threshold for virt ls solutions
! dchisqc   ! limiting delta chi2 for the importance sampler
car=0.35
amtol=1.0d-5
nmc=10 ! n virtual
lsigrv=0.00001
vsc=5.0
nmcmc=10 ! n mcmc
dchisqc=100.

open(1,file="inputParameters.txt")
! Basic input
write(1,10) car,"Lower limit for ellipsoid axis ratios"
! Advanced input
write(1,10) amtol,"Simplex tolerance"
write(1,20) nmc,"Number of virtual MCMC solutions"
write(1,10) lsigrv,"Virtual error"
write(1,20) nmcmc,"Number of MCMC solutions"
write(1,10) dchisqc
write(1,10) vsc
close(1)
10 format(F14.6,5X,A60)
20 format(I5,5X,A60)

! Define the id for the test output files
outdir='Output/Set'//trim(adjustl(setnr))
print*,outdir
outdir=adjustl(outdir)

do isim=nsim1,nsim2 ! Loop over simulated lightcurves
   write(strsim,'(I4)') isim
   strsim=adjustl(strsim)
! Input observations, and the illumination and observation geometry. Write file inputData.txt:
   open(unit=1,file='gavitea/algorithms/TestSet/lc'//trim(strsim)//'.out')
   read (1,*) nobsi
   do j0=1,nobsi
      read(1,*) TDT(j0),LOBS(j0),XSUN(1:3,j0),XOBS(1:3,j0)
      LSIG(j0)=0.001
      TDT(j0)=TDT(j0)-2400000.5d0 ! Just to make the number smaller
   end do
   tspan=maxval(TDT(1:nobsi))-minval(TDT(1:nobsi))
   if (isim<=10) then ! ellipsoid simulations
      read(1,*) dum
      read(1,*) P0(1:12)
      P0(1)=P0(1)/3600.
      P0(2)=P0(2)/rd
       P0(3)=P0(3)/rd
   else ! nonconvex shape simulations
      read(1,*) dum
      read(1,*) dum
      read(1,5) dum,P0(5:7)
      read(1,1) dum,P0(8)
      read(1,2) dum,P0(10)
      read(1,3) dum,P0(1)
      read(1,4) dum,P0(3)
      read(1,4) dum,P0(2)
   endif
   close(1)
1   format(A6,F4.2)
2   format(A8,F6.2)
3   format(A16,F8.6)
4   format(A13,F6.2)
5   format(A17,F1.0,1X,F4.2,1X,F4.2)

   open(1,file="inputData.txt")
   write(1,('(I4)')) nobsi
   write(1,('(A50)')) "time,br,br_err,sun_x,sum_y,sum_z,obs_x,obs_y,obs_z"
   do j0=1,nobsi
      csig=ran2()
      csig=csig*(-1)**int(csig*100)
      write(1,*) TDT(j0),LOBS(j0)*(1.+LSIG(j0)*csig),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
   end do
   close(1)

! Loop over the parameters that are sampled for testing the effect on the result
   do intr1=mintri1,maxtri1,step1 ! The number of pole triangulation rows for period search
      write(isntr1,'(I4)') intr1
      isntr1=adjustl(isntr1)
      do intr2=mintri2,maxtri2,step2  ! The number of pole triangulation rows for pole search
         print*,"Simulation",isim,"ntri1:",intr1,"ntri2:",intr2
         write(isntr2,'(I4)') intr2
         isntr2=adjustl(isntr2)
      
! Start the AVI ellipsoid MCMC workflow:
         output=trim(outdir)//"/Set"//trim(adjustl(setnr))//"_T"//trim(strsim)//"_"//trim(isntr1)//"_"//trim(isntr2)
         output=adjustl(output)
         open(1,file="initialSolution.txt")
         print*,"../A-IS/"//trim(output)//"_a_is_FitAll.out"
         inquire( file='../A-IS/'//trim(output)//"_a_is_FitAll.out", exist=dir_e )
         if(dir_e) then
              open(2,file='../A-IS/'//trim(output)//"_a_is_FitAll.out")
              read(2,*) P(1:12)
              write(1,*) P(1:12)
              close(1)
              close(2)

              call cpu_time(cpui)
              call ellipsoid()
              call cpu_time(cpuf)
         endif
! Write simulation-wise output files
         open (unit=1,file='Output/mcmc1.out')
         open (unit=3,file=trim(output)//'_mcmc1.out')
         do while(1==1)
             read(1,*,END=50) fline
             write(3,('(13(F14.6,1X),F10.6)')) fline
          end do
50         close(1)
         close(3)
         open (unit=1,file='Output/is1.out')
         open (unit=3,file=trim(output)//'_is1.out')
         do while(1==1)
             read(1,*,END=100) fline
             write(3,('(14(E20.14,1X))')) fline
         end do
100         close(1)
         close(3)
        end do
   end do
end do ! Loop over simulations, i.e., lightcurves         
end