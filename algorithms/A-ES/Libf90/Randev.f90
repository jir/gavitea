module Randev

! Random deviates:

! RAN2:  uniform deviates within (0, 1)  (SIRIS)
! RANG2: Gaussian deviates with zero mean and unit standard deviation (SIRIS)

implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark)::RAN2
public::RAN2

contains

FUNCTION RAN2()

integer,save::idum=-1
integer,parameter::IM1=2147483563,IM2=2147483399,IMM1=IM1-1,IA1=40014,IA2=40692,IQ1=53668,IQ2=52774,IR1=12211,&
         IR2=3791,NTAB=32
integer::j,k
integer,save::iv(32)=NTAB*0,iy=0,idum2=123456789
real(kind=tark),parameter::AM=1.0d0/IM1,NDIV=1+IMM1/NTAB,EPS=1.2d-14,RNMX=1.0d0-EPS

if (idum.le.0) then
   idum=max(-idum,1)
   idum2=idum
   do j=NTAB+8,1,-1
      k=idum/IQ1
      idum=IA1*(idum-k*IQ1)-k*IR1
      if (idum.lt.0) idum=idum+IM1
      if (j.le.NTAB) iv(j)=idum
   end do
   iy=iv(1)
endif
k=idum/IQ1
idum=IA1*(idum-k*IQ1)-k*IR1
if (idum.lt.0) idum=idum+IM1
k=idum2/IQ2
idum2=IA2*(idum2-k*IQ2)-k*IR2
if (idum2.lt.0) idum2=idum2+IM2
j=1+iy/NDIV
iy=iv(j)-idum2
iv(j)=idum
if(iy.lt.1)iy=iy+IMM1
RAN2=min(AM*iy,RNMX)
end



subroutine RANG2(r1)

! Returns a normally distributed random deviate with zero mean and unit variance. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-14

integer,save::flag=0
real(kind=tark)::q1,q2
real(kind=tark),save::r2
real(kind=tark),intent(out)::r1

if (flag.eq.1) then
   r1=r2
   flag=0
   return
endif

flag=0
q1=1.1
do while (q1.ge.1.0d0 .or. q1.le.0.0d0)
   r1=2.0d0*RAN2()-1.0d0
   r2=2.0d0*RAN2()-1.0d0
   q1=r1**2+r2**2
end do
q2=sqrt(-2.0d0*log(q1)/q1)
r1=r1*q2
r2=r2*q2
flag=1
end

end module Randev