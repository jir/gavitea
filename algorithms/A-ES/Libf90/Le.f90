module Le

use HG1G2
use viewgeo
use Randev

implicit none
integer,parameter,private::tark=selected_real_kind(12,30),&
         maxobs=500,maxpar=12,&
         npar=12,nini=10
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0),&
                scpd=sqrt(10.0d0),chi2tol=0.05d0
real(kind=tark)::AMETRY
public::AMETRY

contains

subroutine LDISKINTE(SSS,LDI,EILL,EOBS,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
           dphi1a,dphi2a,nd1,nd2,nd3)

! Computation of the normalized disk-integrated brightness for different 
! illumination and observation geometries for a triaxial ellipsoid.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-27

integer,intent(in)::nd1,nd2,nd3
real(kind=tark),intent(in)::SSS(10),EILL(3,3),EOBS(3,3),dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
                                           YL32,YL2_32
real(kind=tark)::alpha,D,a,b,c,pg,G1,G2,G12,idir,odor,cap,sap,ap,alam,clam,slam,&
                 lam,ee,pfr
real(kind=tark),parameter::eetol=0.99999999d0
real(kind=tark),intent(out)::LDI

! Parameters and cosine of phase angle:

a=SSS(1)
b=SSS(2)
c=SSS(3)
pg=SSS(4)
G1=SSS(5)
G2=SSS(6)
D=SSS(7)
ee=EILL(2,1)*EOBS(2,1)+EILL(2,2)*EOBS(2,2)+EILL(2,3)*EOBS(2,3)

! Disk-integrated brightness, separate treatments for phase angles
! of 0 deg and 180 deg:

if (ee.lt.eetol .and. ee.gt.-eetol) then
   alpha=acos(ee)
   idir=sqrt((EILL(2,1)/a)**2+(EILL(2,2)/b)**2+(EILL(2,3)/c)**2)
   odor=sqrt((EOBS(2,1)/a)**2+(EOBS(2,2)/b)**2+(EOBS(2,3)/c)**2)
   cap=(EILL(2,1)*EOBS(2,1)/a**2+EILL(2,2)*EOBS(2,2)/b**2+&
        EILL(2,3)*EOBS(2,3)/c**2)/(idir*odor)
   sap=sqrt(1.0d0-cap**2)
   ap=acos(cap)

   alam=sqrt(idir**2+odor**2+2.0d0*idir*odor*cap)  
   clam=(idir+odor*cap)/alam
   slam=odor*sap/alam
   lam=acos(clam)
   if (slam.le.0.0d0) lam=2.0d0*pi-lam

   G12=G1
   pfr=PHIHG12(G12,alpha,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
               dphi1a,dphi2a,nd1,nd2,nd3)/PHILS(alpha)
   LDI=(0.5d0*D)**2*pi*pg*pfr*a*b*c*idir*odor*(cos(lam-ap)+cos(lam)-&
        sin(lam)*sin(lam-ap)*log(tan(0.5d0*lam)*tan(0.5d0*(ap-lam))))/alam

! Phase angle 0 deg:

elseif (ee.gt.eetol) then
   alpha=0.0d0
   pfr=1.0d0
   idir=sqrt((EILL(2,1)/a)**2+(EILL(2,2)/b)**2+(EILL(2,3)/c)**2)

   LDI=(0.5d0*D)**2*pi*pg*pfr*a*b*c*idir

! Phase angle 180 deg:

else
   LDI=0.0d0
endif
end


subroutine ELSI(P0,PD1,PD0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,&
      YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq1,rms,car,&
      nobs,NLS,ndim,nd1,nd2,nd3)

! Initializes the least-squares Lommel-Seeliger ellipsoid fit to the
! photometric data.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-27

integer,intent(in)::nobs,NLS(maxpar),ndim,nd1,nd2,nd3
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),dimension(maxpar),intent(in)::P0,PD0
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),car,&
                            dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
                                           YL2_32
integer::j1,j2
real(kind=tark)::chisq2,chisqr,P1(maxpar)
real(kind=tark),intent(inout)::LCOM(maxobs),lcommn
real(kind=tark),intent(out)::PD1(maxpar),chisq1,rms

do j1=1,npar
   PD1(j1)=PD0(j1)
end do
do j1=1,ndim
        P1(j1)=P0(NLS(j1))
end do

! Iterate proper initialization deviations for the
! simplex method:

call LECHI2(chisq1,rms,P1,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,&
       YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,NLS,ndim,&
       nd1,nd2,nd3)
      
! The following is implemented slightly differently in A-IS. Why would that be..??      
do j1=1,ndim
        do j2=1,ndim
      P1(j2)=P0(NLS(j2))
   end do
   j2=0
   chisqr=0
   do while (j2<nini.and.chisqr<chi2tol)
      j2=j2+1
      PD1(NLS(j1))=scpd*PD1(NLS(j1))
      P1(j1)=P0(NLS(j1))+PD1(NLS(j1))
      call LECHI2(chisq2,rms,P1,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,&
             XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
             dphi1a,dphi2a,car,nobs,NLS,ndim,nd1,nd2,nd3)
      chisqr=abs(chisq2-chisq1)/chisq1
   end do
end do
end



subroutine ELS(PLS,P0,PD,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,&
          YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,chi2,rms,car,tol,nobs,NLS,ndim,&
          nd1,nd2,nd3)

! Computes the least-squares triaxial ellipsoid fit to the
! photometric data by utilizing the downhill simplex method.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-27

integer,intent(in)::nobs,NLS(maxpar),ndim,nd1,nd2,nd3
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),dimension(maxpar),intent(in)::P0,PD
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
                                            YL32,YL2_32
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),car,&
       tol,dphi1a,dphi2a
real(kind=tark)::y(maxpar),P(maxpar,maxpar),P1(maxpar)
integer::j1,j2,iter
real(kind=tark),intent(inout)::LCOM(maxobs),lcommn
real(kind=tark),intent(out)::chi2,rms,PLS(maxpar)

do j1=1,npar
        PLS(j1)=P0(j1)
end do

! Initialize the simplex:

do j1=1,ndim
   P(1,j1)=P0(NLS(j1))
end do

do j1=2,ndim+1
   do j2=1,ndim
      P(j1,j2)=P(1,j2)
   end do
end do

do j1=1,ndim
   P(j1+1,j1)=P(j1+1,j1)+PD(NLS(j1))
end do

do j2=1,ndim+1
   do j1=1,ndim
      P1(j1)=P(j2,j1)
   end do
   call LECHI2(y(j2),rms,P1,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,&
          YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,NLS,ndim,&
          nd1,nd2,nd3)
end do

! Least-squares solution using the downhill simplex method:

call AME(P,y,tol,iter,ndim,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,&
    YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,rms,car,nobs,NLS,nd1,nd2,nd3)

chi2=y(1)
do j1=1,ndim
        PLS(NLS(j1))=P(1,j1)
end do
end



SUBROUTINE AME(p,y,ftol,iter,ndim,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,&
          YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,rms,car,nobs,NLS,&
          nd1,nd2,nd3)

integer,intent(in)::ndim,nd1,nd2,nd3,nobs,NLS(maxpar)
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
                                           YL32,YL2_32
real(kind=tark),intent(in)::ftol,RILL(3,maxobs),ROBS(3,maxobs),&
       P0(maxpar),car,dphi1a,dphi2a
integer::i,ihi,ilo,inhi,j,m,n
integer,parameter::ITMAX=5000
real(kind=tark)::rtol,sum,swap,ysave,ytry,psum(maxpar)
real(kind=tark),intent(inout)::p(maxpar,maxpar),LCOM(maxobs),lcommn,rms
integer,intent(out)::iter
real(kind=tark),intent(out)::y(maxpar)

iter=0
1 do n=1,ndim
   sum=0.0d0
   do m=1,ndim+1
      sum=sum+p(m,n)
    end do
   psum(n)=sum
end do
2 ilo=1
if (y(1).gt.y(2)) then
   ihi=1
   inhi=2
else
   ihi=2
   inhi=1
endif
do i=1,ndim+1
   if(y(i).le.y(ilo)) ilo=i
   if(y(i).gt.y(ihi)) then
      inhi=ihi
      ihi=i
   else if(y(i).gt.y(inhi)) then
      if(i.ne.ihi) inhi=i
   endif
end do
rtol=2.0d0*abs(y(ihi)-y(ilo))/(abs(y(ihi))+abs(y(ilo)))

if (rtol.lt.ftol) then
   swap=y(1)
   y(1)=y(ilo)
   y(ilo)=swap
   do n=1,ndim
      swap=p(1,n)
      p(1,n)=p(ilo,n)
      p(ilo,n)=swap
   end do
   return
endif
if (iter.ge.ITMAX) then
   return
endif

iter=iter+2

ytry=AMETRY(p,y,psum,-1.0d0,ndim,ihi,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,&
       XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
       dphi1a,dphi2a,rms,car,nobs,NLS,nd1,nd2,nd3)

if (ytry.le.y(ilo)) then

   ytry=AMETRY(p,y,psum,2.0d0,ndim,ihi,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,&
          XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
          dphi1a,dphi2a,rms,car,nobs,NLS,nd1,nd2,nd3)

else if (ytry.ge.y(inhi)) then

   ysave=y(ihi)
   ytry=AMETRY(p,y,psum,0.5d0,ndim,ihi,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,&
          XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
          dphi1a,dphi2a,rms,car,nobs,NLS,nd1,nd2,nd3)

   if (ytry.ge.ysave) then
      do i=1,ndim+1
         if(i.ne.ilo)then
            do j=1,ndim
               psum(j)=0.5d0*(p(i,j)+p(ilo,j))
               p(i,j)=psum(j)
            end do
            call LECHI2(y(i),rms,psum,P0,LOBS,LSIG,LCOM,lcommn,TMJD,&
                   RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
                   YL32,YL2_32,dphi1a,dphi2a,car,nobs,NLS,ndim,&
                   nd1,nd2,nd3)

         endif
      end do
      iter=iter+ndim
      goto 1
   endif
else
   iter=iter-1
endif
goto 2
END



function ametry(p,y,psum,fac,ndim,ihi,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,&
      XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
      dphi1a,dphi2a,rms,car,nobs,NLS,nd1,nd2,nd3)

integer,intent(in)::ihi,ndim,nobs,NLS(maxpar),nd1,nd2,nd3
real(kind=tark),intent(in)::fac,RILL(3,maxobs),ROBS(3,maxobs),P0(maxpar),&
       car,dphi1a,dphi2a
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
                                           YL32,YL2_32
integer::j
real(kind=tark)::fac1,fac2,ytry,ptry(maxpar)
real(kind=tark),intent(inout)::p(maxpar,maxpar),psum(maxpar),y(maxpar),rms,&
                               LCOM(maxobs),lcommn

fac1=(1.0d0-fac)/ndim
fac2=fac1-fac
do j=1,ndim
   ptry(j)=psum(j)*fac1-p(ihi,j)*fac2
end do

call LECHI2(ytry,rms,ptry,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,&
       YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,NLS,ndim,nd1,nd2,nd3)

if (ytry.lt.y(ihi)) then
   y(ihi)=ytry
   do j=1,ndim
      psum(j)=psum(j)-p(ihi,j)+ptry(j)
      p(ihi,j)=ptry(j)
   end do
endif
AMETRY=ytry
return
END



subroutine LECHI2(chi2,rms,P1,P0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,&
        YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,nobs,NLS,ndim,&
        nd1,nd2,nd3)

! Computes the chi-square difference between the observed and
! computed disk-integrated brightnesses using a triaxial ellipsoid.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-27

integer,intent(in)::nobs,NLS(maxpar),ndim,nd1,nd2,nd3
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),P1(maxpar),P0(maxpar),&
       car,dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
                                           YL32,YL2_32
integer::j0,j1,j3
real(kind=tark)::RILL1(3),ROBS1(3),SS(4),EILL(3,3),EOBS(3,3),SSS(10),CEU(3),SEU(3),&
                 P(maxpar),SPIN(4)
real(kind=tark),intent(out)::chi2,rms,LCOM(maxobs),lcommn

! Parameters:

do j1=1,npar
   P(j1)=P0(j1)
end do
do j1=1,ndim
   P(NLS(j1))=P1(j1)
end do

! Sanity verification:

if (P(3).gt.0.5d0*pi .or. P(3).lt.-0.5d0*pi) then
   chi2=1.0d12
        return
endif
if (car.lt.1.0d0) then
   if (P(5).lt.P(6)  .or. P(5).lt.P(7) .or. P(6).lt.P(7) .or.P(7).lt.car*P(5)) then
      chi2=1.0d12
      return
   endif
elseif (car.gt.1.0d0) then
   if (P(5).gt.P(6)  .or. P(5).gt.P(7) .or. P(6).gt.P(7) .or.P(7).gt.car*P(5)) then
      chi2=1.0d12
      return
        endif
endif
 
if (P(8).lt.0.0d0) then
        chi2=1.0d12
        return
endif
 
if (P(9) .lt.0.0d0 .or. P(9) .gt.1.0d0) then
   chi2=1.0d12
   return
endif
 
! Model parameters:

do j1=1,4
        SPIN(j1)=P(j1)
end do
do j1=5,npar
        SSS(j1-4)=P(j1)
end do

! Disk-integrated brightnesses:

lcommn=0.0d0
do j0=1,nobs
   do j3=1,3
      RILL1(j3)=RILL(j3,j0)
      ROBS1(j3)=ROBS(j3,j0)
   end do
   call ILLOBS(SPIN,RILL1,ROBS1,EILL,EOBS,SS,CEU,SEU,TMJD(j0),P(npar))
   call LDISKINTE(SSS,LCOM(j0),EILL,EOBS,XL1,XL2,XL3,YL12,YL2_12,YL22,&
             YL2_22,YL32,YL2_32,dphi1a,dphi2a,nd1,nd2,nd3)
   lcommn=lcommn+LCOM(j0)
end do
lcommn=lcommn/nobs

! Chi-square difference between the observed and computed 
! brightnesses:

chi2=0.0d0
rms=0.0d0
do j0=1,nobs
   chi2=chi2+((LOBS(j0)-LCOM(j0)/lcommn)/LSIG(j0))**2
   rms=rms+(LOBS(j0)-LCOM(j0)/lcommn)**2
end do
rms=sqrt(rms/nobs)
end



subroutine EPROPOSE(P,P0,PV,car,NS,ndim,nmc)

! Proposes new parameters for MCMC.
! 
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-27

integer,intent(in)::NS(maxpar),ndim,nmc
real(kind=tark),intent(in)::PV(maxpar,100000),P0(maxpar),car
integer::j1,k1,k2,nprop=0
real(kind=tark),intent(out)::P(maxpar)

do j1=1,npar
   P(j1)=P0(j1)
end do

! Propose:
nprop=0
100 nprop=nprop+1
if(nprop>1000) call exit(65)
k1=1
k2=k1
do while (k1.eq.k2)
   k1=int(RAN2()*nmc)+1
   k2=int(RAN2()*nmc)+1
end do
do j1=1,ndim
   P(NS(j1))=P0(NS(j1))+PV(NS(j1),k1)-PV(NS(j1),k2)
end do

! Sanity verification:
if (P(3).gt.0.5d0*pi .or. P(3).lt.-0.5d0*pi) goto 100

if (car.lt.1.0d0) then
   if (P(5).lt.P(6).or.P(5).lt.P(7).or.P(6).lt.P(7) .or.&
       P(7).lt.car*P(5)) then
      goto 100
      return
   endif
endif
 
if (P(8).lt.0.0d0) then
   goto 100
endif

if (P(9) .lt.0.0d0 .or. P(9) .gt.1.0d0) then
   goto 100
endif
 
! Enforce pole longitude and rotational phase within [0, 2pi]:

do j1=2,4,2
   if (P(j1).gt.2.0d0*pi)&
      P(j1)=P(j1)-2.0d0*pi*(int((P(j1)-2.0d0*pi)/(2.0d0*pi))+1)
   if (P(j1).lt.0.0d0)&
      P(j1)=P(j1)+2.0d0*pi*(int((2.0d0*pi-P(j1))/(2.0d0*pi)))
end do
end

end module