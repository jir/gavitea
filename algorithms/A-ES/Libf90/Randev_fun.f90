! Randev_fun.f90 - a unit test suite for Randev.f90
!
! funit generated this file from Randev.fun

module Randev_fun

 use Randev

 implicit none

 logical :: noAssertFailed

 public :: test_Randev

 private

 integer :: numTests          = 0
 integer :: numAsserts        = 0
 integer :: numAssertsTested  = 0
 integer :: numFailures       = 0



! DO NOT USE THE WORD T-E-S-T anywhere in the comments!

! The only input parameter is ntri, the number of triangle rows in a half hemisphere.
! For the general case, only the first, last and mid values of MU and PHI are checked.
! For the case where ntri=1, MU and PHI for all the 6 vertices are checked.

! CHANGE THE VALUE OF ntr1 ON LINE 18 TO CHECK ADDITIONAL INPUT VALUES.

! Global variables can be declared here
integer,parameter::tark=selected_real_kind(12,30)
real(kind=tark)::mn_n,mn_g,std_n,std_g

 contains

! Place code here that should be run before each test
  
! Place code here that should be run after each test

 subroutine RAN_output

  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((0.5 &
     +0.01) &
     .ge. &
     (mn_n) &
             .and. &
     (0.5 &
     -0.01) &
     .le. &
     (mn_n) )) then
      print *, " *Assert_Equal_Within failed* in test RAN_output &
              &[Randev.fun:54]"
      print *, "  ", "mn_n (",mn_n,") is not", &
 0.5,"within",0.01
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((0 &
     +0.01) &
     .ge. &
     (mn_g) &
             .and. &
     (0 &
     -0.01) &
     .le. &
     (mn_g) )) then
      print *, " *Assert_Equal_Within failed* in test RAN_output &
              &[Randev.fun:55]"
      print *, "  ", "mn_g (",mn_g,") is not", &
 0,"within",0.01
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((1. &
     +0.01) &
     .ge. &
     (std_g) &
             .and. &
     (1. &
     -0.01) &
     .le. &
     (std_g) )) then
      print *, " *Assert_Equal_Within failed* in test RAN_output &
              &[Randev.fun:56]"
      print *, "  ", "std_g (",std_g,") is not", &
 1.,"within",0.01
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif

  numTests = numTests + 1

 end subroutine RAN_output


 subroutine funit_setup
   real(kind=tark)::cpui,cpuf,s_cpu_n,s_cpu_g
   integer::i,irn
   integer,parameter::nsamp=50000
   real(kind=tark),dimension(nsamp)::ran_n,ran_g
   irn=1
   ran_n(1)=ran2()
   std_n=0
   std_g=0
   s_cpu_n=0
   s_cpu_g=0
   do i=1,nsamp
      call cpu_time(cpui)
      ran_n(i)=RAN2()
      call cpu_time(cpuf)
      s_cpu_n=s_cpu_n+cpuf-cpui
      call cpu_time(cpui)
      call RANG2(ran_g(i))
      call cpu_time(cpuf)
      s_cpu_g=s_cpu_g+cpuf-cpui
   end do
   mn_n=sum(ran_n)/nsamp
   mn_g=sum(ran_g)/nsamp
   do i=1,nsamp
      std_n=std_n+(ran_n(i)-mn_n)
      std_g=std_g+(ran_g(i)-mn_g)**2
   end do
   std_n=std_n/nsamp
   std_g=sqrt(std_g/nsamp)
   print*,'CPU ran2=',s_cpu_n/nsamp
   print*,'CPU rang2=',s_cpu_g/nsamp
  noAssertFailed = .true.
 end subroutine funit_setup


 subroutine funit_teardown
 end subroutine funit_teardown


 subroutine test_Randev( nTests, nAsserts, nAssertsTested, nFailures )

  integer :: nTests
  integer :: nAsserts
  integer :: nAssertsTested
  integer :: nFailures

  continue

  call funit_setup
  call RAN_output
  call funit_teardown

  nTests          = numTests
  nAsserts        = numAsserts
  nAssertsTested  = numAssertsTested
  nFailures       = numFailures

 end subroutine test_Randev

end module Randev_fun
