module Viewgeo

use voper,only:spro,vpro,vroteu

implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark),parameter,private::pi=3.1415927410125732,rd=1.7453293005625408d-2,sd=24.0d0*3600.0d0

contains

subroutine ILLOBS(SPIN,RILL,ROBS,EILL,EOBS,SS,CEU,SEU,tdt,tdt0)

! Compute the solar and observer directions in the ecliptic and
! principal axes reference frames. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-04-27

real(kind=tark),intent(in)::RILL(3),ROBS(3),tdt,tdt0
real(kind=tark),intent(in)::SPIN(4)
integer j1
real(kind=tark),dimension(3)::EU,EI,EO,EI1,EO1,ES,EIO,EN
real(kind=tark)::norm,rphs
real(kind=tark),intent(out)::EILL(3,3),EOBS(3,3),SS(4),CEU(3),SEU(3)

! Euler cosines and sines:

EU(1)=SPIN(2)           ! Euler gamma (Weissbluth, p.54)
EU(2)=0.5d0*pi-SPIN(3)  ! Euler beta
EU(3)=SPIN(4)           ! Euler alpha0

CEU(1)=cos(EU(1))
CEU(2)=cos(EU(2))
SEU(1)=sin(EU(1))
SEU(2)=sqrt(1.0d0-CEU(2)**2)

! Observer and illumination directions in the two reference
! frames; ecliptic reference frame (1):

EI(1)=cos(RILL(2))*cos(RILL(3))
EI(2)=sin(RILL(2))*cos(RILL(3))
EI(3)=sin(RILL(3))
EO(1)=cos(ROBS(2))*cos(ROBS(3))
EO(2)=sin(ROBS(2))*cos(ROBS(3))
EO(3)=sin(ROBS(3))
do j1=1,3
   EILL(1,j1)=EI(j1)
   EOBS(1,j1)=EO(j1)
end do

! Scalar products:

ES(1)=cos(SPIN(2))*cos(SPIN(3))
ES(2)=sin(SPIN(2))*cos(SPIN(3))
ES(3)=sin(SPIN(3))
call SPRO(SS(1),ES,EI)
call SPRO(SS(2),ES,EO)
   
norm=0.0d0
do j1=1,3
   EIO(j1)=EI(j1)+EO(j1)
   norm=norm+EIO(j1)**2
end do
do j1=1,3
   EIO(j1)=EIO(j1)/sqrt(norm)
end do
call SPRO(SS(3),ES,EIO)

call VPRO(EN,EI,EIO)
norm=0.0d0
do j1=1,3
   norm=norm+EN(j1)**2
end do
do j1=1,3
   EN(j1)=EN(j1)/sqrt(norm)
end do
call SPRO(SS(4),ES,EN)
SS(4)=SS(4)/sqrt(1.0d0-SS(3)**2)

! Principal axes reference frame (2):

rphs=SPIN(4)+2.0d0*pi*((tdt-tdt0)*sd/SPIN(1)-int((tdt-tdt0)*sd/SPIN(1)))
CEU(3)=cos(rphs)
SEU(3)=sin(rphs)
call VROTEU(EI,CEU,SEU)
call VROTEU(EO,CEU,SEU)
do j1=1,3
   EILL(2,j1)=EI(j1)
   EOBS(2,j1)=EO(j1)
end do
end

end module