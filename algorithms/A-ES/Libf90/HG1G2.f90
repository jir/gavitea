module HG1G2

implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark),parameter,private::pi=3.1415927410125732,rd=1.7453293005625408d-2
real(kind=tark)::PHIHG12,PHIHG1G2_1,PHIHG1G2_2,PHIHG1G2_3,LSPLINE,phils
public::PHIHG12,PHIHG1G2_1,PHIHG1G2_2,PHIHG1G2_3,LSPLINE,phils

contains

function PHIHG12(G12,alpha,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,&
            nd1,nd2,nd3)

! The H, G12 phase function.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-03-15

integer,intent(in)::nd1,nd2,nd3
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark),intent(in)::G12,alpha,dphi1a,dphi2a
real(kind=tark)::G1,G2,G3
       
if (alpha.gt.2.0d0*pi/3.0) call exit(20) 

if (G12.lt.0.2d0) then
    G1=0.75271388793925d0*G12+0.061644011961942d0
    G2=-0.96122852439935d0*G12+0.62699537444344d0
else
    G1=0.95285549966701d0*G12+0.021615689616390d0
    G2=-0.61248162543453d0*G12+0.55724599465048d0
endif

G3=1.0d0-G1-G2
PHIHG12=G1*PHIHG1G2_1(XL1,YL12,YL2_12,dphi1a,alpha,nd1)+&
   G2*PHIHG1G2_2(XL2,YL22,YL2_22,dphi2a,alpha,nd2)+&
   G3*PHIHG1G2_3(XL3,YL32,YL2_32,alpha,nd3)
end



function PHIHG1G2_1(XL,YL,YL2,dphi,alpha,nd)

! The PHI1 function of the H, G1, G2 magnitude system.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-03-15

integer,intent(in)::nd
real(kind=tark),intent(in)::XL(361),YL(361),YL2(361),dphi,alpha

if (alpha.le.7.5d0*rd) then
   PHIHG1G2_1=1.0d0+dphi*alpha
else
   PHIHG1G2_1=LSPLINE(XL,YL,YL2,alpha,nd)
endif
end



function PHIHG1G2_2(XL,YL,YL2,dphi,alpha,nd)

! The PHI2 function of the H, G1, G2 magnitude system.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-03-15


integer,intent(in)::nd
real(kind=tark),intent(in)::XL(361),YL(361),YL2(361),dphi,alpha

if (alpha.le.7.5d0*rd) then
   PHIHG1G2_2=1.0d0+dphi*alpha
else
   PHIHG1G2_2=LSPLINE(XL,YL,YL2,alpha,nd)
endif
end



function PHIHG1G2_3(XL,YL,YL2,alpha,nd)

! The PHI3 function of the H, G1, G2 magnitude system.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-03-15


integer,intent(in)::nd
real(kind=tark),intent(in)::XL(361),YL(361),YL2(361),alpha

if (alpha.le.30.0d0*rd) then
   PHIHG1G2_3=LSPLINE(XL,YL,YL2,alpha,nd)
else
   PHIHG1G2_3=0.0d0
endif
end



subroutine PHIINI(PHID1,PHID2,PHID3,dphi1a,dphi1b,dphi2a,dphi2b,dphi3,AD1,AD2,AD3,nd1,nd2,nd3)

! Initialization of the interpolation grid for the PHI1, PHI2, and PHI3 
! functions of the revised magnitude system.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-03-15


integer::j1
real(kind=tark)::eps
integer,intent(out)::nd1,nd2,nd3
real(kind=tark),intent(out)::PHID1(0:360),PHID2(0:360),PHID3(0:360),AD1(0:360),AD2(0:360),AD3(0:360),&
       dphi1a,dphi1b,dphi2a,dphi2b,dphi3
logical::lex

nd1=5
AD1(0:nd1)=(/7.5d0,30.0d0,60.0d0,90.0d0,120.0d0,150.0d0/)
AD1(0:nd1)=AD1(0:nd1)*rd

nd2=5
AD2(0:nd2)=(/7.5d0,30.0d0,60.0d0,90.0d0,120.0d0,150.0d0/)
AD2(0:nd2)=AD2(0:nd2)*rd

nd3=8
AD3(0:nd3)=(/0.0d0,0.3d0,1.0d0,2.0d0,4.0d0,8.0d0,12.0d0,20.0d0,30.0d0/)
AD3(0:nd3)=AD3(0:nd3)*rd

inquire(file='./bf.dat',EXIST=lex)
if(.not. lex) call exit(50)
open(unit=1,file='./bf.dat')
do j1=0,nd1
   read (1,*) eps,PHID1(j1)
end do
do j1=0,nd2
   read (1,*) eps,PHID2(j1)
end do
do j1=0,nd3
   read (1,*) eps,PHID3(j1)
end do
read (1,*) dphi1a,dphi1b,dphi2a,dphi2b,dphi3
close(unit=1)
end



function LSPLINE(XL,YL,YL2,ang,nd)

! Spline-interpolating phase function.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-03-15

integer,intent(in)::nd
real(kind=tark),intent(in)::XL(361),YL(361),YL2(361),ang
real(kind=tark)::q

if (ang.le.XL(nd+1)) then
   call SPLINT(XL,YL,YL2,nd+1,ang,q)
   LSPLINE=q
else
   LSPLINE=0.0d0
endif
end



subroutine LSPLINEDI(L,dla,dlb,AD,XL,YL,YL2,nd)

! Spline initialization.

! Author Johanna Torppa (based on code by Karri Muinonen)
! Version 2016-03-18

integer,intent(in)::nd
real(kind=tark),intent(in)::L(0:360),AD(0:360),dla,dlb
integer::j1
real(kind=tark),intent(out)::XL(361),YL(361),YL2(361)

! Compute the spline representation:

do j1=1,nd+1
   XL(j1)=AD(j1-1)
   YL(j1)=L(j1-1)
end do
call SPLINE(XL,YL,nd+1,dla,dlb,YL2)
end



SUBROUTINE spline(x,y,n,yp1,ypn,y2)

integer,intent(in)::n
real(kind=tark),intent(in)::x(n),y(n),yp1,ypn
integer::i,k
integer,parameter::NMAX=500
real(kind=tark)::p,qn,sig,un,u(NMAX)
real(kind=tark),intent(out)::y2(n)

if(n<3) call exit(31)
if (yp1.gt.0.99d30) then ! Vertical
  y2(1)=0.0d0
  u(1)=0.0d0
else
  y2(1)=-0.5d0
  u(1)=(3.0d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
endif
do i=2,n-1
  sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
  p=sig*y2(i-1)+2.0d0
  y2(i)=(sig-1.0d0)/p
  u(i)=(6.0d0*((y(i+1)-y(i))/(x(i+1)-&
       x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
end do
if (ypn.gt.0.99d30) then
  qn=0.0d0
  un=0.0d0
else
  qn=0.50d0
  un=(3.0d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
endif
y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.0d0)
do k=n-1,1,-1
  y2(k)=y2(k)*y2(k+1)+u(k)
end do
end


SUBROUTINE splint(xa,ya,y2a,n,x,y)

integer,intent(in)::n
real(kind=tark),intent(in)::x,xa(n),y2a(n),ya(n)
integer::k,khi,klo
real(kind=tark)::a,b,h
real(kind=tark),intent(out)::y

if(n<3) call exit(32)
klo=1
khi=n
1 if (khi-klo.gt.1) then
  k=(khi+klo)/2
  if(xa(k).gt.x)then
    khi=k
  else
    klo=k
  endif
goto 1
endif
h=xa(khi)-xa(klo)
if (h.eq.0.0D0) call exit(30)
a=(xa(khi)-x)/h
b=(x-xa(klo))/h
y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.0D0
return
END



function phils(alpha)

! Lommel-Seeliger phase curve.

! Author Johanna Torppa (based on code by Karri Muinonen)
! Version 2016-03-17

real(kind=tark)::alpha

if(alpha<=0) call exit(40)
if(alpha>pi) call exit(40)

phils=1.0d0-sin(0.5d0*alpha)*tan(0.5d0*alpha)*log(1.0d0/tan(0.25d0*alpha))
end function phils

end module HG1G2
