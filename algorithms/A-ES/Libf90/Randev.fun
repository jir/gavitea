test_suite Randev

! DO NOT USE THE WORD T-E-S-T anywhere in the comments!

! The only input parameter is ntri, the number of triangle rows in a half hemisphere.
! For the general case, only the first, last and mid values of MU and PHI are checked.
! For the case where ntri=1, MU and PHI for all the 6 vertices are checked.

! CHANGE THE VALUE OF ntr1 ON LINE 18 TO CHECK ADDITIONAL INPUT VALUES.

! Global variables can be declared here
integer,parameter::tark=selected_real_kind(12,30)
real(kind=tark)::mn_n,mn_g,std_n,std_g

! Place code here that should be run before each test
setup
   real(kind=tark)::cpui,cpuf,s_cpu_n,s_cpu_g
   integer::i,irn
   integer,parameter::nsamp=50000
   real(kind=tark),dimension(nsamp)::ran_n,ran_g
   irn=1
   ran_n(1)=ran2()
   std_n=0
   std_g=0
   s_cpu_n=0
   s_cpu_g=0
   do i=1,nsamp
      call cpu_time(cpui)
      ran_n(i)=RAN2()
      call cpu_time(cpuf)
      s_cpu_n=s_cpu_n+cpuf-cpui
      call cpu_time(cpui)
      call RANG2(ran_g(i))
      call cpu_time(cpuf)
      s_cpu_g=s_cpu_g+cpuf-cpui
   end do
   mn_n=sum(ran_n)/nsamp
   mn_g=sum(ran_g)/nsamp
   do i=1,nsamp
      std_n=std_n+(ran_n(i)-mn_n)
      std_g=std_g+(ran_g(i)-mn_g)**2
   end do
   std_n=std_n/nsamp
   std_g=sqrt(std_g/nsamp)
   print*,'CPU ran2=',s_cpu_n/nsamp
   print*,'CPU rang2=',s_cpu_g/nsamp
end setup
  
! Place code here that should be run after each test
teardown
end teardown

test RAN_output
  assert_equal_within(mn_n,0.5,0.01)
  assert_equal_within(mn_g,0,0.01)
  assert_equal_within(std_g,1.,0.01)
end test

end test_suite