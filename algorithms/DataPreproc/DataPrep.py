#!/opt/local/bin/python2.7

def solve_kepler_equation( kep, t1 ):
    
    "Iterates Kepler's equation for input Keplerian orbital elements"
    "and epoch with accelerated Newton's method, and returns the" 
    "eccentric anomaly at the given time. If an error occurs, the"
    "subroutine returns a negative eccentric anomaly."
    ""
    "References:"
    ""
    "J. M. A. Danby: 'Fundamentals of Celestial mechanics' pp. 149-154"

    import math

    nmax = 10000;
    k = 0.85;
    tol = 1.0e-14;
    ea = -99999999999999.9;
    planetary_mu = 0.295912208285591095e-03;
    
    # Find initial guess:
    ma = kep[5] + math.sqrt( planetary_mu/kep[0]**3 ) * (t1-kep[6]);
    ma = ma%(2*math.pi);
    sigma = math.copysign( 1, math.sin(ma) );
    x = ma + sigma*k*kep[1];

    # Solve Kepler's equation iteratively using Newton's accelerated
    # method:
    i = 1;
    esinx = kep[1]*math.sin(x);
    f = x - esinx - ma;
    while (math.fabs(f) >= tol):
        if (i > nmax):
            print "solve_kepler_equation : uncertain convergence."
            ea = -1.0;
            break
        ecosx = kep[1]*math.cos(x);
        fp    = 1.0 - ecosx;
        fpp   = esinx;
        fppp  = ecosx;
        dx    = -f/fp;
        dx    = -f/(fp+0.5*dx*fpp);
        dx    = -f/(fp+0.5*dx*fpp+dx*dx*fppp/6.0);
        x     = x + dx;
        esinx = kep[1]*math.sin( x );
        f     = x - esinx - ma;
        i     = i + 1;
    else:
        ea = x%(2*math.pi);

    return ea;




def transformation_matrix( kep ):

  "Returns the transformation matrix from polar coordinate"
  "system to the ecliptical one."

  import math;
  import numpy;
  
  sin_angles = [ math.sin(kep[2]), math.sin(kep[3]), math.sin(kep[4]) ];
  cos_angles = [ math.cos(kep[2]), math.cos(kep[3]), math.cos(kep[4]) ];

  mat = [[cos_angles[1]*cos_angles[2] - sin_angles[1]*sin_angles[2]*cos_angles[0],
                       -(cos_angles[1]*sin_angles[2] + sin_angles[1]*cos_angles[2]*cos_angles[0]),
                       sin_angles[1]*sin_angles[0]],
                      [sin_angles[1]*cos_angles[2] + cos_angles[1]*sin_angles[2]*cos_angles[0],
                       -(sin_angles[1]*sin_angles[2] - cos_angles[1]*cos_angles[2]*cos_angles[0]),
                       -cos_angles[1]*sin_angles[0]],
                      [sin_angles[2]*sin_angles[0],
                       cos_angles[2]*sin_angles[0],
                       cos_angles[0]]];

  return mat;




def el_in_crd_out(kep,car6):
  "Takes one set of orbital elements as input"
  "and returns the corresponding ecliptical coordinates"
  
  import sys;       
  import math;	
  import numpy;

  car = range(7)
  planetary_mu = 0.295912208285591095e-03;

  car[6]=car6

# Compute necessary quantities
  ea = solve_kepler_equation( kep[:], car[6] );
  cea = math.cos( ea );
  sea = math.sin( ea );
  b = kep[0] * math.sqrt( 1.0 - kep[1]**2 );
  dot_ea = math.sqrt( planetary_mu/kep[0]**3 ) / (1.0 - kep[1]*cea);

# Keplerian elements to polar Cartesian elements:
#  -positions:
  car[0] = kep[0]*(cea - kep[1]);
  car[1] = b*sea;
  car[2] = 0.0;
#  -velocities:
  car[3] = -kep[0]*dot_ea*sea;
  car[4] = b*dot_ea*cea;
  car[5] = 0.0;

# Polar Cartesian elements to ecliptical Cartesian elements:
  R = transformation_matrix( kep );
  car[0:3] = numpy.dot(R,car[0:3]);
  car[3:6] = numpy.dot(R,car[3:6]);
  return car;
  
  
import math
import numpy
import sys
"Reads asteroid's orbital elements (elements.txt) and input photometric data (gaiadata.txt)."
"Runs el_in_crd_out to transform orbital elements to ecliptic cartesian coordinates."
"Outputs all coordinates in equatorial reference frame"
"Does light-time correction to the input epochs"

#------------------------------------------------------------------------------------------
# The input files here should be aligned with the format of data from the Gaia archive.
# The following is just an example how reading input might be done, based on the simulated
# asteroid data from the Observatory of Code d'Azur Gaia asteroid simulator.

# Input orbital elements of the asteroid, and the corresponding epoch are read from file
# "elements.txt" as a 7-element vector kep=[a,e,i,node,argperi,meananom0,epoch0].
# "i,node,argperi and meananom0" should be given in degrees.
# "a" should be given in the same units as the x,y,z ecliptic Cartesian coordinates
# of Gaia in the datafile.
# "epoch0" should be Julian Date

# Read asteroid's orbital elements
with open("elements.txt") as file:
     kep_in = [[float(digit) for digit in line.split()] for line in file]
kep_in = numpy.array(kep_in)+0.
if numpy.isnan(numpy.min(kep_in[:])): # If NaNs, should not be used
	sys.exit("---------------------------"+"\n"+"Asteroid data cannot be analyzed, since"+
	           "\n"+"orbital element vector contains NaN values"+"\n"+
			   "----------------------------")
kep=range(7)
kep=kep_in[0][:]
# Inclination, node, argperi and meananom0 given in deg and transformed to radians
kep[2] = math.radians(kep_in[0][2]);
kep[3] = math.radians(kep_in[0][3]);
kep[4] = math.radians(kep_in[0][4]);
kep[5] = math.radians(kep_in[0][5]);

# Input photometric data is expected to be in file "gaiadata.txt".
# Input data is stored in matrix datamat with ndat rows and columns as:
# datamat[,0]		observing epochs in days Gaia time 
#					(zero point 01/01/2010, i.e., JD 2455197.5)
# datamat[,1]		observed brightnesses (in brightness units, not magnitudes)
# datamat[,2]		observing errors (in brightness units, not magnitude or relative)
# datamat[,3:5]		Gaia's x/y/z coordinates (equatorial cartesian coordinates in km)

# Read gaia photometric data
with open("gaiadata.txt") as file:
     datamat = [[float(digit) for digit in line.split()] for line in file]
datamat = numpy.array(datamat)+0.
shapemat=numpy.shape(datamat)
ndat=shapemat[0]
outmat=numpy.zeros((ndat,9))
iout=-1
eps=23.43712*math.pi/180.
ecl2eq=[[1,0,0],[0,math.cos(eps),-math.sin(eps)],[0,math.sin(eps),math.cos(eps)]]
for i in range(0,ndat):
  epoch=datamat[i][0]+2455197.5
  car=el_in_crd_out(kep,epoch) # car contains x, y, z, dx/dt, dy/dt, dz/dt, epoch
  car=numpy.array(car)
  astsun=-numpy.dot(ecl2eq,numpy.array(car[0:3]))
  astgaia=datamat[i][3:6]/149597870.7+astsun
  sl=2.99792458*10**5*3600.*24./149597870.7 # speed of light from km/s to au/d
  epoch=epoch-math.sqrt(numpy.dot(astgaia,astgaia))/sl # Light-time correction
  if numpy.isnan(numpy.min(datamat[i,:])): # Rows containing NaNs should not be used
    continue
  iout=iout+1
  outmat[iout,0]=epoch
  outmat[iout,1:2]=datamat[i,1:2]
  if outmat[iout,1] < 0.: # If negative brightness values occur, this should be executed
	 outmat[iout,1]=0.0000001
  outmat[iout,3:6]=astsun[:]
  outmat[iout,6:9]=astgaia[:]

head="time,br,br_err,sun_x,sun_y,sun_z,obs_x,obs_y,obs_z"
with open('inputData.txt','w') as file:
    file.write(str(iout+1)+"\n")
with open('inputData.txt','a') as file:
    numpy.savetxt(file,outmat[0:iout,:],header=head)







