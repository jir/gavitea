#!/opt/local/bin/python2.7

def solve_kepler_equation( kep, t1 ):
    
    "Iterates Kepler's equation for input Keplerian orbital elements"
    "and epoch with accelerated Newton's method, and returns the" 
    "eccentric anomaly at the given time. If an error occurs, the"
    "subroutine returns a negative eccentric anomaly."
    ""
    "References:"
    ""
    "J. M. A. Danby: 'Fundamentals of Celestial mechanics' pp. 149-154"

    import math

    nmax = 10000;
    k = 0.85;
    tol = 1.0e-14;
    ea = -99999999999999.9;
    planetary_mu = 0.295912208285591095e-03;
    
    # Find initial guess:
    ma = kep[5] + math.sqrt( planetary_mu/kep[0]**3 ) * (t1-kep[6]);
    ma = ma%(2*math.pi);
    sigma = math.copysign( 1, math.sin(ma) );
    x = ma + sigma*k*kep[1];

    # Solve Kepler's equation iteratively using Newton's accelerated
    # method:
    i = 1;
    esinx = kep[1]*math.sin(x);
    f = x - esinx - ma;
    while (math.fabs(f) >= tol):
        if (i > nmax):
            print "solve_kepler_equation : uncertain convergence."
            ea = -1.0;
            break
        ecosx = kep[1]*math.cos(x);
        fp    = 1.0 - ecosx;
        fpp   = esinx;
        fppp  = ecosx;
        dx    = -f/fp;
        dx    = -f/(fp+0.5*dx*fpp);
        dx    = -f/(fp+0.5*dx*fpp+dx*dx*fppp/6.0);
        x     = x + dx;
        esinx = kep[1]*math.sin( x );
        f     = x - esinx - ma;
        i     = i + 1;
    else:
        ea = x%(2*math.pi);

    return ea;




def transformation_matrix( kep ):

  "Returns the transformation matrix from polar coordinate"
  "system to the ecliptical one."

  import math;
  import numpy;
  
  sin_angles = [ math.sin(kep[2]), math.sin(kep[3]), math.sin(kep[4]) ];
  cos_angles = [ math.cos(kep[2]), math.cos(kep[3]), math.cos(kep[4]) ];

  mat = [[cos_angles[1]*cos_angles[2] - sin_angles[1]*sin_angles[2]*cos_angles[0],
                       -(cos_angles[1]*sin_angles[2] + sin_angles[1]*cos_angles[2]*cos_angles[0]),
                       sin_angles[1]*sin_angles[0]],
                      [sin_angles[1]*cos_angles[2] + cos_angles[1]*sin_angles[2]*cos_angles[0],
                       -(sin_angles[1]*sin_angles[2] - cos_angles[1]*cos_angles[2]*cos_angles[0]),
                       -cos_angles[1]*sin_angles[0]],
                      [sin_angles[2]*sin_angles[0],
                       cos_angles[2]*sin_angles[0],
                       cos_angles[0]]];

  return mat;




import sys;       
import math;
import numpy;

kep = range(7)
car = range(7)
planetary_mu = 0.295912208285591095e-03;

# Read input and convert input degrees to radians
#  Semimajor axis [au]
kep[0] = float( sys.argv[1] )
#  Eccentricity
kep[1] = float( sys.argv[2] )
#  Inclination [deg]
kep[2] = math.radians( float( sys.argv[3] ) );
#  Longitude of ascending node [deg]
kep[3] = math.radians( float( sys.argv[4] ) );
#  Argument of perihelion [deg]
kep[4] = math.radians( float( sys.argv[5] ) );
#  Mean anomaly [deg]
kep[5] = math.radians( float( sys.argv[6] ) );
#  Epoch of input Keplerian elements
kep[6] = float( sys.argv[7] )
#  Desired epoch of output Cartesian elements
car[6] = float( sys.argv[8] )

# Compute necessary quantities
ea = solve_kepler_equation( kep[:], car[6] );
cea = math.cos( ea );
sea = math.sin( ea );
b = kep[0] * math.sqrt( 1.0 - kep[1]**2 );
dot_ea = math.sqrt( planetary_mu/kep[0]**3 ) / (1.0 - kep[1]*cea);

# Keplerian elements to polar Cartesian elements:
#  -positions:
car[0] = kep[0]*(cea - kep[1]);
car[1] = b*sea;
car[2] = 0.0;
#  -velocities:
car[3] = -kep[0]*dot_ea*sea;
car[4] = b*dot_ea*cea;
car[5] = 0.0;

# Polar Cartesian elements to ecliptical Cartesian elements:
R = transformation_matrix( kep );
car[0:3] = numpy.dot(R,car[0:3]);
car[3:6] = numpy.dot(R,car[3:6]);

print car;





