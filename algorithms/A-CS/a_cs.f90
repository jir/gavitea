module acs

! Uses units 1 and 10

use HG1G2
use Corrfunc
use Discrets
use Ellips
use Randev
use Gsg
use LM
use Numint
use minko
use flush_progress
implicit none
integer,parameter,private::tark=selected_real_kind(12,30),&
         nparEll=12,&
         maxobs=500,&
         maxpar=2000,&
         maxres=5,&
         MMAX=2000,&
         nini=10,&
         muphi=130000,&
         itsize=260000,&
         itmax=100,&
         nmax=6000,&
         nedmax=500,&
         ntr_m=3
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0),&
         tp=2.*pi,&
         rd=pi/180.0d0,&
         sd=24.0d0*3600.0d0,&
         tol=1.0d-14,&
         EPS=3.0d-14,&
         scpd=sqrt(10.0d0),&
         chi2tol=0.05d0,&
         tiny=1e-8,&
         epsilon=.001
real(kind=tark),dimension(0:nmax),private::x,y,z
integer,private::numfaces,NCF(maxpar,2),nacf
common/points/x,y,z/nf/numfaces/ncfvec/NCF/nacoef/nacf


contains

subroutine a_cs(ecode)

! A_CS computes photometric brightnesses for asteroids modeled as spherical harmonics
! series representation of their gaussian curvature
! and solves the lightcurve inversion problem using optimization,
! virtual observations, virtual-observation Markov-Chain Monte-Carlo method (MCMC)
! and virtual-observation MCMC importance sampling (subroutine NSHI). 
! The shape of the asteroid is obtained from the facet area information using
! the Minkowski solver (subroutine Minkowski).

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer::lmin,lmax,niter,ntr,nmcmc,nmc,i,j,k,l,lb,dumi,jp,npar
real(kind=tark),dimension(nmax)::initarea
real(kind=tark)::lsigr,dchisqc,lsigrv,nut,dumr
real(kind=tark),dimension(130000)::MUN,PHIN
real(kind=tark),dimension(260000)::MUT,PHIT,AR,AT
real(kind=tark),dimension(260000,3)::NT
real(kind=tark)::AX(6),rmax,XN(130000,3),DAR(7200,256),atot,coef,&
            ACF(0:256,0:256),BCF(0:256,0:256),nor(nmax,3)
real(kind=tark),allocatable,dimension(:)::Pout
integer::IT(260000,3),ntri,nnod,numedges(10),edge(10,10),sal,nsol
integer,intent(out)::ecode
character(len=1) dumc
character(len=200)::indir,outdir

call get_command_argument(1,indir)
call get_command_argument(2,outdir)

! AX elements: 1-4 are equator (theta=0) dimensions at phi=0,90,180 and 270 deg.
! 5,6 are c dimensions at north and south poles 
! Input observations, and the illumination and observation 
! geometry:

! Check how lsig is defined. Is it relative or does it have to be normalized along
! with LOBS 

! Start the AVI convex shape MCMC workflow:
call NSHI(npar,ecode,indir,outdir)

! MINKOWSKI
! A program for determining a convex polyhedron from its EGI by 
! Minkowski. Writes every 20th result to file minkotmp.

10 call TRIDS(MUN,PHIN,IT,nnod,ntri,ntr_m)
! MUN,PHIN=node radius spherical coordinates
do i=1,6
    AX(i)=1.0d0 ! Dimensions for a sphere
end do
call RELLTD(XN,NT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
! XN=node coordinates, NT=unit triangle normals

call ARELLTD(AT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
do i=1,ntri
   MUT(i)=NT(i,3)
   nut=sqrt(1.0d0-MUT(i)**2)
   PHIT(i)=acos(NT(i,1)/nut)
   if (NT(i,2).lt.0.0d0) PHIT(i)=2.0d0*pi-PHIT(i)
   nor(i,:)=NT(i,:)
end do
! MUT,PHIT = facet normal spherical coordinates
numfaces=ntri

open(1,file=trim(indir)//"/acs_inputParameters.txt")
read(1,*) nmc
read(1,*) nmcmc
read(1,*) lmin
read(1,*) lmax
read(1,*) dumi
read(1,*) dumi
read(1,*) dumr
read(1,*) dumr
read(1,*) dumr
close(1)

if(ecode==60) then
  open (unit=10,file=trim(outdir)//'/acs_lsv.txt')
  nsol=nmc
elseif(ecode==-60) then
  open (unit=10,file=trim(outdir)//'/acs_mcmc.txt')
  nsol=nmcmc
elseif(ecode==-70) then
  open (unit=10,file=trim(outdir)//'/acs_is.txt')
  nsol=nmcmc
else
  print*,"What's going on???"
  return
endif
read(10,*) dumc
jp=9
do i=lmin,lmax
   do j=0,i
      jp=jp+1
   end do
end do
nacf=jp
allocate(Pout(npar),STAT=sal)
if(sal/=0) then
!  print*,"Allocation failed in a_cs"
  stop
endif

do i=1,nsol
!	print*,i,nsol
   read(10,*) Pout
   l=9
   lb=nacf
   do j=lmin,lmax
      do k=0,j
         l=l+1
         ACF(j,k)=Pout(l)
         if (k>0) then
            lb=lb+1
            BCF(j,k)=Pout(lb)
         endif
      end do
   end do
   call ARDAR(AR,DAR,ACF,BCF,AT,MUT,PHIT,npar,ntri,lmin,lmax)
! Scale the areas of a shape to make a reasonably sized object.
   do j=1,ntri
          atot=atot+AR(j)
   end do
   coef=1000./atot
   do j=1,ntri
       initarea(j)=coef*AR(j)
   end do
   call Minkowski(initarea,nor,i,outdir)
end do 
close(10)
deallocate(Pout,STAT=sal)
if(sal/=0) then
!  print*,"Deallocation failed in a_cs"
  stop
endif
end



subroutine NSHI(npar,ecode,indir,outdir)
 
! NSHI solves the lightcurve inverse problem using optimization and 
! Markov-Chain Monte-Carlo methods with spherical harmonics expansion
! of the Gaussian surface density. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-29

integer,intent(inout)::ecode
integer::nobs,PID(maxpar),ndim,ntr,niter,nmc,nmcmc,lmin,lmax,j0,j1,j2,&
         nd1,nd2,nd3
real(kind=tark),dimension(maxobs)::LOBS,LSIG,TMJD,LCOM
real(kind=tark),dimension(3,maxobs)::XSUN,XOBS
real(kind=tark)::PLS(maxpar),P(maxpar),PD(maxpar),lrms,lcommn,RILL(3,maxobs),&
       ROBS(3,maxobs),SB(4,2),SSS(10),lsigr,lsigrv,chisq,dchisqc,&
       dphi1a,dphi1b,dphi2a,dphi2b,dphi3a,dphi3b,clat,lobsmn
real(kind=tark),dimension(0:360)::PHID1,PHID2,PHID3,AD1,AD2,AD3
real(kind=tark),dimension(361)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
character(len=70) header,dumc
integer,intent(out)::npar
character(len=200)::indir,outdir

open(1,file=trim(indir)//"/inputData.txt")
read(1,*) nobs
read(1,*) header
do j0=1,nobs
   read(1,*) TMJD(j0),LOBS(j0),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
end do
close(1)
! Initial model parameter values obtained from spin scanning:
open(1,file=trim(outdir)//'/ais_FitAll.txt')
read(1,*) dumc
read(1,*) (P(j0),j0=1,nparEll)
close(1)
P(1)=P(1)*3600.
do j0=2,4
   P(j0)=P(j0)*rd
end do
if (P(2).gt.tp)    P(2)=P(2)-tp*(int((P(2)-tp)/(tp))+1)
if (P(2).lt.0.0d0) P(2)=P(2)+tp*(int((tp-P(2))/(tp)))
if (P(4).gt.tp)    P(4)=P(4)-tp*(int((P(4)-tp)/(tp))+1)
if (P(4).lt.0.0d0) P(4)=P(4)+tp*(int((tp-P(4))/(tp)))

open(1,file=trim(indir)//"/acs_inputParameters.txt")
read(1,*) nmc
read(1,*) nmcmc
read(1,*) lmin
read(1,*) lmax
read(1,*) ntr
read(1,*) niter
read(1,*) lsigrv
read(1,*) dchisqc
read(1,*) lsigr
close(1)

! Input observations, and the illumination and observation geometry:
do j1=1,nobs
   lobsmn=lobsmn+LOBS(j1)
end do
lobsmn=lobsmn/nobs
do j1=1,nobs
   LOBS(j1)=LOBS(j1)/lobsmn
!    LSIG0(j1)=LSIG0(j1)/lobsmn
   ROBS(1,j1)=sqrt(XOBS(1,j1)**2+XOBS(2,j1)**2+XOBS(3,j1)**2)
   ROBS(3,j1)=asin(XOBS(3,j1)/ROBS(1,j1))
   clat=cos(ROBS(3,j1))
   ROBS(2,j1)=acos(XOBS(1,j1)/(ROBS(1,j1)*clat))
   if (XOBS(2,j1)/clat.lt.0.0d0) ROBS(2,j1)=2.0d0*pi-ROBS(2,j1)
   RILL(1,j1)=sqrt(XSUN(1,j1)**2+XSUN(2,j1)**2+XSUN(3,j1)**2)
   RILL(3,j1)=asin(XSUN(3,j1)/RILL(1,j1))
   clat=cos(RILL(3,j1))
   RILL(2,j1)=acos(XSUN(1,j1)/(RILL(1,j1)*clat))
   if (XSUN(2,j1)/clat .lt.0.0d0) RILL(2,j1)=2.0d0*pi-RILL(2,j1)
end do

! Input ellipsoid parameters for spin, shape, scattering, and size:

! P(1)  = rotation period           (s;   given in d in ptd.in) 
! P(2)  = pole longitude            (rad; given in deg in ptd.in)
! P(3)  = pole latitude             (rad; given in deg in ptd.in)
! P(4)  = rotational phase          (rad; given in deg in ptd.in)
! P(5)  = ellipsoid axis a          (relative units)
! P(6)  = ellipsoid axis b          (relative units)
! P(7)  = ellipsoid axis c          (relative units) 
! P(8)  = geometric albedo
! P(9)  = G1 parameter
! P(10) = G2 parameter
! P(11) = ellipsoid axis a          (km)
! P(12) = epoch of rotational phase (MJD)

do j1=5,9
   SSS(j1-1)=P(j1+3)   ! Scattering, Size, Epoch
end do
! Regularization "observations":
LOBS(nobs+1)=0.0d0
LOBS(nobs+2)=0.0d0
LOBS(nobs+3)=0.0d0
LSIG(nobs+1)=1.0d-2*lsigr
LSIG(nobs+2)=1.0d-2*lsigr
LSIG(nobs+3)=1.0d-2*lsigr

! Initial parameter deviations for the preconditioning of the
! simplex least-squares iteration:

npar=4+(lmax+1)**2-lmin**2+5

PD(1)=0.5d-2*P(1)**2/(sd*(TMJD(nobs)-TMJD(1)))
PD(2)=1.0d-1*rd
PD(3)=1.0d-1*rd
PD(4)=1.0d-1*rd
PD(5)=1.0d-4
PD(6)=1.0d-4
PD(7)=1.0d-4
PD(8)=1.0d-4
PD(9)=1.0d-3
do j0=10,npar
        PD(j0)=1.0d-4
end do

! Initialization of the H, G1, G2 phase function:

dphi3b=0.0d0
call PHIINI(PHID1,PHID2,PHID3,dphi1a,dphi1b,dphi2a,dphi2b,dphi3a,&
       AD1,AD2,AD3,nd1,nd2,nd3)
call LSPLINEDI(PHID1,dphi1a,dphi1b,AD1,XL1,YL12,YL2_12,nd1)
call LSPLINEDI(PHID2,dphi2a,dphi2b,AD2,XL2,YL22,YL2_22,nd2)
call LSPLINEDI(PHID3,dphi3a,dphi3b,AD3,XL3,YL32,YL2_32,nd3)

! Least-squares optimization using the Levenberg-Marquardt method:
! Parameters to be optimized or sampled:

!print*,'Define initial spherical harmonis coefficients'
!print*,'-----------------------------------------'

PID=0
! free: period,pole,size,zero phase.
! fixed: geom alb, g1, (g2), size, tmjd0 (These cannot be free, since derivatives are not computed)
PID(1:4)=1
PID(10:npar)=1

ndim=0
do j0=1,npar
  if (PID(j0).gt.0) ndim=ndim+1
end do
! Keep initial spin parameters, and compute spherical harmonics coefficients
! corresponding to the initial ellipsoid solution 
call LS(PLS,P,PD,SSS,LOBS,LSIG,lrms,LCOM,lcommn,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,&
   YL2_12,YL22,YL2_22,YL32,YL2_32,chisq,dphi1a,dphi2a,nobs,PID,&
   ndim,npar,ntr,niter,lmin,lmax,nd1,nd2,nd3)

!print*,'Completed spherical harmonics coefficient initialization'
!print*,'---------------------------------------------'

! Compute virtual solutions

!print*,'BEGIN VIRTUAL SOLUTION COMPUTATION'
!print*,'-----------------------------------'

call LSV(P,PD,SSS,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,&
    YL22,YL2_22,YL32,YL2_32,chisq,dphi1a,dphi2a,lsigrv,nobs,PID,&
    ndim,npar,ntr,niter,nmc,lmin,lmax,nd1,nd2,nd3,outdir)

!print*,'Completed virtual solution computation'
!print*,'---------------------------------------------'


! Compute importance sampler

!print*,'BEGIN IMPORTANCE SAMPLER'
!print*,'-------------------------'
ecode=0
call IS(PLS,SSS,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
   YL32,YL2_32,dphi1a,dphi2a,dchisqc,nobs,PID,&
   ndim,npar,ntr,nmcmc,nmc,lmin,lmax,nd1,nd2,nd3,ecode,outdir)
if (ecode==0) then
!   print*,'Completed importance sampling'
   ecode=-70
   open(1,file=trim(outdir)//"/acs_outf_name.txt")
   write(1,*) "acs_is.txt"
   close(1)
   open(10,file="./acs_mcmc_progress.txt")
   write(10,*) 0
   close(10)
   return
endif
!print*,'----------------------------'

! Compute markov chain monte carlo solutions

!print*,'BEGIN MCMC COMPUTATION'
!print*,'----------------------'

ecode=0
call MCMC(PLS,SSS,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,&
     YL2_22,YL32,YL2_32,dphi1a,dphi2a,nobs,PID,&
     ndim,npar,ntr,nmcmc,nmc,lmin,lmax,nd1,nd2,nd3,ecode,outdir)
if (ecode==0) then
!   print*,'Completed MCMC sampling'
   ecode=-60
   open(1,file=trim(outdir)//"/acs_outf_name.txt")
   write(1,*) "acs_mcmc.txt"
   close(1)
   return
endif
!print*,'--------------------------'
open(1,file=trim(outdir)//"/acs_outf_name.txt")
write(1,*) "acs_lsv.txt"
close(1)

end



subroutine LS(P,P0,PD,SSS,LOBS,LSIG,lrms,LCOM,lcommn,RILL,ROBS,TMJD,XL1,XL2,XL3,&
         YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,chisq1,dphi1a,dphi2a,nobs,&
         PID,ndim,npar,ntr,niter,lmin,lmax,nd1,nd2,nd3)

! Least-squares convex optimization using the Levenberg-Marquardt method.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::nobs,ndim,ntr,lmin,lmax,PID(maxpar),niter,nd1,nd2,nd3,npar
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),&
       SSS(10),dphi1a,dphi2a
real(kind=tark),dimension(maxobs),intent(in)::LOBS,LSIG,TMJD
real(kind=tark),dimension(maxpar),intent(in)::P0,PD
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
integer::j0,j1,j2,jp,nca,IT(260000,3),nnod,ntri,nmui,nphii,nthe,nphi
real(kind=tark)::EILL(3,3,maxobs),EOBS(3,3,maxobs),chisq2,chisqmin,SPIN(4),XN(130000,3),&       
       NT(260000,3),MUN(130000),PHIN(130000),AX(6),rmax,PCOV(maxpar,maxpar),&
       PCURV(maxpar,maxpar),alamda,chisqa,chisqb,CSCF(0:256),PD1(1200),nut,&
       GSD(512,0:720),MUI(512),PHII(0:720),WI(512),&
       XS(0:180,0:360,3),MUS(0:180),PHIS(0:360)
real(kind=tark),dimension(maxobs)::LSIGC
real(kind=tark),dimension(260000)::MUT,PHIT,AR,AT
real(kind=tark),dimension(0:256,0:256)::ACF,BCF,SCFSTD
real(kind=tark),intent(out)::P(maxpar),LCOM(maxobs),lcommn,lrms,chisq1

nca=2000

! Initialize the discrete normal directions and the areas of the triangles:

call TRIDS(MUN,PHIN,IT,nnod,ntri,ntr)
do j1=1,6
   AX(j1)=1.0d0
end do

call RELLTD(XN,NT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
call ARELLTD(AT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
do j1=1,ntri
   MUT(j1)=NT(j1,3)
   nut=sqrt(1.0d0-MUT(j1)**2)
   PHIT(j1)=acos(NT(j1,1)/nut)
   if (NT(j1,2).lt.0.0d0) PHIT(j1)=2.0d0*pi-PHIT(j1)
end do

! Initialize the spherical harmonics coefficients for the Gaussian surface density:
nmui=64
nphii=180

call GAULEG(-1.0d0,1.0d0,MUI,WI,nmui)

AX(1)=P0(5)
AX(3)=P0(5)
AX(2)=P0(6)
AX(4)=P0(6)
AX(5)=P0(7)
AX(6)=P0(7)
! Gaussian surface density GSD for an ellipsoid
do j1=1,nmui
   do j2=0,nphii-1
            PHII(j2)=j2*2.0d0*pi/nphii
            GSD(j1,j2)=log(GSDE(AX,MUI(j1),PHII(j2)))
            PHII(j2)=j2*2.0d0*pi/nphii
   end do
end do

call ACFBCFN(ACF,BCF,GSD,MUI,PHII,WI,nmui,nphii,lmax)
! Initialize the spherical-harmonics counter:

jp=9
do j1=lmin,lmax
        do j2=0,j1
            jp=jp+1
            NCF(jp,1)=j1
            NCF(jp,2)=j2
   end do
end do
nacf=jp   
do j1=lmin,lmax
   do j2=1,j1
            jp=jp+1
            NCF(jp,1)=j1
            NCF(jp,2)=j2
   end do
end do

! Initialize the parameter deviations:

call CS2CF(CSCF,0.5d0,lmin,lmax)
! Initialize spin, size, scattering, and spherical harmonics coefficients:

P(1)=P0(1)
P(2)=P0(2)
P(3)=P0(3)
P(4)=P0(4)
P(5)=P0(8)
P(6)=P0(9)
P(7)=P0(10)
P(8)=P0(11)
P(9)=P0(12)
do j1=10,nacf
        P(j1)=ACF(NCF(j1,1),NCF(j1,2))
end do
do j1=nacf+1,npar
        P(j1)=BCF(NCF(j1,1),NCF(j1,2))
end do

! Solve for the parameters using the Levenberg-Marquardt method:
call LMCX(P,PD,SSS,LOBS,LSIG,LCOM,lcommn,RILL,ROBS,TMJD,NT,AT,MUT,PHIT,chisq1,&
     chisqa,chisqb,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,&
     dphi2a,nobs,PID,npar,ntri,nca,niter,lmin,lmax,nd1,nd2,nd3)
	 
! Check chisq-values vs. rms
!call LSTRES(LSIGC,lrms,LOBS,LCOM,lcommn,nobs)

! Output Gaussian surface density:
!do j0=1,4
!        SPIN(j0)=P(j0)
!end do
!do j0=10,nacf
!        ACF(NCF(j0,1),NCF(j0,2))=P(j0)
!end do
!do j0=nacf+1,npar
!        BCF(NCF(j0,1),NCF(j0,2))=P(j0)
!end do

! Max values: nthe=180,nphi=360
!nthe=60
!nphi=120
!call SPHDS(MUS,PHIS,nthe,nphi)
!call RGSNSD(XS,MUS,PHIS,ACF,BCF,rmax,0.0d0,nthe,nphi,lmin,lmax)

end



subroutine LSV(P0,PD,SSS,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,&
          YL22,YL2_22,YL32,YL2_32,chisq1,dphi1a,dphi2a,lsigrv,nobs,PID,&
          ndim,npar,ntr,niter,nmc,lmin,lmax,nd1,nd2,nd3,outdir)

! Least-squares convex optimization using the Levenberg-Marquardt method
! for virtual observations.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17
                                           
integer,intent(in)::nobs,ndim,ntr,nmc,lmin,lmax,PID(maxpar),niter,nd1,nd2,nd3,npar
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),lsigrv,dphi1a,dphi2a
real(kind=tark),dimension(maxobs),intent(in)::LOBS,LSIG,TMJD
real(kind=tark),dimension(maxpar),intent(in)::P0(maxpar),PD(maxpar)
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
character(len=200)::outdir

integer::j0,j1,j2,jp,IT(260000,3),nnod,ntri,nca,nmui,nphii,nthe,nphi,jmc
real(kind=tark)::EILL(3,3,maxobs),EOBS(3,3,maxobs),lcommn,lobsvmn,chisq2,chisqmin,&
       SB(4,2),SSS(10),MUN(130000),PHIN(130000),AX(6),rmax,PCOV(maxpar,maxpar),&
       PCURV(maxpar,maxpar),alamda,chisqa,chisqb,lsigrv1,SPIN(4),XN(130000,3),NT(260000,3),&
       CSCF(0:256),PD1(1200),nut,GSD(512,0:720),MUI(512),PHII(0:720),WI(512),&
       XS(0:180,0:360,3),MUS(0:180),PHIS(0:360),rg
real(kind=tark),dimension(maxobs)::LCOM,LOBSV,LSIGV
real(kind=tark),dimension(maxpar)::P(maxpar),P1(maxpar)
real(kind=tark),dimension(260000)::AT,AR,MUT,PHIT
real(kind=tark),dimension(0:256,0:256)::ACF,BCF,SCFSTD
real(kind=tark),intent(out)::chisq1

nca=2000

! Initialize the discrete normal directions and the areas of the triangles:

call TRIDS(MUN,PHIN,IT,nnod,ntri,ntr)
do j1=1,6
        AX(j1)=1.0d0
end do
call RELLTD(XN,NT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
call ARELLTD(AT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
do j1=1,ntri
        MUT(j1)=NT(j1,3)
        nut=sqrt(1.0d0-MUT(j1)**2)
        PHIT(j1)=acos(NT(j1,1)/nut)
        if (NT(j1,2).lt.0.0d0) PHIT(j1)=2.0d0*pi-PHIT(j1)
end do

! Initialize the spherical harmonics coefficients for the Gaussian surface density:

nmui=64
nphii=180
call GAULEG(-1.0d0,1.0d0,MUI,WI,nmui)

AX(1)=P0(5)
AX(3)=P0(5)
AX(2)=P0(6)
AX(4)=P0(6)
AX(5)=P0(7)
AX(6)=P0(7)
do j1=1,nmui
   do j2=0,nphii-1
            PHII(j2)=j2*2.0d0*pi/nphii
            GSD(j1,j2)=log(GSDE(AX,MUI(j1),PHII(j2)))
            PHII(j2)=j2*2.0d0*pi/nphii
   end do
end do
call ACFBCFN(ACF,BCF,GSD,MUI,PHII,WI,nmui,nphii,lmax)

! Initialize the spherical-harmonics counter:

jp=9
do j1=lmin,lmax
        do j2=0,j1
            jp=jp+1
            NCF(jp,1)=j1
            NCF(jp,2)=j2
   end do
end do
nacf=jp   
do j1=lmin,lmax
        do j2=1,j1
            jp=jp+1
            NCF(jp,1)=j1
            NCF(jp,2)=j2
   end do
end do

! Initialize spin, size, scattering, and spherical harmonics coefficients:

P(1)=P0(1)
P(2)=P0(2)
P(3)=P0(3)
P(4)=P0(4)
P(5)=P0(8)
P(6)=P0(9)
P(7)=P0(10)
P(8)=P0(11)
P(9)=P0(12)
do j1=10,nacf
        P(j1)=ACF(NCF(j1,1),NCF(j1,2))
end do
do j1=nacf+1,npar
        P(j1)=BCF(NCF(j1,1),NCF(j1,2))
end do

! Virtual least-squares solutions:

open (unit=1,file=trim(outdir)//'/acs_lsv.txt')
write(1,*) "P lon lat phi_0 alb g1 g2 size epoch_0 shape[1:Nl] rms"
open(unit=10,file="./acs_lsv_progress.txt")
write(10,*) nmc
do jmc=1,nmc

   lobsvmn=0.0d0
   lsigrv1=lsigrv
   do j0=1,nobs
      call RANG2(rg)
      LOBSV(j0)=LOBS(j0)*(1.0d0+rg*lsigrv1)
      lobsvmn=lobsvmn+LOBSV(j0)
      LSIGV(j0)=sqrt(LSIG(j0)**2+lsigrv1**2)
   end do
   lobsvmn=lobsvmn/nobs
   do j0=1,nobs
      LOBSV(j0)=LOBSV(j0)/lobsvmn
   end do
   
   do j1=1,npar
            P1(j1)=P(j1)
   end do
   call flush_()
   call LMCX(P1,PD,SSS,LOBSV,LSIGV,LCOM,lcommn,RILL,ROBS,TMJD,NT,AT,MUT,PHIT,&
        chisq1,chisqa,chisqb,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
        YL2_32,dphi1a,dphi2a,NOBS,PID,npar,ntri,nca,niter,lmin,lmax,nd1,nd2,nd3)

   if (P1(2).gt.tp)    P1(2)=P1(2)-tp*(int((P1(2)-tp)/(tp))+1)
   if (P1(2).lt.0.0d0) P1(2)=P1(2)+tp*(int((tp-P1(2))/(tp)))
   if (P1(4).gt.tp)    P1(4)=P1(4)-tp*(int((P1(4)-tp)/(tp))+1)
   if (P1(4).lt.0.0d0) P1(4)=P1(4)+tp*(int((tp-P1(4))/(tp)))

   write (1,260) P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),(P1(j1),j1=5,npar),&
            LSIGV(1)*sqrt(chisqa/nobs)
end do
260 format(10000(E20.14,2X))
close (unit=1)
close(10)
end


subroutine MCMC(P0,SSS,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
      YL2_32,dphi1a,dphi2a,nobs,PID,ndim,npar,ntr,nmcmc,nmc,lmin,lmax,nd1,nd2,nd3,ecode,outdir)
 
! Virtual-observation MCMC for convex models.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::nobs,PID(maxpar),ndim,ntr,nmc,nmcmc,lmin,lmax,nd1,nd2,nd3,npar
integer,intent(inout)::ecode
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),SSS(10),dphi1a,dphi2a
real(kind=tark),dimension(maxpar),intent(in)::P0
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32

integer::jmc,jmcmc,jp,j0,j1,j2,j3,IT(260000,3),nnod,ntri
real(kind=tark),dimension(maxobs)::LCOM
real(kind=tark)::lcommn,PV(2000,1000),W(2000),chisq1,chisq2,pdfr,rms1,rms2,SPIN(4),&
       XN(130000,3),NT(260000,3),MUN(130000),PHIN(130000),AX(6),nut,rmax,&
       PCOV(maxpar,maxpar),PCURV(maxpar,maxpar),chisq,chisqa,chisqb
real(kind=tark),dimension(maxpar)::P1,P2,PD
real(kind=tark),dimension(260000)::AT,MUT,PHIT
character(len=1) header
character(len=200)::outdir
! Initialize the discrete normal directions and the areas of the triangles:

call TRIDS(MUN,PHIN,IT,nnod,ntri,ntr)
do j1=1,6
        AX(j1)=1.0d0
end do
call RELLTD(XN,NT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
call ARELLTD(AT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
do j1=1,ntri
   MUT(j1)=NT(j1,3)
        nut=sqrt(1.0d0-MUT(j1)**2)
        PHIT(j1)=acos(NT(j1,1)/nut)
        if (NT(j1,2).lt.0.0d0) PHIT(j1)=2.0d0*pi-PHIT(j1)
end do
! Initialize the spherical-harmonics counter:

jp=9
do j1=lmin,lmax
   do j2=0,j1
      jp=jp+1
      NCF(jp,1)=j1
      NCF(jp,2)=j2
   end do
end do
nacf=jp   
do j1=lmin,lmax
   do j2=1,j1
      jp=jp+1
      NCF(jp,1)=j1
      NCF(jp,2)=j2
   end do
end do

! Initialize spin, size, scattering, and spherical harmonics coefficients:

do j1=1,npar
        P1(j1)=P0(j1)
end do

! Input virtual least-squares solutions:

open(unit=1,file=trim(outdir)//'/acs_lsv.txt')
read(1,*) header
do jmc=1,nmc
        read (1,*) (PV(j1,jmc),j1=1,npar)
        PV(1,jmc)=PV(1,jmc)*3600.0d0
        PV(2,jmc)=PV(2,jmc)*rd
        PV(3,jmc)=PV(3,jmc)*rd
        PV(4,jmc)=PV(4,jmc)*rd
end do
close(unit=1)

! Virtual-observation MCMC; reset weights and compute chisq for initial 
! parameters:

do jmcmc=1,nmcmc
        W(jmcmc)=0.0d0
end do

call MRQCOFS(TMJD,LOBS,LSIG,P1,PID,npar,PCOV,PCURV,chisq,chisqa,chisqb,LCOM,lcommn,&
        SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
        YL32,YL2_32,dphi1a,dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)

chisq1=chisqa

! Output initial parameters as the first sample (strictly: burn-in phase)

open(unit=1,file=trim(outdir)//'/acs_mcmc.txt')
write(1,*) "P lon lat phi_0 alb g1 g2 size epoch_0 shape[1:Nl] rms chisq"

jmcmc=1
W(jmcmc)=1.0d0
write (1,200) P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),(P1(j1),j1=5,npar),&
         rms1,chisq1-nobs
200 format(10000(E20.12E3,2X))

! Markov chain Monte Carlo:
open(10,file="./acs_mcmc_progress.txt")
write(10,*) nmcmc
do while (jmcmc<=nmcmc)
   if(jmcmc==nmcmc) then
      close(unit=1)

! Output weights:

      open(unit=1,file=trim(outdir)//'/acs_mcmc_w.txt')
      do j1=1,nmcmc
          write (1,'(I12,1X,E12.6)') j1,W(j1)
      end do
      close(unit=1)
	  close(10)
      return
   endif

! Proposal and its chisq:

   call CXPROPOSE(P2,P1,PV,PID,nmc,npar)

   call MRQCOFS(TMJD,LOBS,LSIG,P2,PID,npar,PCOV,PCURV,chisq,&
          chisqa,chisqb,LCOM,lcommn,SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,&
          XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,&
          nobs,ntri,lmin,lmax,nd1,nd2,nd3)

   chisq2=chisqa

! Accept or reject:

   pdfr=exp(-0.5d0*(chisq2-chisq1))
! Accept 
   if (RAN2().le.pdfr) then
      chisq1=chisq2
      rms1=rms2
      do j1=1,npar
         P1(j1)=P2(j1)
      end do
      jmcmc=jmcmc+1
      W(jmcmc)=1.0d0
      write (1,200) P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),&
                   (P1(j1),j1=5,npar),rms1,chisq1-nobs
	  call flush_()
   else
      W(jmcmc)=W(jmcmc)+1.0d0
  	  if(W(jmcmc)>100000) then
		ecode=60
		close(1)
!		call flush_(3,0,0)
		close(10)
		return
	  endif
   endif
end do
close(1)
close(10)
end


subroutine IS(P0,SSS,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
              YL32,YL2_32,dphi1a,dphi2a,dchisqc,nobs,PID,&
              ndim,npar,ntr,nmcmc,nmc,lmin,lmax,nd1,nd2,nd3,ecode,outdir)
 
! Virtual-observation MCMC for convex models.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::nobs,PID(maxpar),ndim,ntr,nmc,nmcmc,lmin,lmax,nd1,nd2,nd3,npar
integer,intent(inout)::ecode
real(kind=tark),dimension(maxobs),intent(in)::TMJD,LOBS,LSIG
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),SSS(10),dchisqc,dphi1a,dphi2a
real(kind=tark),dimension(maxpar),intent(in)::P0
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32

integer::jmc,jmcmc,jp,j0,j1,j2,j3,IT(260000,3),nnod,ntri,nsamp
real(kind=tark),dimension(maxobs)::LCOM
real(kind=tark)::lcommn,PV(2000,1000),chisq1,chisq2,dchisq1,dchisq2,pdfr,rms1,rms2,&
       SPIN(4),XN(130000,3),NT(260000,3),MUN(130000),PHIN(130000),AX(6),nut,rmax,&
       PCOV(2000,2000),PCURV(2000,2000),chisq,chisqa,chisqb
real(kind=tark),dimension(maxpar)::P1,P2,W,PD
real(kind=tark),dimension(260000)::AT,MUT,PHIT
character(len=1) header
character(len=200)::outdir

! Initialize the discrete normal directions and the areas of the triangles:

call TRIDS(MUN,PHIN,IT,nnod,ntri,ntr)
do j1=1,6
        AX(j1)=1.0d0
end do
call RELLTD(XN,NT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
call ARELLTD(AT,MUN,PHIN,AX,rmax,IT,nnod,ntri)
do j1=1,ntri
        MUT(j1)=NT(j1,3)
        nut=sqrt(1.0d0-MUT(j1)**2)
        PHIT(j1)=acos(NT(j1,1)/nut)
        if (NT(j1,2).lt.0.0d0) PHIT(j1)=2.0d0*pi-PHIT(j1)
end do

! Initialize the spherical-harmonics counter:

jp=9
do j1=lmin,lmax
        do j2=0,j1
           jp=jp+1
           NCF(jp,1)=j1
           NCF(jp,2)=j2
   end do
end do
nacf=jp   
do j1=lmin,lmax
        do j2=1,j1
           jp=jp+1
           NCF(jp,1)=j1
           NCF(jp,2)=j2
   end do
end do

! Initialize spin, size, scattering, and spherical harmonics coefficients:

do j1=1,npar
        P1(j1)=P0(j1)
end do

! Input virtual least-squares solutions:

open(unit=1,file=trim(outdir)//'/acs_lsv.txt')
read(1,*) header
do jmc=1,nmc
   read (1,*) (PV(j1,jmc),j1=1,npar)
        PV(1,jmc)=PV(1,jmc)*3600.0d0
        PV(2,jmc)=PV(2,jmc)*rd
        PV(3,jmc)=PV(3,jmc)*rd
        PV(4,jmc)=PV(4,jmc)*rd
end do
close(unit=1)

! Virtual-observation MCMC; reset weights and compute chisq for initial 
! parameters:

do jmcmc=1,nmcmc
        W(jmcmc)=0.0d0
end do

call MRQCOFS(TMJD,LOBS,LSIG,P1,PID,npar,PCOV,PCURV,chisq,&
             chisqa,chisqb,LCOM,lcommn,SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,&
             XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,&
             nobs,ntri,lmin,lmax,nd1,nd2,nd3)

chisq1=chisqa

! Output initial parameters as the first sample (strictly: burn-in phase)

open(unit=1,file=trim(outdir)//'/acs_is.txt')
write(1,*) "P lon lat phi_0 alb g1 g2 size epoch_0 shape[1:Nl] rms chisq"

jmcmc=1
nsamp=0
dchisq1=chisq1-nobs
W(jmcmc)=exp(-0.5d0*dchisq1)
write (1,'(10000(E20.14,2X))') P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),&
                    (P1(j1),j1=5,npar),rms1,chisq1-nobs

! Markov chain Monte Carlo:
open(10,file="./acs_is_progress.txt")
write(10,*) nmcmc
do while (jmcmc<=nmcmc)
   if (jmcmc.ge.nmcmc) then
         close(unit=1)

! Output weights:

         open(unit=1,file=trim(outdir)//'/acs_is_w.txt')
         do j1=1,nmcmc
               write (1,'(I12,1X,E12.6)') j1,W(j1)
         end do
         close(unit=1)
		 close(10)
         return
   endif

! Proposal and its chisq:

   call CXPROPOSE(P2,P1,PV,PID,nmc,npar)

   call MRQCOFS(TMJD,LOBS,LSIG,P2,PID,npar,PCOV,PCURV,chisq,&
                chisqa,chisqb,LCOM,lcommn,SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,&
                XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,&
                nobs,ntri,lmin,lmax,nd1,nd2,nd3)

   chisq2=chisqa
   dchisq2=chisq2-nobs

! Accept or reject:

   pdfr=exp(-0.5d0*(chisq2-chisq1))
   if (dchisq2.le.dchisqc) then
       dchisq1=dchisq2
       rms1=rms2
       do j1=1,npar
           P1(j1)=P2(j1)
       end do
       jmcmc=jmcmc+1
	   nsamp=0
       W(jmcmc)=exp(-0.5d0*dchisq1)
       write (1,'(10000(E20.14,2X))') P1(1)/3600.0d0,(P1(j1)/rd,j1=2,4),(P1(j1),j1=5,npar),&
               rms1,dchisq1
	   call flush_()
   else
       W(jmcmc)=W(jmcmc)+exp(-0.5d0*dchisq1)
	   nsamp=nsamp+1
       if(nsamp>100) then
	      ecode=70
		  close(1)
!		  call flush_(2,0,0)
		  close(10)
	      return
	   endif
   endif
end do
close(1)
close(10)
end

end module