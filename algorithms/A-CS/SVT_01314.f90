program call_acs
! This is just to generate test input data.
! It is assumed that the data is treated as one single lightcurve.
! Here the time of the first observation is NOT 0, but T(0)-240000.5
! Observations must be in chronological order.

! Compile:
! f95 Libf90c/flush_progress.f90 Libf90c/Discrets.f90 Libf90c/Randev.f90 Libf90c/HG1G2.f90 Libf90c/Voper.f90 Libf90c/Specfunc.f90 Libf90c/Corrfunc.f90 Libf90c/Ellips.f90 Libf90c/Gsg.f90 Libf90c/Insectcx.f90 Libf90c/Scatlaw.f90 Libf90c/Lditd.f90 Libf90c/LM.f90 Libf90c/Numint.f90 Libf90c/Viewgeo.f90 Libf90c/jacobi_pca.f90 Libf90c/covcor.f90 Libf90c/vectprocs.f90 Libf90c/chn2.f90 Libf90c/minko.f90 a_cs.f90 SVT_01314.f90 -o SVT01314.o 

use acs
use Randev
implicit none
integer,parameter::tark=selected_real_kind(12,30),maxobs=10000,maxpar=20
real(kind=tark),parameter::pi=4.0d0*atan(1.0d0),rd=pi/180.0d0
integer::nobsi,j0,lmin(2),lmax(2),niter(2),ntr(2),nmcmc(2),nmc(2),i,j,k,l,lb,isim,nshpass,&
         npas1,npas2,ipas1,ipas2,pas1,pas2,il,inmc,ilsigrv,ilsigr,idchisqc,intr,initer,&
		 nallpass,ecode,nsol,lfline
real(kind=tark),dimension(maxobs)::LOBS,LSIG,TDT
real(kind=tark)::XSUN(3,maxobs),XOBS(3,maxobs),lsigr(2),Pin(maxpar),dchisqc(2),&
       lobsmn,lsigmn,RILL(3,maxobs),ROBS(3,maxobs),SB(4,2),clat,&
       lsigrv(2),P(maxpar),nut,P0(maxpar),tspan,cpui,cpuf,csig,fline(14)
character(len=10) setnr,strsim,dum,sl,snmc,slsigrv,slsigr,sdchisqc,sntr,sniter,ifile
character(len=45) outdir,output

! For testing purposes, output files are coded according to the test case

! Choose which lightcurves are used for the tests. The following parameters must be in agreement with the parameters
! used in A-IS algorithm testing, since the output files from A-IS are used in A-ES, and named based on these parameters
isim=1
write(strsim,'(I4)') isim
strsim=adjustl(strsim)
setnr='01314'

! Input parameters, two values of each tested
nmc(1)=50 ! n virtual
nmc(2)=1000
nmcmc(1)=100 ! n mcmc
nmcmc(2)=1000 ! n mcmc
lsigrv(1)=0.0001
lsigrv(2)=0.001
dchisqc(1)=100.
dchisqc(2)=1000.
lsigr(1)=0.05
lsigr(2)=0.1
lmin(1)=1
lmin(2)=2
lmax(1)=4
lmax(2)=6
ntr(1)=5
ntr(2)=10
niter(1)=10 ! Number of iterations in L-M minimization
niter(2)=30
! Define the id for the test output files
outdir='Output/Set'//trim(adjustl(setnr))
outdir=adjustl(outdir)

! Input observations, and the illumination and observation geometry. Write file inputData.txt:
open(unit=1,file='../TestSet/lc'//trim(strsim)//'.out')
read (1,*) nobsi
do j0=1,nobsi
	read(1,*) TDT(j0),LOBS(j0),XSUN(1:3,j0),XOBS(1:3,j0)
	LSIG(j0)=0.1
	TDT(j0)=TDT(j0)-2400000.5d0 ! Just to make the number smaller
end do
tspan=maxval(TDT(1:nobsi))-minval(TDT(1:nobsi))
if (isim<=10) then ! ellipsoid simulations
	read(1,*) dum
	read(1,*) P0(1:12)
	P0(1)=P0(1)/3600.
	P0(2)=P0(2)/rd
	P0(3)=P0(3)/rd
else ! nonconvex shape simulations
	read(1,*) dum
	read(1,*) dum
	read(1,5) dum,P0(5:7)
	read(1,1) dum,P0(8)
	read(1,2) dum,P0(10)
	read(1,3) dum,P0(1)
	read(1,4) dum,P0(3)
	read(1,4) dum,P0(2)
endif
close(1)
1 format(A6,F4.2)
2 format(A8,F6.2)
3 format(A16,F8.6)
4 format(A13,F6.2)
5 format(A17,F1.0,1X,F4.2,1X,F4.2)

open(1,file="Input/inputData.txt")
write(1,('(I4)')) nobsi
write(1,('(A50)')) "time,br,br_err,sun_x,sum_y,sum_z,obs_x,obs_y,obs_z"
do j0=1,nobsi
	csig=ran2()
	csig=csig*(-1)**int(csig*100)
	write(1,*) TDT(j0),LOBS(j0)*(1.+LSIG(j0)*csig),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
end do
close(1)

open(1,file="Input/ais_FitAll.txt")
open(3,file="Input/ais_FitAllList.txt")
do i=1,isim
	read(3,*) P(1:12)
	if(i==isim) write(1,*) P(1:12)
end do
close(1)
close(3)

open(2,file=trim(outdir)//'/'//trim(setnr)//'_test.out')
open(4,file=trim(outdir)//'/'//trim(setnr)//'_testTable.out')
write(4,*) "lmin, lmax, lsigr, nmc, lsigrv, nmcmc, dchisqc, ntri, niter, cpuf-cpui"
npas1=0
npas2=0
nallpass=0
do il=1,2
 write(sl,'(I4)') il
 sl=adjustl(sl)
 do ilsigr=1,2
  write(slsigr,'(I4)') ilsigr
  slsigr=adjustl(slsigr)
  do inmc=1,2  		
   write(snmc,'(I4)') inmc
   snmc=adjustl(snmc)
   do ilsigrv=1,2  		
    write(slsigrv,'(I4)') ilsigrv
    slsigrv=adjustl(slsigrv)
    do idchisqc=1,2  		
     write(sdchisqc,'(I4)') idchisqc
     sdchisqc=adjustl(sdchisqc)
     do intr=1,2  		
      write(sntr,'(I4)') intr
      sntr=adjustl(sntr)
	  do initer=1,2
       write(sniter,'(I4)') initer
       sniter=adjustl(sniter)
	   print*,"Simulation",il,ilsigr,inmc,ilsigrv,idchisqc,intr,initer
       output=trim(outdir)//"/"//trim(setnr)//"_T"//trim(sl)//"_"//trim(slsigr)//"_"//trim(snmc)//"_"&
	         //trim(slsigrv)//"_"//trim(sdchisqc)//"_"//trim(sntr)//"_"//trim(sniter)

	   open(1,file="Input/acs_inputParameters.txt")
! Basic input
	   write(1,20) nmc(inmc),"Number of virtual solutions"
	   write(1,20) nmcmc(inmc),"Number of MCMC solutions"
	   write(1,20) lmin(il),"Minimum index of the spherical harmonics series"
	   write(1,20) lmax(il),"Maximum index of the spherical harmonics series"
	   write(1,20) ntr(intr),"Number of triangle rows for shape discretization"
! Advanced input
	   write(1,20) niter(initer),"Maximum number of iterations"
	   write(1,10) lsigrv(ilsigrv),"Virtual error"
	   write(1,10) dchisqc(idchisqc),"Limiting Delta chi2"
	   write(1,10) lsigr(ilsigr),"Error coefficient for reqularizing observations"
	   close(1)
10     format(F14.6,5X,A60)
20     format(I5,5X,A60)
! Start the AVI ellipsoid MCMC workflow:
       call cpu_time(cpui)
       call a_cs(ecode) ! unit 1 used
       call cpu_time(cpuf)
	   write(4,40) lmin(il),lmax(il),lsigr(ilsigr),nmc(inmc),lsigrv(ilsigrv),nmcmc(inmc),&
	               dchisqc(idchisqc),ntr(intr),niter(initer),cpuf-cpui
40 	   format(2(I3,1X),F8.4,1X,I5,1X,F8.6,1X,I5,1X,F12.5,I3,1X,I3,1X,F12.3)
	   write(2,*) "--------------------------------------------------------------------------------"
	   write(2,*) "Simulated dataset: ",isim,"npts:",nobsi,"T range (years):",tspan/360.
	   if (isim<11) write(2,*) "Ellipsoid shape model"
	   if (isim>=11) write(2,*) "Nonconvex, spacecraft/radar derived shape model"
	   write(2,*) "CPU:",cpuf-cpui
	   write(2,'(8(1X,A6))') "car","amtol","nmc","lsigrv","vsc","nmcmc","dchisqc"
	   write(2,'(2(I3,1X),F8.4,1X,I5,1X,F8.6,1X,I5,1X,F12.5,I3,1X,I3,1X,F12.3)')&
   	        lmin(il),lmax(il),lsigr(ilsigr),nmc(inmc),lsigrv(ilsigrv),nmcmc(inmc),&
	               dchisqc(idchisqc),ntr(intr),niter(initer)
! Write simulation-wise output files
       if(ecode==60) then
         open (unit=1,file='Output/acs_lsv.txt')
         open (unit=3,file=trim(output)//'_acs_lsv.txt')
	   elseif(ecode==-60) then
         open (unit=1,file='Output/acs_mcmc.txt')
         open (unit=3,file=trim(output)//'_acs_mcmc.txt')
	   elseif(ecode==-70) then
         open (unit=1,file='Output/acs_is.txt')
         open (unit=3,file=trim(output)//'_acs_is.txt')
	   else
	    print*,"What's going on??"
		goto 30
	   endif
	   ipas1=0
	   ipas2=0
	   nsol=0
	   lfline=4+(lmax(il)+1)**2-lmin(il)**2+5
       do while(1==1)
        read(1,*,END=50) fline(1:lfline)
	    nsol=nsol+1
        write(3,('(13(F14.6,1X),F10.6)')) fline(1:lfline)
		pas1=1
		pas2=1
		do j0=1,12
			if(j0<4.or.(j0>4.and.j0<8)) then
				if (j0>1.and.j0<5) then
					if (isim<=10) then
						if ((abs(P0(j0)-fline(j0))<10.or.abs(abs(P0(j0)-fline(j0))-180.)<10.or.abs(abs(P0(j0)-fline(j0))-360.)<10.)) then
						else
							pas1=0
						endif
					else
						if ((abs(P0(j0)-fline(j0))<15.or.abs(abs(P0(j0)-fline(j0))-180.)<15.or.abs(abs(P0(j0)-fline(j0))-360.)<15.)) then
						else
							pas2=0
						endif				
					endif
				else
					if (isim<=10.and.(j0==6.or.j0==7)) then
						if (abs(P0(j0)-fline(j0))/P0(j0)>0.05) then
							pas1=0
						endif
					else if (isim>10.and.(j0==6.or.j0==7)) then
						if (abs(P0(j0)-fline(j0))/P0(j0)>0.4) then
							pas2=0
						endif
					else if (j0==1) then
						if (abs(P0(j0)-fline(j0))/P0(j0)>0.1) then
							if(isim<=10) pas1=0
							if(isim>10) pas2=0
						endif			
					endif
				endif
			endif
	    end do
	    if (isim<=10.and.pas1==1) ipas1=1
	    if (isim>10.and.pas2==1) ipas2=1
       end do ! End do over mc solutions
50     close(1)
       close(3)
	   print*,"Spin passed ",npas1+npas2," test sets."
 	   if (isim<=10.and.ipas1==1) then
			npas1=npas1+1
			write(2,*) "PASSED SPIN TEST"
			print*,"PASSED SPIN TEST"
	   elseif (isim>10.and.ipas2==1) then
			npas2=npas2+1
			write(2,*) "PASSED SPIN TEST"
			print*,"PASSED SPIN TEST"
	   endif
       nshpass=0
       do i=1,nsol
   	     write(ifile,'(I5)') i
	     ifile=adjustl(ifile)
	     open(unit=7,file='Output/acs_vertices'//trim(ifile)//'.txt')
	     read(7,*) dum
	     dum=adjustl(dum)
	     if (dum(1:1)/="!") then
	 	  nshpass=nshpass+1
	     endif
	     close(7)
	   end do
	   if(nshpass/(nmcmc(inmc)*1.0)>0.0.and.(ipas1==1.or.ipas2==1)) then
			nallpass=nallpass+1
			write(2,*) "PASSED shape test"
			print*,"Passed shape test with",nshpass,"of",nmcmc
	   else
			print*,"FAILED shape test"
       endif
	   print*,"Spin and shape passed ",nallpass," test sets."
30	  end do ! End loop over input parameters
	 end do
    end do
   end do
  end do
 end do
end do
!print*,"npas1 of 10:",npas1 ! Spin passed
!print*,"npas2 of 12:",npas2 ! Spin passed
!print*,nallpass," passed of ",22 ! Shape and spin both passed
!if(nallpass/22.>0.6) then
!	print*,"PASSED"
!else
!	print*,"FAILED"
!end if
close(2)
close(4)
end