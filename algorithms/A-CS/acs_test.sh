export FC=f95
export FCFLAGS='-fprofile-arcs -ftest-coverage'
cd Libf90c

# Test for test case GAVITEA-SVT_01344

echo "\n-----------------------"
echo "Running test case 01344"
echo "Unit tests for modeules Discrets, HG1G2 and Randev"
echo "-----------------------\n"

rm -f Discrets_fun*
rm -f Discrets.o
rm -f Discrets.mod
funit Discrets
gcov Discrets.f90

rm -f Randev_fun*
rm -f Randev.o
rm -f Randev.mod
funit Randev
gcov Randev.f90

rm -f HG1G2_fun*
rm -f HG1G2.o
rm -f HG1G2.mod
funit HG1G2
gcov HG1G2.f90

cd ../

# Test for test case GAVITEA-SVT_01304

echo "\n-----------------------"
echo "Running test case 01304"
echo "Each simulation (i.e., test run) takes of order an hour."
echo "Tests algorithm performance for 22 datasets with constant input parameters"
echo "-----------------------\n"

f95 Libf90c/flush_progress.f90 Libf90c/Discrets.f90 Libf90c/Randev.f90 Libf90c/HG1G2.f90 Libf90c/Voper.f90 Libf90c/Specfunc.f90 Libf90c/Corrfunc.f90 Libf90c/Ellips.f90 Libf90c/Gsg.f90 Libf90c/Insectcx.f90 Libf90c/Scatlaw.f90 Libf90c/Lditd.f90 Libf90c/LM.f90 Libf90c/Numint.f90 Libf90c/Viewgeo.f90 Libf90c/jacobi_pca.f90 Libf90c/covcor.f90 Libf90c/vectprocs.f90 Libf90c/chn2.f90 Libf90c/minko.f90 a_cs.f90 SVT_01304.f90 -o SVT_01304.o
./SVT_01304.o

# Test for test case GAVITEA-SVT_01314

echo "\n-----------------------"
echo "Running test case 01314"
echo "A simulation (i.e., test run) may take hours"
echo "Tests algorithm performance for 22 datasets with constant input parameters"
echo "-----------------------\n"

f95 Libf90c/flush_progress.f90 Libf90c/Discrets.f90 Libf90c/Randev.f90 Libf90c/HG1G2.f90 Libf90c/Voper.f90 Libf90c/Specfunc.f90 Libf90c/Corrfunc.f90 Libf90c/Ellips.f90 Libf90c/Gsg.f90 Libf90c/Insectcx.f90 Libf90c/Scatlaw.f90 Libf90c/Lditd.f90 Libf90c/LM.f90 Libf90c/Numint.f90 Libf90c/Viewgeo.f90 Libf90c/jacobi_pca.f90 Libf90c/covcor.f90 Libf90c/vectprocs.f90 Libf90c/chn2.f90 Libf90c/minko.f90 a_cs.f90 SVT_01314.f90 -o SVT_01314.o
./SVT_01314.o
