program call_acs

! To compile:
! f95 Libf90c/flush_progress.f90 Libf90c/Discrets.f90 Libf90c/Randev.f90 Libf90c/HG1G2.f90 Libf90c/Voper.f90 Libf90c/Specfunc.f90 Libf90c/Corrfunc.f90 Libf90c/Ellips.f90 Libf90c/Gsg.f90 Libf90c/Insectcx.f90 Libf90c/Scatlaw.f90 Libf90c/Lditd.f90 Libf90c/LM.f90 Libf90c/Numint.f90 Libf90c/Viewgeo.f90 Libf90c/jacobi_pca.f90 Libf90c/covcor.f90 Libf90c/vectprocs.f90 Libf90c/chn2.f90 Libf90c/minko.f90 a_cs.f90 SVT_01304.f90 -o SVT01304.o 

use acs
use Randev
implicit none
integer,parameter::tark=selected_real_kind(12,30),maxobs=10000,maxpar=20
real(kind=tark),parameter::pi=4.0d0*atan(1.0d0),rd=pi/180.0d0
integer::nobsi,j0,lmin,lmax,niter,ntr,nmcmc,nmc,i,j,k,l,lb,nsim1,nsim2,isim,npas1,npas2,&
         ipas1,ipas2,pas1,pas2,imcmc,nshpass,nallpass,ecode,iini,nsol,lfline
real(kind=tark),dimension(maxobs)::LOBS,LSIG,TDT
real(kind=tark)::XSUN(3,maxobs),XOBS(3,maxobs),lsigr,Pin(maxpar),dchisqc,&
       lobsmn,lsigmn,RILL(3,maxobs),ROBS(3,maxobs),SB(4,2),clat,&
       lsigrv,P(maxpar),nut,P0(maxpar),tspan,cpui,cpuf,csig,fline(100)
character(len=10) setnr,strsim,dum,ifile
character(len=45) outdir,output

! Choose which lightcurves are used for the tests
nsim1=1
nsim2=22
setnr='01304'

! These are provided by the user in the parameter-input GUI, partly the same as for spin scanning:
nmc=100        ! Number of virtual-observation solutions.
nmcmc=100    ! Number of MCMC solutions
lmin=1      ! Minimum degree in spherical harmonics.
lmax=5      ! Maximum degree in spherical harmonics.
ntr=8      ! Number of triangle rows per octant for shape rendering.
niter=20   ! Number of iterations in L-M minimization.
lsigrv=0.001 ! Virtual error.
dchisqc=10000.! Limiting Delta_chi2 for IS
lsigr=0.05! error coef for reqularizing observations
open(1,file="Input/acs_inputParameters.txt")
! Basic input
write(1,20) nmc,"Number of virtual solutions"
write(1,20) nmcmc,"Number of MCMC solutions"
write(1,20) lmin,"Minimum index of the spherical harmonics series"
write(1,20) lmax,"Maximum index of the spherical harmonics series"
write(1,20) ntr,"Number of triangle rows for shape discretization"
! Advanced input
write(1,20) niter,"Maximum number of iterations"
write(1,10) lsigrv,"Virtual error"
write(1,10) dchisqc,"Limiting Delta chi2"
write(1,10) lsigr,"error coefficient for reqularizing observations"
close(1)
10 format(F14.6,5X,A60)
20 format(I5,5X,A60)

outdir='Output/Set'//trim(adjustl(setnr))
outdir=adjustl(outdir)

open(unit=2,file=trim(outdir)//'/'//trim(setnr)//'_test.out')
open(unit=4,file=trim(outdir)//'/'//trim(setnr)//'_testTable.out')
write(4,*) "nobsi,cpuf-cpui"
npas1=0
npas2=0
nallpass=0
open(11,file="Input/ais_FitAllList.txt")
do iini=1,nsim1-1
	read(11,*) P(1:12)
end do
do isim=nsim1,nsim2 ! Loop over simulated lightcurves
   write(2,*) "----------------------------------------------------------"
   write(strsim,'(I4)') isim
   strsim=adjustl(strsim)
   open(unit=1,file='../TestSet/lc'//trim(strsim)//'.out')
   read (1,*) nobsi
   do j0=1,nobsi
      read(1,*) TDT(j0),LOBS(j0),XSUN(1:3,j0),XOBS(1:3,j0)
      LSIG(j0)=0.1
      TDT(j0)=TDT(j0)-2400000.5d0 ! Just to make the number smaller
   end do
   tspan=maxval(TDT(1:nobsi))-minval(TDT(1:nobsi))
   if (isim<=10) then ! ellipsoid simulations
      read(1,*) dum
      read(1,*) P0(1:12)
      P0(1)=P0(1)/3600.
      P0(2)=P0(2)/rd
      P0(3)=P0(3)/rd
   else ! nonconvex shape simulations
      read(1,*) dum
      read(1,*) dum
      read(1,5) dum,P0(5:7)
      read(1,1) dum,P0(8)
      read(1,2) dum,P0(10)
      read(1,3) dum,P0(1)
      read(1,4) dum,P0(3)
      read(1,4) dum,P0(2)
   endif
   close(1)
1  format(A6,F4.2)
2  format(A8,F6.2)
3  format(A16,F8.6)
4  format(A13,F6.2)
5  format(A17,F1.0,1X,F4.2,1X,F4.2)

   open(1,file="Input/inputData.txt")
   write(1,('(I4)')) nobsi
   write(1,('(A50)')) "time,br,br_err,sun_x,sum_y,sum_z,obs_x,obs_y,obs_z"
   do j0=1,nobsi
      csig=ran2()
      csig=csig*(-1)**int(csig*100)
      write(1,*) TDT(j0),LOBS(j0)*(1.+LSIG(j0)*csig),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
   end do
   close(1)
      
! Start the AVI convex MCMC workflow:
	output=trim(outdir)//"/Set"//trim(adjustl(setnr))//"_T"//trim(strsim)
	output=adjustl(output)
	open(1,file="Input/ais_FitAll.txt")
	read(11,*) P(1:12)
	write(1,*) P(1:12)
	close(1)
	call cpu_time(cpui)
	call a_cs(ecode) ! Uses units 1 and 7-9
	call cpu_time(cpuf)
	write(4,40) nobsi,cpuf-cpui
40 	format(I4,F12.5)
	write(2,*) "Simulated dataset: ",isim,"npts:",nobsi,"T range (years):",tspan/360.
	if (isim<11) write(2,*) "Ellipsoid shape model"
	if (isim>=11) write(2,*) "Nonconvex, spacecraft/radar derived shape model"
	write(2,*) "CPU:",cpuf-cpui
	write(2,'(8(1X,A6))') "lmin","lmax","nmc","lsigrv","nmcmc","dchisqc","lsigr","ntr","niter"
	write(2,'(3(I5,1X),F6.4,1X,I5,1X,F8.4,1X,F6.2,1X,I5,1X,I5)')&
	      lmin,lmax,nmc,lsigrv,nmcmc,dchisqc,lsigr,ntr,niter

! Write simulation-wise output files
   open (unit=1,file='Output/acs_lsv.txt')
   open (unit=3,file=trim(output)//'_acs_lsv.txt')
   lfline=4+(lmax+1)**2-lmin**2+5
   do j0=1,nmc
        read(1,*) fline(1:lfline)
        write(3,('(13(F14.6,1X),F10.6)')) fline(1:lfline)
   end do
   close(1)
   close(3)
   if(ecode==60) then
		 nsol=nmc
         open (unit=1,file='Output/acs_lsv.txt')
   elseif(ecode==-60) then
		 nsol=nmcmc
         open (unit=1,file='Output/acs_mcmc.txt')
         open (unit=3,file=trim(output)//'_acs_mcmc.txt')
   elseif(ecode==-70) then
		 nsol=nmcmc
         open (unit=1,file='Output/acs_is.txt')
         open (unit=3,file=trim(output)//'_acs_is.txt')
   else
     print*,"Output parameter file not detected"
	 goto 30
   endif
   do while(1==1)
        read(1,*,END=50) fline(1:lfline)
        if(ecode==-60.or.ecode==-70) write(3,('(13(F14.6,1X),F10.6)')) fline(1:lfline)
		pas1=1
		pas2=1
		do j0=1,12
			if(j0<4) then
				if (j0>1) then
					if (isim<=10) then
						if ((abs(P0(j0)-fline(j0))<10.or.abs(abs(P0(j0)-fline(j0))-180.)<10.or.abs(abs(P0(j0)-fline(j0))-360.)<10.)) then
						else
							pas1=0
						endif
					else
						if (j0.ne.4.and.(abs(P0(j0)-fline(j0))<15.or.abs(abs(P0(j0)-fline(j0))-180.)<15.or.abs(abs(P0(j0)-fline(j0))-360.)<15.)) then
						else
							pas2=0
						endif				
					endif
				else
					if (abs(P0(j0)-fline(j0))/P0(j0)>0.1) then
						if(isim<=10) pas1=0
						if(isim>10) pas2=0
					else
					endif
				endif
			endif
		end do
		if (isim<=10.and.pas1==1) ipas1=1
		if (isim>10.and.pas2==1) ipas2=1
   end do
50 close(1)
   close(3)
   if (isim<=10.and.ipas1==1) npas1=npas1+1
   if (isim>10.and.ipas2==1) npas2=npas2+1
!   print*,"npas1,npas2: ",npas1,npas2
   if(ipas1==1.or.ipas2==1) then
	write(2,*) "PASSED spin test"
   else
	print*,"Simulation",isim,"FAILED spin test"
   endif    
   nshpass=0
   do imcmc=1,nmcmc
   	write(ifile,'(I5)') imcmc
	ifile=adjustl(ifile)
	open(unit=7,file='Output/acs_vertices'//trim(ifile)//'.txt')
	read(7,*) dum
	dum=adjustl(dum)
	if (dum(1:1)/="!") then
		nshpass=nshpass+1
	endif
	close(7)
   end do
   if(nshpass/(nmcmc*1.0)>0.and.(ipas1==1.or.ipas2==1)) then
		nallpass=nallpass+1
		write(2,*) "Simulation",isim,"PASSED shape test"
	else
		print*,"Simulation",isim,"FAILED shape test"
   endif
30 end do ! Loop over simulations, i.e., lightcurves
close(2)
close(4)
close(11)
!print*,"Spin passed for 10 ellipsoid shapes:",npas1 ! Spin passed
!print*,"Spin passed for 12 nonconvex shapes:",npas2 ! Spin passed
!print*,nallpass," passed shape and spin of ",22 ! Shape and spin both passed
if(nallpass==22) then
	print*,"Test case SVT-01304 PASSED"
else
	print*,"Test case SVT-01304 FAILED"
end if
end