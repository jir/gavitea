module Lditd

use Insectcx
use Voper
use Scat_law
implicit none
integer,parameter,private::tark=selected_real_kind(12,30)

contains

subroutine LDISKINTNS(LDI,AR,SSS,NT,EILL,EOBS,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
            YL2_32,dphi1a,dphi2a,ntri,nd1,nd2,nd3)

! Computation of the normalized disk-integrated brightness and its partial
! derivatives for different illumination and observation geometries. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-13

integer,intent(in)::ntri,nd1,nd2,nd3
real(kind=tark),intent(in)::EILL(3,3),EOBS(3,3),AR(260000),NT(260000,3),SSS(10),&
       dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
integer::j1,j2
real(kind=tark)::EI(3),EO(3),N(3),CSCAT(3),D,pg,G1,G2,x2x3,darea,sl
real(kind=tark),intent(out)::LDI

LDI=0.0d0

! Parameters:

pg=SSS(4)
G1=SSS(5)
G2=SSS(6)
D=SSS(7)

! Computation of the normalized disk-integrated brightness;
! illumination and observation directions (in the principal axes
! frame), cosine of phase angle:

do j1=1,3
        EI(j1)=EILL(2,j1)
        EO(j1)=EOBS(2,j1)
end do
CSCAT(3)=EI(1)*EO(1)+EI(2)*EO(2)+EI(3)*EO(3)

! Check all triangular surface elements:

do j1=1,ntri

! Check orientation of the normal vector of the surface element:
        
    do j2=1,3
         N(j2)=NT(j1,j2)
   end do
    call SPRO(CSCAT(1),N,EI)
    if (CSCAT(1).le.0.0d0) then
         goto 150
    endif
    call SPRO(CSCAT(2),N,EO)
    if (CSCAT(2).le.0.0d0) then
         goto 150
    endif

! Disk-integrated brightness:

    sl=SCATLAW(CSCAT,pg,G1,G2,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
          YL2_32,dphi1a,dphi2a,nd1,nd2,nd3)
    darea=(0.5d0*D)**2*AR(j1)
    LDI=LDI+sl*darea
150 end do
end


subroutine LDISKINTNSD(DLDI,AR,SSS,NT,EILL,EOBS,DEI,DEO,XL1,XL2,XL3,YL12,YL2_12,&
             YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,ntri,nd1,nd2,nd3)

! Computation of the normalized disk-integrated brightness and its partial
! derivatives for different illumination and observation geometries. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-13

integer::ntri,nd1,nd2,nd3
real(kind=tark),intent(in)::DEI(3,4),DEO(3,4),EILL(3,3),EOBS(3,3),AR(260000),&
       SSS(10),NT(260000,3),dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
integer::j1,j2,j3
real(kind=tark),dimension(3)::DEI1,DEO1,EI,EO,CSCAT,DCSCAT,N
real(kind=tark)::D,pg,G1,G2,x2x3,darea,dsl
real(kind=tark),intent(out)::DLDI(4)

do j1=1,4
        DLDI(j1)=0.0d0
end do

! Parameters:

pg=SSS(4)
G1=SSS(5)
G2=SSS(6)
D=SSS(7)

! Computation of the normalized disk-integrated brightness;
! illumination and observation directions (in the principal axes
! frame), cosine of phase angle:

do j1=1,3
        EI(j1)=EILL(2,j1)
        EO(j1)=EOBS(2,j1)
end do
CSCAT(3)=EI(1)*EO(1)+EI(2)*EO(2)+EI(3)*EO(3)

! Check all triangular surface elements:

do j1=1,ntri

! Check orientation of the normal vector of the surface element:
        
    do j2=1,3
         N(j2)=NT(j1,j2)
   end do
    call SPRO(CSCAT(1),N,EI)
    if (CSCAT(1).le.0.0d0) then
         goto 150
    endif
    call SPRO(CSCAT(2),N,EO)
    if (CSCAT(2).le.0.0d0) then
         goto 150
    endif

! Partials of disk-integrated brightness:

    do j2=1,4
         do j3=1,3
          DEI1(j3)=DEI(j3,j2)
          DEO1(j3)=DEO(j3,j2)
      end do
         call SPRO(DCSCAT(1),N,DEI1)
         call SPRO(DCSCAT(2),N,DEO1)
         dsl=DSCATLAW(CSCAT,DCSCAT,pg,G1,G2,XL1,XL2,XL3,YL12,YL2_12,YL22,&
        YL2_22,YL32,YL2_32,dphi1a,dphi2a,nd1,nd2,nd3)
         darea=(0.5d0*D)**2*AR(j1)
         DLDI(j2)=DLDI(j2)+dsl*darea
   end do
150 end do
end

end module