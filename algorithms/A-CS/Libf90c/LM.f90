module LM

! USES covsrt,gaussj,mrqcof
use Lditd
use Specfunc
use Randev
use Viewgeo
implicit none
integer,parameter,private::tark=selected_real_kind(12,30),maxpar=2000,maxobs=500,MMAX=2000
integer,private::nacf,NCF(maxpar,2)
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0)
common/ncfvec/NCF/nacoef/nacf

contains

subroutine LMCX(P,PD,SSS,LOBS,LSIG,LCOM,lcommn,RILL,ROBS,TMJD,NT,AT,MUT,PHIT,chisq,&
      chisqa,chisqb,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,&
      dphi2a,nobs,PID,npar,ntri,nca,niter,lmin,lmax,nd1,nd2,nd3)

! Levenberg-Marquardt least-squares iteration.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-13

integer,intent(in)::nobs,PID(2000),ntri,nca,niter,lmin,lmax,nd1,nd2,nd3,npar
real(kind=tark),dimension(maxpar),intent(in)::PD
real(kind=tark),dimension(maxobs),intent(in)::LOBS,LSIG,TMJD
real(kind=tark),dimension(260000),intent(in)::AT,MUT,PHIT
real(kind=tark)::SSS(10),RILL(3,maxobs),ROBS(3,maxobs),NT(260000,3),&
       dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
integer::j1
real(kind=tark)::PCOV(2000,2000),PCURV(2000,2000),alamda
real(kind=tark),intent(inout)::P(maxpar)
real(kind=tark),intent(out)::chisq,chisqa,chisqb,LCOM(maxobs),lcommn

! Solve for the parameters using the Levenberg-Marquardt method:

alamda=-1.0d0
call mrqmins(TMJD,LOBS,LSIG,P,PID,npar,PCOV,PCURV,nca,chisq,alamda,chisqa,chisqb,LCOM,lcommn,&
        SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
        YL2_32,dphi1a,dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)
j1=0
!print*,'iter', j1,real(sqrt((LSIG(1)**2*chisqa+LSIG(nobs+1)**2*chisqb)/(nobs+3))),&
!   real(sqrt(LSIG(1)**2*chisqa/nobs)),real(sqrt(LSIG(nobs+1)**2*chisqb/3.0d0)),&
!   real(alamda)

do j1=1,niter

call mrqmins(TMJD,LOBS,LSIG,P,PID,npar,PCOV,PCURV,nca,chisq,alamda,chisqa,chisqb,LCOM,lcommn,&
        SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
        YL2_32,dphi1a,dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)

!print*,'iter', j1,real(sqrt((LSIG(1)**2*chisqa+LSIG(nobs+1)**2*chisqb)/(nobs+3))),&
!   real(sqrt(LSIG(1)**2*chisqa/nobs)),real(sqrt(LSIG(nobs+1)**2*chisqb/3.0d0)),&
!   real(alamda)

end do

alamda=0.0d0
call mrqmins(TMJD,LOBS,LSIG,P,PID,npar,PCOV,PCURV,nca,chisq,alamda,chisqa,chisqb,LCOM,lcommn,&
        SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
        YL2_32,dphi1a,dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)
end



SUBROUTINE mrqmins(x,y,sig,a,ia,ma,covar,alpha,nca,chisq,alamda,chisq1,chisq2,LCOM,lcommn,&
         SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
         YL32,YL2_32,dphi1a,dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)

integer,intent(in)::ma,nca,ia(maxpar),nobs,ntri,lmin,lmax,nd1,nd2,nd3
real(kind=tark),intent(in)::NT(260000,3),RILL(3,maxobs),ROBS(3,maxobs),SSS(10),dphi1a,dphi2a
real(kind=tark),dimension(maxobs),intent(in)::sig,x,y
real(kind=tark),dimension(maxpar),intent(in)::PD
real(kind=tark),dimension(260000),intent(in)::AT,MUT,PHIT
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
integer::j,k,l
integer,save::ochisq,mfit
real(kind=tark),save,dimension(maxpar)::atry,da
real(kind=tark),dimension(maxpar)::db,beta
real(kind=tark),intent(inout)::a(maxpar)
real(kind=tark),intent(inout)::alamda,LCOM(maxobs),lcommn,chisq,chisq1,chisq2,&
                               alpha(maxpar,maxpar),covar(maxpar,maxpar)

if(alamda.lt.0.0d0)then
   mfit=0
   do j=1,ma
      if (ia(j).ne.0) mfit=mfit+1
   end do
   alamda=0.0001d0
   call mrqcofs(x,y,sig,a,ia,ma,alpha,beta,chisq,chisq1,chisq2,LCOM,lcommn,SSS,NT,AT,MUT,&
        PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,&
        dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)
   ochisq=chisq
   do j=1,ma
      atry(j)=a(j)
   end do
endif

do j=1,mfit
    do k=1,mfit
         covar(j,k)=alpha(j,k)
   end do
   covar(j,j)=alpha(j,j)*(1.0d0+alamda)
   da(j)=beta(j)
end do

call gaussj(covar,mfit,nca,da,1,1)

if(alamda.eq.0.0d0)then
   call covsrt(covar,nca,ma,ia,mfit)
   return
endif
j=0
do l=1,ma
   if(ia(l).ne.0) then
           j=j+1
           atry(l)=a(l)+da(j)
        endif
end do

call mrqcofs(x,y,sig,atry,ia,ma,covar,da,chisq,chisq1,chisq2,LCOM,lcommn,SSS,NT,AT,MUT,&
        PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,&
        dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)

if(chisq.lt.ochisq)then

   alamda=0.1d0*alamda
        ochisq=chisq
        do j=1,mfit
           do k=1,mfit
              alpha(j,k)=covar(j,k)
      end do
            beta(j)=da(j)
   end do
   do l=1,ma
           a(l)=atry(l)
   end do
else
   alamda=10.0d0*alamda
   chisq=ochisq
endif
return
END



SUBROUTINE mrqcofs(x,y,sig,a,ia,ma,alpha,beta,chisq,chisq1,chisq2,LCOM,lcommn,SSS,NT,AT,&
        MUT,PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
        dphi1a,dphi2a,nobs,ntri,lmin,lmax,nd1,nd2,nd3)

integer,intent(in)::ma,ia(maxpar),nobs,ntri,lmin,lmax,nd1,nd2,nd3
real(kind=tark),dimension(maxpar),intent(in)::a,PD
real(kind=tark),dimension(maxobs),intent(in)::sig,x,y
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark),dimension(260000),intent(in)::AT,MUT,PHIT
real(kind=tark),intent(in)::NT(260000,3),RILL(3,maxobs),ROBS(3,maxobs),SSS(10),dphi1a,dphi2a
integer::nalp,mfit,i,j,k,l,m,j1,j2,j3
real(kind=tark),dimension(maxpar)::dyda
real(kind=tark)::dy,sig2i,wt,ymod,DLTMP(1000,maxpar),lmn,LTMP(maxobs)
real(kind=tark),intent(out)::alpha(maxpar,maxpar),beta(maxpar),chisq,chisq1,chisq2,lcommn,&
                             LCOM(maxobs)
	   
mfit=0
do j=1,ma
   if (ia(j).ne.0) mfit=mfit+1
end do
do j=1,mfit
   do k=1,j
        alpha(j,k)=0.0d0
   end do
   beta(j)=0.0d0
end do
chisq=0.0d0
chisq1=0.0d0
chisq2=0.0d0

! Observation part:
i=0
call LDLLCS(x,LTMP,DLTMP,a,SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,lmn,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
      YL32,YL2_32,dphi1a,dphi2a,nobs,ma,ntri,lmin,lmax,nd1,nd2,nd3)
lcommn=lmn
do i=1,nobs
         LCOM(i)=ltmp(i)
         ymod=ltmp(i)/lmn
         do j3=1,ma
            if(ia(j3).ne.0) then
                 dyda(j3)=DLTMP(i,j3)/lmn
             endif
   end do

   sig2i=1.0d0/(sig(i)*sig(i))
   dy=y(i)-ymod
   j=0
   do l=1,ma
      if(ia(l).ne.0) then
         j=j+1
         wt=dyda(l)*sig2i
         k=0
         do m=1,l
            if(ia(m).ne.0) then
               k=k+1
               alpha(j,k)=alpha(j,k)+wt*dyda(m)
!			   if(j==4.or.j==5.or.k==4.or.k==5) print*,m,wt,dyda(m)
            endif
         end do
         beta(j)=beta(j)+dy*wt
      endif
   end do
   chisq1=chisq1+dy*dy*sig2i
end do

! Total chisq:

chisq=chisq1+chisq2

! Full matrix:

do j=2,mfit
   do k=1,j-1
      alpha(k,j)=alpha(j,k)
   end do
end do
return
END

subroutine LDLLCS(TMJD,LTMP,DLTMP,P,SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,lmn,XL1,XL2,XL3,YL12,&
        YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,nobs,npar,ntri,lmin,&
        lmax,nd1,nd2,nd3)

! Disk-integrated brightness and its derivatives for a single lightcurve.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-13

integer,intent(in)::nobs,ntri,lmin,lmax,nd1,nd2,nd3,npar
real(kind=tark),dimension(maxobs),intent(in)::TMJD
real(kind=tark),dimension(maxpar),intent(in)::P,PD
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark),dimension(260000),intent(in)::AT,MUT,PHIT
real(kind=tark),intent(in)::NT(260000,3),RILL(3,maxobs),ROBS(3,maxobs),&
       SSS(10),dphi1a,dphi2a
integer::j1,j2
real(kind=tark),dimension(maxpar)::DLTMP1,DLMN
real(kind=tark)::RILL1(3),ROBS1(3)
real(kind=tark),intent(out)::lmn,DLTMP(1000,maxpar),LTMP(maxobs)

! Normalized disk-integrated brightness:

!x,LTMP,DLTMP,a,SSS,

lmn=0.0d0
do j1=1,nobs
    do j2=1,3
      ROBS1(j2)=ROBS(j2,j1)
      RILL1(j2)=RILL(j2,j1)
   end do
   call LDI1S(TMJD(j1),LTMP(j1),DLTMP1,P,SSS,NT,AT,MUT,PHIT,RILL1,ROBS1,PD,&
         XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,npar,ntri,&
         lmin,lmax,nd1,nd2,nd3)
   lmn=lmn+LTMP(j1)
   do j2=1,npar
        DLTMP(j1,j2)=DLTMP1(j2)
   end do
end do
lmn=lmn/nobs

end


subroutine LDI1S(tmjd,ldi,DLTMP,P,SSS,NT,AT,MUT,PHIT,RILL,ROBS,PD,XL1,XL2,XL3,YL12,&
       YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,npar,ntri,lmin,lmax,nd1,nd2,nd3)

! Disk-integrated brightness and its derivatives for a single date.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::ntri,lmin,lmax,nd1,nd2,nd3,npar
real(kind=tark),dimension(maxpar),intent(in)::P,PD
real(kind=tark),dimension(260000),intent(in)::AT,MUT,PHIT
real(kind=tark)::NT(260000,3),RILL(3),ROBS(3),SSS(10),tmjd,dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
integer::j0,j1,j2
real(kind=tark)::D,pg,G1,G2,tmjd0,armn0,armn,ldi1,DEI(3,4),DEO(3,4),SPIN1(4),EILL(3,3),EOBS(3,3),&
                 DAR1(260000),DAR(7200,256),SS(4),SD(4),DLDI(4),SPIN(4),AR(260000)
real(kind=tark),dimension(0:256,0:256)::ACF,BCF
real(kind=tark),intent(out)::ldi,DLTMP(maxpar)

! Parameters:

pg=SSS(4)
G1=SSS(5)
G2=SSS(6)
D=SSS(7)
tmjd0=SSS(8)
! Spin parameters and spherical harmonics coefficients:

do j0=1,4
        SPIN(j0)=P(j0)
end do
do j0=10,nacf
        ACF(NCF(j0,1),NCF(j0,2))=P(j0)
end do
do j0=nacf+1,npar
        BCF(NCF(j0,1),NCF(j0,2))=P(j0)
end do

! Areas and their partial derivatives:
call ARDAR(AR,DAR,ACF,BCF,AT,MUT,PHIT,npar,ntri,lmin,lmax)

! Disk-integrated brightness:

call ILLOBSD(SPIN,RILL,ROBS,EILL,EOBS,DEI,DEO,SS,tmjd,tmjd0)
call LDISKINTNS(ldi,AR,SSS,NT,EILL,EOBS,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
            YL32,YL2_32,dphi1a,dphi2a,ntri,nd1,nd2,nd3)
! Partial derivatives wrt spherical harmonics coefficients:

do j0=10,npar
   do j1=1,ntri
           DAR1(j1)=DAR(j1,j0)
   end do
   call LDISKINTNS(DLTMP(j0),DAR1,SSS,NT,EILL,EOBS,XL1,XL2,XL3,YL12,YL2_12,YL22,&
          YL2_22,YL32,YL2_32,dphi1a,dphi2a,ntri,nd1,nd2,nd3)
end do
! Partial derivatives wrt spin parameters:

call LDISKINTNSD(DLDI,AR,SSS,NT,EILL,EOBS,DEI,DEO,XL1,XL2,XL3,YL12,YL2_12,YL22,&
       YL2_22,YL32,YL2_32,dphi1a,dphi2a,ntri,nd1,nd2,nd3)
do j0=1,4
        DLTMP(j0)=DLDI(j0)
end do
end



subroutine ARDAR(AR,DAR,ACF,BCF,AT,MUT,PHIT,npar,ntri,lmin,lmax)

integer,intent(in)::ntri,lmin,lmax,npar
real(kind=tark),dimension(260000),intent(in)::AT,MUT,PHIT
real(kind=tark),dimension(0:256,0:256),intent(in)::ACF,BCF
integer::l,m,j1,j2
real(kind=tark)::CPHI(0:256),SPHI(0:256),SGS,mu,phi,armn0,armn,LEGP(0:256,0:256)
real(kind=tark),intent(out)::DAR(7200,256),AR(260000)

armn0=4.0d0*pi/ntri
CPHI(0)=1.0d0
SPHI(0)=0.0d0

do j1=1,ntri
    do j2=10,npar
         DAR(j1,j2)=0.0d0
   end do
end do


armn=0.0d0
do j1=1,ntri
   mu=MUT(j1)
   phi=PHIT(j1)

! Precomputation of sines, cosines, and associated Legendre functions:

   call LEGAN(LEGP,mu,lmax,0)
   do m=1,lmax
      call LEGAN(LEGP,mu,lmax,m)
      CPHI(m)=cos(m*phi)
      SPHI(m)=sin(m*phi)
   end do
   LEGP(0,0)=1.0d0/sqrt(2.0d0)

! Sum up:

   SGS=0.0d0
   do l=lmin,lmax
      SGS=SGS+LEGP(l,0)*ACF(l,0)
   end do
   do m=1,lmax
      do l=max(m,lmin),lmax
         SGS=SGS+LEGP(l,m)*(ACF(l,m)*CPHI(m)+BCF(l,m)*SPHI(m))
      end do
   end do

   AR(j1)=exp(SGS)*AT(j1)
   armn=armn+AR(j1)
   do j2=10,nacf
      DAR(j1,j2)=LEGP(NCF(j2,1),NCF(j2,2))*CPHI(NCF(j2,2))*AR(j1)
   end do
   do j2=nacf+1,npar
         DAR(j1,j2)=LEGP(NCF(j2,1),NCF(j2,2))*SPHI(NCF(j2,2))*AR(j1)
   end do
end do
armn=armn/ntri

end


SUBROUTINE gaussj(a,n,np,b,m,mp)

integer,intent(in)::m,mp,n,np
real(kind=tark),intent(inout)::a(np,np)
integer::i,icol,irow,j,k,l,ll,indxc(MMAX),indxr(MMAX),ipiv(MMAX)
real(kind=tark)::big,dum,pivinv
real(kind=tark),intent(out)::b(np,mp)

!covar,mfit,nca,da,1,1
do j=1,n
    ipiv(j)=0
end do
do i=1,n
    big=0.0d0
    do j=1,n
      if(ipiv(j).ne.1)then
        do k=1,n
          if (ipiv(k).eq.0) then
            if (abs(a(j,k)).ge.big)then
              big=abs(a(j,k))
              irow=j
              icol=k
            endif
          else if (ipiv(k).gt.1) then
            call exit(80)
          endif
      end do
      endif
   end do
   ipiv(icol)=ipiv(icol)+1
   if (irow.ne.icol) then
      do l=1,n
         dum=a(irow,l)
         a(irow,l)=a(icol,l)
         a(icol,l)=dum
      end do
      do l=1,m
         dum=b(irow,l)
         b(irow,l)=b(icol,l)
         b(icol,l)=dum
      end do
   endif
   indxr(i)=irow
   indxc(i)=icol
   if (a(icol,icol).eq.0.0d0) then
			print*,"a",icol,icol,"==0 at i=",i,"of",n
		    print*,"Exit gaussj"
            call exit(80)
   endif
   pivinv=1.0d0/a(icol,icol)
   a(icol,icol)=1.0d0
   do l=1,n
         a(icol,l)=a(icol,l)*pivinv
   end do
   do l=1,m
           b(icol,l)=b(icol,l)*pivinv
   end do
   do ll=1,n
      if(ll.ne.icol)then
         dum=a(ll,icol)
         a(ll,icol)=0.0d0
         do l=1,n
            a(ll,l)=a(ll,l)-a(icol,l)*dum
         end do
         do l=1,m
            b(ll,l)=b(ll,l)-b(icol,l)*dum
         end do
      endif
   end do
end do
do l=n,1,-1
    if(indxr(l).ne.indxc(l))then
        do k=1,n
             dum=a(k,indxr(l))
             a(k,indxr(l))=a(k,indxc(l))
             a(k,indxc(l))=dum
      end do
   endif
end do
return
END



SUBROUTINE covsrt(covar,npc,ma,ia,mfit)

integer,intent(in)::ma,mfit,npc,ia(ma)
integer::i,j,k
real(kind=tark)::swap
real(kind=tark),intent(inout)::covar(npc,npc)

do i=mfit+1,ma
    do j=1,i
        covar(i,j)=0.0d0
        covar(j,i)=0.0d0
   end do
end do
k=mfit
do j=ma,1,-1
    if(ia(j).ne.0)then
        do i=1,ma
             swap=covar(i,k)
             covar(i,k)=covar(i,j)
             covar(i,j)=swap
      end do
        do i=1,ma
             swap=covar(k,i)
             covar(k,i)=covar(j,i)
             covar(j,i)=swap
      end do
        k=k-1
   endif
end do
END





subroutine LSTRES(LSIG,lrms,LOBS,LCOM,lcommn,nobs)

! Computes internal correlations of the O-C residuals for 
! each lightcurve and returns the corrected error standard deviation 
! for each lightcurve.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::nobs
real(kind=tark),dimension(maxobs),intent(in)::LOBS,LCOM
real(kind=tark),intent(in)::lcommn
integer::j0,j1,j2
real(kind=tark)::lccf,lresmn,LRESCV(2),lcomn1,lcomn2,sc
real(kind=tark),intent(out)::LSIG(maxobs),lrms

! Residual means:

lresmn=0.0d0
do j0=1,nobs
         lcomn1=LCOM(j0)/lcommn
         lresmn=lresmn+(LOBS(j0)-lcomn1)
end do
lresmn=lresmn/nobs

! Residual nearest neighbor covariances:

LRESCV(1)=0.0d0
LRESCV(2)=0.0d0
do j0=1,nobs-1

! Compute 2x2 covariance matrix:

    lcomn1=LCOM(j0)  /lcommn
         lcomn2=LCOM(j0+1)/lcommn
         LRESCV(1)=LRESCV(1)+(LOBS(j0)-lcomn1)*(LOBS(j0)  -lcomn1)
         LRESCV(2)=LRESCV(2)+(LOBS(j0)-lcomn1)*(LOBS(j0+1)-lcomn2)
end do
j0=nobs
lcomn1=LCOM(j0)  /lcommn
lcomn2=LCOM(j0-nobs+1)/lcommn
LRESCV(1)=LRESCV(1)+(LOBS(j0)-lcomn1)*(LOBS(j0)-lcomn1)
LRESCV(2)=LRESCV(2)+(LOBS(j0)-lcomn1)*(LOBS(j0-nobs+1)-lcomn2)
do j2=1,2
         LRESCV(j2)=LRESCV(j2)/nobs-lresmn**2
end do

! Compute scale factor to approximate 1-sigma level of correlation:

sc=sqrt(abs((LRESCV(2)/LRESCV(1))/(1.0d0/sqrt(dble(nobs)))))

if (sc.le.1.0d0) then
         lccf=1.0d0
else 
         lccf=sc
endif

! Set the observational noise and scale it to approximate 
! 1-sigma level of correlation:

lrms=sqrt(LRESCV(1))
do j0=1,nobs
   LSIG(j0)=lccf*lrms
end do
end





subroutine CXPROPOSE(P,P0,PV,PID,nmc,npar)

! Proposes new parameters for MCMC.
! 
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::PID(maxpar),nmc,npar
real(kind=tark)::PV(maxpar,1000),P0(maxpar)
integer::j1,k1,k2,nprop
real(kind=tark),intent(out)::P(maxpar)

do j1=1,npar
        P(j1)=P0(j1)
end do
nprop=0
! Propose:
100  nprop=nprop+1
if(nprop>1000) call exit(65)
k1=1
k2=k1
do while (k1.eq.k2)
   k1=int(RAN2()*nmc)+1
   k2=int(RAN2()*nmc)+1
end do
do j1=1,npar
        if (PID(j1).gt.0) P(j1)=P0(j1)+PV(j1,k1)-PV(j1,k2)
end do
! Sanity verification:
if (P(3)>0.5d0*pi .or. P(3)<-0.5d0*pi.or.P(5)<0.0d0) then
	print*,"Spin axis lat or rot phase failed"
	goto 100
endif

if (P(6) .lt.0.0d0 .or. P(6) .gt.1.0d0) then
	print*,'ellipsoid axis b not in allowed range'
   goto 100
endif
! Enforce pole longitude and rotational phase within [0, 2pi]:
do j1=2,4,2
    if (P(j1).gt.2.0d0*pi)&
      P(j1)=P(j1)-2.0d0*pi*(int((P(j1)-2.0d0*pi)/(2.0d0*pi))+1)
    if (P(j1).lt.0.0d0)&
        P(j1)=P(j1)+2.0d0*pi*(int((2.0d0*pi-P(j1))/(2.0d0*pi)))
end do
end

end module