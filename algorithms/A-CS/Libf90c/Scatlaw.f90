module Scat_law

use HG1G2
implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark),public::SCATLAW,DSCATLAW

contains

function SCATLAW(CSCAT,pg,G1,G2,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
       YL2_32,dphi1a,dphi2a,nd1,nd2,nd3)

! Empirical scattering law.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::nd1,nd2,nd3
real(kind=tark),intent(in)::CSCAT(3),pg,G1,G2,dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark)::G12,alpha,pfr

alpha=acos(CSCAT(3))
G12=G1
pfr=PHIHG12(G12,alpha,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,&
            dphi1a,dphi2a,nd1,nd2,nd3)/PHILS(alpha)
!!! Karri 2017-01-30
!!!SCATLAW=0.5d0*pg*pfr*CSCAT(1)*CSCAT(2)/(CSCAT(1)+CSCAT(2))
SCATLAW=2.0d0*pg*pfr*CSCAT(1)*CSCAT(2)/(CSCAT(1)+CSCAT(2))
!!!
end




function DSCATLAW(CSCAT,DCSCAT,pg,G1,G2,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
        YL32,YL2_32,dphi1a,dphi2a,nd1,nd2,nd3)

! Empirical scattering law.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::nd1,nd2,nd3
real(kind=tark),intent(in)::CSCAT(3),DCSCAT(3),pg,G1,G2,dphi1a,dphi2a
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark)::G12,alpha,pfr

alpha=acos(CSCAT(3))
G12=G1
pfr=PHIHG12(G12,alpha,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,&
            YL2_32,dphi1a,dphi2a,nd1,nd2,nd3)/PHILS(alpha)
pfr=pfr/(CSCAT(1)+CSCAT(2))**2
!!! Karri 2017-01-30
!!!DSCATLAW=0.5d0*pg*pfr*(DCSCAT(2)*CSCAT(1)**2+DCSCAT(1)*CSCAT(2)**2)
DSCATLAW=2.0d0*pg*pfr*(DCSCAT(2)*CSCAT(1)**2+DCSCAT(1)*CSCAT(2)**2)
!!!
end

end module
