module Numint

use Specfunc
implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0),EPS=3.0d-14

contains

subroutine ACFBCFN(ACF,BCF,S,MU,PHI,WI,nmu,nphi,lmax)

! ACFBCF computes the spherical harmonics coefficients for a given
! function.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17
!
! Something wrong here!!!

integer,intent(in)::nmu,nphi,lmax
real(kind=tark),intent(in)::MU(512),PHI(0:720),WI(512),S(512,0:720)
integer::j1,j2,j3,j4
real(kind=tark)::CAZ(256),SAZ(256),norm1,norm2,LEGPLM(0:256,0:256)
real(kind=tark),dimension(0:256,0:256),intent(out)::ACF,BCF

! Compute spherical harmonics coefficients with respect to the
! given origin:

 do j1=0,lmax
    do j2=0,j1
         ACF(j1,j2)=0.0d0
         BCF(j1,j2)=0.0d0
   end do
end do

do j1=1,nmu
   call LEGAN(LEGPLM,MU(j1),lmax,0)
   do j2=1,lmax
       call LEGAN(LEGPLM,MU(j1),lmax,j2)
   end do
       do j2=0,nphi-1
           do j3=1,lmax
                CAZ(j3)=cos(j3*PHI(j2))
                SAZ(j3)=sin(j3*PHI(j2))
      end do
      do j3=0,lmax
            ACF(j3,0)=ACF(j3,0)+WI(j1)*S(j1,j2)*LEGPLM(j3,0)*2.0d0*pi/nphi
      end do
      do j3=1,lmax
         do j4=1,j3
            ACF(j3,j4)=ACF(j3,j4)+WI(j1)*&
                  S(j1,j2)*LEGPLM(j3,j4)*CAZ(j4)*2.0d0*pi/nphi
            BCF(j3,j4)=BCF(j3,j4)+WI(j1)*&
                  S(j1,j2)*LEGPLM(j3,j4)*SAZ(j4)*2.0d0*pi/nphi
         end do
      end do
   end do
end do

! Final normalization:
do j1=0,lmax
        norm1=1.0d0/(2.0d0*pi)
        ACF(j1,0)=norm1*ACF(j1,0)
end do

do j1=1,lmax
    norm1=2.0d0/(2.0d0*pi)
    do j2=1,j1
         norm2=norm1
         ACF(j1,j2)=norm2*ACF(j1,j2)
         BCF(j1,j2)=norm2*BCF(j1,j2)
   end do
end do
end


SUBROUTINE gauleg(x1,x2,x,w,n)

! Gauss-Legendre quadrature, weights for numerical integration
integer,intent(in)::n
real(kind=tark)::x1,x2
integer::i,j,m
real(kind=tark)::p1,p2,p3,pp,xl,xm,z,z1
real(kind=tark),intent(out)::x(n),w(n)

m=(n+1)/2
! Change of integration limits from -1,1 to x1,x2
xm=0.5d0*(x2+x1)
xl=0.5d0*(x2-x1)

do i=1,m
   z=cos(PI*(i-0.25d0)/(n+0.5d0))
1  p1=1.0d0
   p2=0.0d0
   do j=1,n
            p3=p2
            p2=p1
            p1=((2.0d0*j-1.0d0)*z*p2-(j-1.0d0)*p3)/j
   end do
   pp=n*(z*p1-p2)/(z*z-1.0d0)
   z1=z
   z=z1-p1/pp
    if(abs(z-z1).gt.EPS) goto 1
    x(i)=xm-xl*z
    x(n+1-i)=xm+xl*z
    w(i)=2.0d0*xl/((1.0d0-z*z)*pp*pp)
    w(n+1-i)=w(i)
end do
end

end module