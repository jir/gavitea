module vectprocs

implicit none
integer,parameter,private::tark=selected_real_kind(12,30),nmax=6000
real(kind=tark),dimension(0:nmax),private::x,y,z
real(kind=tark),public::dot
integer,public::modlo
common/points/x,y,z

contains

subroutine crossproduct(avect,bvect,crossvect)
! The cross product of two vectors
real(kind=tark),intent(in)::avect(3),bvect(3)
real(kind=tark),intent(out)::crossvect(3)

crossvect(1)=avect(2)*bvect(3)-avect(3)*bvect(2)
crossvect(2)=avect(3)*bvect(1)-avect(1)*bvect(3)
crossvect(3)=avect(1)*bvect(2)-avect(2)*bvect(1)
end


function dot(avect,bvect)
! The dot product of two vectors
real(kind=tark),intent(in)::avect(3),bvect(3)
integer::n

dot=0.
do n=1,3
	dot=dot+avect(n)*bvect(n)
end do
end


function modlo(index,modsize)
! The modulo function
integer,intent(in)::index,modsize

if (index.gt.modsize) then
    modlo=index-modsize
else
    if (index.lt.1) then
        modlo=index+modsize
	else
        modlo=index
    end if
end if
end


subroutine vector(start,fin,vect)
! A procedure for constructing a vector from two points
integer,intent(in)::fin,start
real(kind=tark),intent(out)::vect(3)

vect(1)=x(fin)-x(start)
vect(2)=y(fin)-y(start)
vect(3)=z(fin)-z(start)
end

end module
