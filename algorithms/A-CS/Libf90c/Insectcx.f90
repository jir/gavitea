module Insectcx

! Particle-line intersections

! ISTRICX: intersection with a triangle-discretized object
! ISPLANE: intersection with a plane

use Voper
implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark),parameter,private::tol=1.0d-14

contains

subroutine ISTRI(X,K,N,XN,NT,IT,nk,s3,ntri,nis,nie)

! Determines the ray intersection point on the discretized object
! from the given position and unit direction vectors.  

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-13

integer,intent(in)::IT(260000,3),ntri,nie
real(kind=tark),intent(in)::XN(130000,3),NT(260000,3),K(3)
integer::j1,j2
real(kind=tark)::X1(3),X2(3),X3(3),S(3),D(2),U3(3),H(3),kx,p1,p2,p3,q1,q2,q3
real(kind=tark),intent(inout)::X(3)
integer,intent(out)::nis
real(kind=tark),intent(out)::N(3),s3,nk
       
! Initialize:
nis=0
s3=1.0d10

! Check all triangles:      
do 40 j1=1,ntri
! Triangle orientation must allow interaction:
    do j2=1,3
         N(j2)=nie*NT(j1,j2)
   end do
    call SPRO(nk,N,K)
    if (nk.ge.0.0d0) goto 40
! Triangle located behind the ray path:
    do j2=1,3
         X1(j2)=XN(IT(j1,1),j2)-X(j2)
         X2(j2)=XN(IT(j1,2),j2)-X(j2)
         X3(j2)=XN(IT(j1,3),j2)-X(j2)
    end do
    call SPRO(p1,K,X1)
    call SPRO(p2,K,X2)
    call SPRO(p3,K,X3)
    if (p1.lt.0.0d0 .and. p2.lt.0.0d0 .and. p3.lt.0.0d0) goto 40

! Triangle located fully under/above the ray path: 
    call SPRO(q1,X,X1)
    call SPRO(q2,X,X2)
    call SPRO(q3,X,X3)
    call SPRO(kx,K,X)
    p1=q1-kx*p1
    p2=q2-kx*p2
    p3=q3-kx*p3
    if (p1.lt.0.0d0 .and. p2.lt.0.0d0 .and. p3.lt.0.0d0) goto 40
    if (p1.gt.0.0d0 .and. p2.gt.0.0d0 .and. p3.gt.0.0d0) goto 40
        
! Triangle located fully aside the ray path: 
    call VPRO(H,K,X) 
    call SPRO(p1,H,X1)
    call SPRO(p2,H,X2)
    call SPRO(p3,H,X3)
    if (p1.lt.0.0d0 .and. p2.lt.0.0d0 .and. p3.lt.0.0d0) goto 40
    if (p1.gt.0.0d0 .and. p2.gt.0.0d0 .and. p3.gt.0.0d0) goto 40

! Triangle intersection:
    do j2=1,3
         U3(j2)=NT(j1,j2)
         X1(j2)=XN(IT(j1,1),j2)
         X2(j2)=XN(IT(j1,2),j2)
         X3(j2)=XN(IT(j1,3),j2)
   end do
    call ISPLANE(S,X,K,U3,D,X1,X2,X3)

! Intersection update:
    if (S(1)>=0.0d0.and.S(2)>=0.0d0.and.D(1)*S(2)+D(2)*S(1)<D(1)*D(2)&
      .and.S(3)>=0.0d0.and.S(3)<=s3) then
         nis=j1
         s3=S(3)
    endif
40 end do

! Compute intersection point and normal:
if (nis.gt.0) then
    do j1=1,3
         X(j1)=X(j1)+s3*K(j1)
         N(j1)=nie*NT(nis,j1)
   end do
    call SPRO(nk,N,K)
endif
end


subroutine ISPLANE(S,X,K,U3,D,X1,X2,X3)

! Computes the intersection point on the plane determined
! by the three given positions.
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-13

real(kind=tark),intent(in)::X(3),K(3),X1(3),X2(3),X3(3),U3(3)
integer j1
real(kind=tark)::U1(3),U2(3),DX(3),uu,u,u1dx,u2dx,u3dx,u1k,u2k,u3k,v,w
real(kind=tark),intent(out)::S(3),D(2)

call VDIFN(U1,D(1),X2,X1)
call VDIFN(U2,D(2),X3,X1)
       
do j1=1,3
        DX(j1)=X1(j1)-X(j1)
end do

call SPRO(u3dx,U3,DX)
if  (abs(u3dx).lt.tol) then
        S(3)=0.0d0
else
        call SPRO(u3k,U3,K)
        if (abs(u3k).lt.tol) then
         print*,U3,K,u3k,u3dx
         stop 'Trouble in ISPLANE: direction parallel to plane.'
        endif
        S(3)=u3dx/u3k
endif

call SPRO(uu,U1,U2)
u=1.0d0-uu**2
if (abs(u).lt.tol) then
        print*,U1,U2,u
        stop 'Trouble in ISPLANE: plane ill-defined.'
endif 
call SPRO(u1dx,U1,DX)
call SPRO(u2dx,U2,DX)
call SPRO(u1k,U1,K)
call SPRO(u2k,U2,K)
v=-u1dx+S(3)*u1k
w=-u2dx+S(3)*u2k
S(1)=(v-uu*w)/u
S(2)=(w-uu*v)/u
end

end module