module Voper

! Vector calculus:
!
! VROTEU:  vector rotation using Euler angles
! VROTY:   vector rotation about the y-axis
! VROTZ:   vector rotation about the z-axis
! VPRO:    vector product
! SPRO:    scalar product
! VDIFN:   normalized vector difference

implicit none
integer,parameter,private::tark=selected_real_kind(12,30)

contains

subroutine VROTEU(X,CA,SA)
! Vector rotation using Euler angles. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

real(kind=tark),dimension(3),intent(in)::CA,SA
real(kind=tark),dimension(3),intent(out)::X

call VROTZ(X,CA(1),SA(1))
call VROTY(X,CA(2),SA(2))
call VROTZ(X,CA(3),SA(3))
end


subroutine VROTY(X,c,s)

! Vector rotation about the y-axis. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-23

real(kind=tark),intent(in)::c,s
real(kind=tark)::q
real(kind=tark),intent(out)::X(3)

q   = c*X(3)+s*X(1)
X(1)=-s*X(3)+c*X(1)
X(3)=q
end



subroutine VROTZ(X,c,s)

! Vector rotation about the z-axis. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-23

real(kind=tark),intent(in)::c,s
real(kind=tark)::q
real(kind=tark),intent(out)::X(3)

q   = c*X(1)+s*X(2)
X(2)=-s*X(1)+c*X(2)
X(1)=q
end



subroutine VROTYD(X,c,s)

! Derivative of vector rotation about the y-axis. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

real(kind=tark),intent(in)::c,s
real(kind=tark)::q
real(kind=tark),intent(out)::X(3)

q   =-s*X(3)+c*X(1)
X(1)=-c*X(3)-s*X(1)
X(3)=q
X(2)=0.0d0
end



subroutine VROTZD(X,c,s)

! Derivative of vector rotation about the z-axis. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

real(kind=tark),intent(in)::c,s
real(kind=tark)::q
real(kind=tark),intent(out)::X(3)

q   =-s*X(1)+c*X(2)
X(2)=-c*X(1)-s*X(2)
X(1)=q
X(3)=0.0d0
end


subroutine VPRO(XY,X,Y)

! Vector product. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

real(kind=tark),dimension(3),intent(in)::X,Y
real(kind=tark),dimension(3),intent(out)::XY

XY(1)=X(2)*Y(3)-X(3)*Y(2)
XY(2)=X(3)*Y(1)-X(1)*Y(3)
XY(3)=X(1)*Y(2)-X(2)*Y(1)    
end



subroutine SPRO(XY,X,Y)

! Scalar product. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

real(kind=tark),dimension(3),intent(in)::X,Y
real(kind=tark),intent(out)::XY

XY=X(1)*Y(1)+X(2)*Y(2)+X(3)*Y(3)    
end


subroutine VDIFN(XY,dxy,X,Y)

! Normalized difference of two vectors. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

real(kind=tark),dimension(3),intent(in)::X,Y
integer j1
real(kind=tark),intent(out)::XY(3),dxy
       
dxy=0.0d0
do j1=1,3
        XY(j1)=X(j1)-Y(j1)
        dxy=dxy+XY(j1)**2
end do
if (dxy.eq.0.0d0) then
        print*,X
        print*,Y  
        call exit(100)
endif
dxy=sqrt(dxy)
do j1=1,3
        XY(j1)=XY(j1)/dxy
end do
end

end module