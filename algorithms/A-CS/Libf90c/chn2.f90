module chn2

use vectprocs
implicit none
integer,parameter,private::tark=selected_real_kind(12,30),nmax=6000,tiny=1e-8

contains

subroutine convhull(numedges,edge,nedge1,nedge2,errc)

integer::numedges(nedge1),edge(nedge1,nedge2),listch(nedge1),inlist(nedge1),&
		 newver,numfaces,mind,i,j,mind2,nvert,nconn,inda,indb,indn,nedge1,nedge2,errc
real(kind=tark),dimension(3)::stick,tvec,base,vectn,crvec,cvec
real(kind=tark),dimension(0:nmax)::x,y,z
real(kind=tark)::zmax,dtest,sticklen,dmin,tveclen,clen,crlen
common/points/x,y,z/nf/numfaces

! origin is the zeroth point (if not inside, use cmass)
x(0)=0.
y(0)=0.
z(0)=0.
errc=0
! First find the largest z-value (any min,max,coord would do) to establish
! one vertex of the CH
zmax=z(1)
mind=1
do i=2,numfaces
        if (z(i).gt.zmax) then
          	zmax=z(i)
          	mind=i
        end if
end do
! listch is the list of all CH vertices; inlist is either 1 or 0 depending
! on whether the point is in listch
do i=1,numfaces
        inlist(i)=0
        numedges(i)=0
end do 
listch(1)=mind
inlist(mind)=1
! Then find the point with the largest 'umbrella angle' from the first 
! point (umbrella 'stick' towards origin) to establish one edge of the CH
call vector(mind,0,stick)
sticklen=sqrt(dot(stick,stick))
dmin=1.
do i=1,numfaces
        if (i.ne.mind) then
        	call vector(mind,i,tvec)
          	tveclen=sqrt(dot(tvec,tvec))
          	dtest=dot(stick,tvec)/(sticklen*tveclen)
          	if (dtest.lt.dmin) then
           		dmin=dtest
           		mind2=i
          	end if
        end if
end do
listch(2)=mind2
inlist(mind2)=1
edge(mind,1)=mind2
edge(mind2,1)=mind
numedges(mind)=1
numedges(mind2)=1

! nvert counts the vertices for which all edges have been  determined; 
! nconn counts the points that have been connected to any other point (and
! are thus vertices of CH)
nvert=0
nconn=2

! Begin nvert-loop
10 inda=listch(nvert+1)
indb=edge(inda,1)
call vector(inda,indb,base)
! Begin numedges-loop: find a new edge for the vertex inda.
! Go through all points; i is the trial point, indn the reference point
20 do i=0,numfaces
        if ((i.ne.inda).and.(i.ne.indb)) then
          	call vector(inda,i,cvec)
          	clen=sqrt(dot(cvec,cvec))
          	if ((i.eq.0).or.(dot(cvec,crvec).lt.(clen*crlen*(-tiny)))) then
! the trial point becomes the new reference point (.lt.(-tiny) in the above
! test for counterclockwise ordering of edges, .gt.tiny for clockwise 
! ordering)
           		indn=i
           		do j=1,3
            		vectn(j)=cvec(j)
           		end do
           		call crossproduct(vectn,base,crvec)
           		crlen=sqrt(dot(crvec,crvec))
          	end if
        end if
end do

! check if we've not yet gone round inda (not found all its edges)
newver=0
do i=1,numedges(inda)
	if (indn.eq.edge(inda,i)) newver=1
end do
if (numedges(inda)==1.or.newver==0) then
! check if a new vertex of CH has been found
        if (indn==0) then
		    print*,"Problem in convex hull computation (chn2.f90)"
			errc=1
			return
		endif
        if (inlist(indn).eq.0) then
          	nconn=nconn+1
          	listch(nconn)=indn
          	inlist(indn)=1
          	numedges(indn)=1
          	edge(indn,1)=inda
        end if
        numedges(inda)=numedges(inda)+1
		edge(inda,numedges(inda))=indn
        indb=indn
        do j=1,3
          	base(j)=vectn(j)
        end do
        goto 20
end if
nvert=nvert+1
! check if we haven't yet found edges for all connected points (otherwise 
! we have determined the CH)
if (nvert.lt.nconn) goto 10
end
end module



