module Viewgeo

use Voper
implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0),rd=pi/180.0d0,sd=24.0d0*3600.0d0

contains

subroutine ILLOBS(SPIN,RILL,ROBS,EILL,EOBS,SS,CEU,SEU,tdt,tdt0)

! Compute the solar and observer directions in the ecliptic and
! principal axes reference frames. 
!
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-13

! This is from ellipsoid subroutines, not from convex. Is it the same?? 

real(kind=tark),dimension(3),intent(in)::RILL,ROBS
real(kind=tark),intent(in)::SPIN(4),tdt,tdt0
integer j1
real(kind=tark),dimension(3)::EU,EI,EO,ES,EIO,EN
real(kind=tark)::norm,rphs
real(kind=tark),intent(out)::CEU(3),SEU(3),EILL(3,3),EOBS(3,3),SS(4)

! Euler cosines and sines:

EU(1)=SPIN(2)           ! Euler gamma (Weissbluth, p.54)
EU(2)=0.5d0*pi-SPIN(3)  ! Euler beta
EU(3)=SPIN(4)           ! Euler alpha0

CEU(1)=cos(EU(1))
CEU(2)=cos(EU(2))
SEU(1)=sin(EU(1))
SEU(2)=sqrt(1.0d0-CEU(2)**2)

! Observer and illumination directions in the two reference
! frames; ecliptic reference frame (1):

EI(1)=cos(RILL(2))*cos(RILL(3))
EI(2)=sin(RILL(2))*cos(RILL(3))
EI(3)=sin(RILL(3))
EO(1)=cos(ROBS(2))*cos(ROBS(3))
EO(2)=sin(ROBS(2))*cos(ROBS(3))
EO(3)=sin(ROBS(3))
do j1=1,3
   EILL(1,j1)=EI(j1)
   EOBS(1,j1)=EO(j1)
end do

! Scalar products:

ES(1)=cos(SPIN(2))*cos(SPIN(3))
ES(2)=sin(SPIN(2))*cos(SPIN(3))
ES(3)=sin(SPIN(3))
call SPRO(SS(1),ES,EI)
call SPRO(SS(2),ES,EO)
   
norm=0.0d0
do j1=1,3
   EIO(j1)=EI(j1)+EO(j1)
   norm=norm+EIO(j1)**2
end do
do j1=1,3
   EIO(j1)=EIO(j1)/sqrt(norm)
end do
call SPRO(SS(3),ES,EIO)

call VPRO(EN,EI,EIO)
norm=0.0d0
do j1=1,3
   norm=norm+EN(j1)**2
end do
do j1=1,3
   EN(j1)=EN(j1)/sqrt(norm)
end do
call SPRO(SS(4),ES,EN)
SS(4)=SS(4)/sqrt(1.0d0-SS(3)**2)

! Principal axes reference frame (2):

rphs=SPIN(4)+2.0d0*pi*((tdt-tdt0)*sd/SPIN(1)-int((tdt-tdt0)*sd/SPIN(1)))
CEU(3)=cos(rphs)
SEU(3)=sin(rphs)
call VROTEU(EI,CEU,SEU)
call VROTEU(EO,CEU,SEU)
do j1=1,3
   EILL(2,j1)=EI(j1)
   EOBS(2,j1)=EO(j1)
end do
end




subroutine ILLOBSD(SPIN,RILL,ROBS,EILL,EOBS,DEI,DEO,SS,tdt,tdt0)

! Compute the solar and observer directions in the ecliptic and
! principal axes reference frames and the related partial derivatives.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

real(kind=tark),dimension(3),intent(in)::RILL,ROBS
real(kind=tark),intent(in)::SPIN(4),tdt,tdt0
real(kind=tark),dimension(3)::EU,CEU,SEU,EI,EO,EI1,EO1,ES,EIO,EN
real(kind=tark)::norm,rphs,drphs
integer::j1
real(kind=tark),intent(out)::EILL(3,3),EOBS(3,3),DEI(3,4),DEO(3,4),SS(4)

! Euler cosines and sines:

EU(1)=SPIN(2)           ! Euler gamma (Weissbluth, p.54)
EU(2)=0.5d0*pi-SPIN(3)  ! Euler beta
EU(3)=SPIN(4)           ! Euler alpha0

CEU(1)=cos(EU(1))
CEU(2)=cos(EU(2))
SEU(1)=sin(EU(1))
SEU(2)=sqrt(1.0d0-CEU(2)**2)

! Observer and illumination directions in the two reference
! frames; ecliptic reference frame (1):

EI(1)=cos(RILL(2))*cos(RILL(3))
EI(2)=sin(RILL(2))*cos(RILL(3))
EI(3)=sin(RILL(3))
EO(1)=cos(ROBS(2))*cos(ROBS(3))
EO(2)=sin(ROBS(2))*cos(ROBS(3))
EO(3)=sin(ROBS(3))
do j1=1,3
   EILL(1,j1)=EI(j1)
   EOBS(1,j1)=EO(j1)
end do
       
! Scalar products:

ES(1)=cos(SPIN(2))*cos(SPIN(3))
ES(2)=sin(SPIN(2))*cos(SPIN(3))
ES(3)=sin(SPIN(3))
call SPRO(SS(1),ES,EI)
call SPRO(SS(2),ES,EO)
   
norm=0.0d0
do j1=1,3
   EIO(j1)=EI(j1)+EO(j1)
   norm=norm+EIO(j1)**2
end do
do j1=1,3
   EIO(j1)=EIO(j1)/sqrt(norm)
end do
call SPRO(SS(3),ES,EIO)

call VPRO(EN,EI,EIO)
norm=0.0d0
do j1=1,3
   norm=norm+EN(j1)**2
end do
do j1=1,3
   EN(j1)=EN(j1)/sqrt(norm)
end do
call SPRO(SS(4),ES,EN)
SS(4)=SS(4)/sqrt(1.0d0-SS(3)**2)

! Principal axes reference frame (2):

rphs=SPIN(4)+2.0d0*pi*((tdt-tdt0)*sd/SPIN(1)-int((tdt-tdt0)*sd/SPIN(1)))
CEU(3)=cos(rphs)
SEU(3)=sin(rphs)
call VROTEU(EI,CEU,SEU)
call VROTEU(EO,CEU,SEU)
do j1=1,3
   EILL(2,j1)=EI(j1)
   EOBS(2,j1)=EO(j1)
end do

! Partial derivatives wrt rotation period:

do j1=1,3
   EI1(j1)=EILL(1,j1)
   EO1(j1)=EOBS(1,j1)
end do

call VROTZ(EI1,CEU(1),SEU(1))
call VROTY(EI1,CEU(2),SEU(2))
call VROTZD(EI1,CEU(3),SEU(3))

call VROTZ(EO1,CEU(1),SEU(1))
call VROTY(EO1,CEU(2),SEU(2))
call VROTZD(EO1,CEU(3),SEU(3))

drphs=-2.0d0*pi*(tdt-tdt0)*sd/SPIN(1)**2
do j1=1,3
   DEI(j1,1)=drphs*EI1(j1) 
   DEO(j1,1)=drphs*EO1(j1)
end do

! Partial derivatives wrt pole longitude:

do j1=1,3
          EI1(j1)=EILL(1,j1)
   EO1(j1)=EOBS(1,j1)
end do

call VROTZD(EI1,CEU(1),SEU(1))
call VROTY(EI1,CEU(2),SEU(2))
call VROTZ(EI1,CEU(3),SEU(3))

call VROTZD(EO1,CEU(1),SEU(1))
call VROTY(EO1,CEU(2),SEU(2))
call VROTZ(EO1,CEU(3),SEU(3))

do j1=1,3
   DEI(j1,2)=EI1(j1) 
   DEO(j1,2)=EO1(j1)
end do

! Partial derivatives wrt pole latitude:

do j1=1,3
   EI1(j1)=EILL(1,j1)
   EO1(j1)=EOBS(1,j1)
end do

call VROTZ(EI1,CEU(1),SEU(1))
call VROTYD(EI1,CEU(2),SEU(2))
call VROTZ(EI1,CEU(3),SEU(3))

call VROTZ(EO1,CEU(1),SEU(1))
call VROTYD(EO1,CEU(2),SEU(2))
call VROTZ(EO1,CEU(3),SEU(3))

do j1=1,3
   DEI(j1,3)=-EI1(j1) 
          DEO(j1,3)=-EO1(j1)
end do

! Partial derivatives wrt pole latitude:

do j1=1,3
        EI1(j1)=EILL(1,j1)
        EO1(j1)=EOBS(1,j1)
end do

call VROTZ(EI1,CEU(1),SEU(1))
call VROTY(EI1,CEU(2),SEU(2))
call VROTZD(EI1,CEU(3),SEU(3))

call VROTZ(EO1,CEU(1),SEU(1))
call VROTY(EO1,CEU(2),SEU(2))
call VROTZD(EO1,CEU(3),SEU(3))

do j1=1,3
   DEI(j1,4)=EI1(j1) 
   DEO(j1,4)=EO1(j1)
end do
end

end module
