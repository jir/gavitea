
module flush_progress
public::fsync
contains
subroutine flush_()
 ! Declare the interface for POSIX fsync function
       interface
         function fsync (fd) bind(c,name="fsync")
         use iso_c_binding, only: c_int
           integer(c_int), value :: fd
           integer(c_int) :: fsync
         end function fsync
       end interface
     
       ! Variable declaration
       integer :: ret
       real :: start, finish
     
       write(10,"(A)",advance="no") "i"
       call cpu_time(start)
       ! Flush and sync
       flush(10)
       ret = fsync(fnum(10))
       call cpu_time(finish)

       ! Handle possible error
       if (ret /= 0) stop "Error calling FSYNC"
end subroutine
end module