module jacobi_pca

implicit none
integer,parameter,private::tark=selected_real_kind(12,30)

contains

SUBROUTINE jacobi(a,n,np,d,v,nrot)
! Computes all eigenvalues and eigenvectors of a real symmetric matrix a,
! which is of size n by n, stored in a physical np by np array.  
! On output, elements of a above the diagonal are destroyed.
! d returns the eigenvalues of a in its first n elements.
! v is a matrix with the same logical  and  physical  dimensions  as a,
! whose  columns  contain,  on  output,  the  normalized eigenvectors of a.
! nrot returns the number  of Jacobi rotations that were required.

INTEGER,parameter::NMAX=500
integer n,np,nrot,i,ip,iq,j
real(kind=tark),dimension(np,np)::a,v
real(kind=tark),dimension(NMAX)::b,z
REAL(kind=tark) d(np),c,g,h,s,sm,t,tau,theta,tresh
do ip=1,n
! Initialize  to  the  identity  matrix.
	do iq=1,n
		v(ip,iq)=0.
	end do
	v(ip,ip)=1.
end do
do ip=1,n
	b(ip)=a(ip,ip)
! Initialize b and d to the diagonal of a.
	d(ip)=b(ip)
	z(ip)=0.
! This  vector  will  accumulate  terms  of  the  form ta(pq) 
! as  in equation  (11.1.14 NumRec).
end do
nrot=0
do i=1,50
	sm=0.
	do ip=1,n-1
! Sum  off-diagonal elements.
		do iq=ip+1,n
			sm=sm+abs(a(ip,iq))
		end do
	end do
	if(sm.eq.0.) return
! The normal return, which relies on quadratic convergence to machine  underflow.
	if(i.lt.4) then
		tresh=0.2*sm/n**2
! ...on the first  three sweeps.
	else
		tresh=0.
! ...thereafter.
	endif
	do ip=1,n-1
		do iq=ip+1,n
			g=100.*abs(a(ip,iq))
! After four sweeps, skip the rotation if the off-diagonal element is small.
			if((i.gt.4).and.(abs(d(ip))+g.eq.abs(d(ip)))&
				.and.(abs(d(iq))+g.eq.abs(d(iq)))) then
				a(ip,iq)=0.
			else if(abs(a(ip,iq)).gt.tresh) then
				h=d(iq)-d(ip)
				if(abs(h)+g.eq.abs(h)) then
					t=a(ip,iq)/h
				else
					theta=0.5*h/a(ip,iq)
					t=1./(abs(theta)+sqrt(1.+theta**2))
					if(theta.lt.0.) t=-t
				endif
				c=1./sqrt(1+t**2)
				s=t*c
				tau=s/(1.+c)
				h=t*a(ip,iq)
				z(ip)=z(ip)-h
				z(iq)=z(iq)+h
				d(ip)=d(ip)-h
				d(iq)=d(iq)+h
				a(ip,iq)=0.
				do j=1,ip-1
! Case of rotations j<p
					g=a(j,ip)
					h=a(j,iq)
					a(j,ip)=g-s*(h+g*tau)
					a(j,iq)=h+s*(g-h*tau)
				end do
				do j=ip+1,iq-1
! Case of rotations p<j<q
					g=a(ip,j)
					h=a(j,iq)
					a(ip,j)=g-s*(h+g*tau)
					a(j,iq)=h+s*(g-h*tau)
				end do
				do j=iq+1,n
! Case of rotations q<j
					g=a(ip,j)
					h=a(iq,j)
					a(ip,j)=g-s*(h+g*tau)
					a(iq,j)=h+s*(g-h*tau)
				end do
				do j=1,n
					g=v(j,ip)
					h=v(j,iq)
					v(j,ip)=g-s*(h+g*tau)
					v(j,iq)=h+s*(g-h*tau)
				end do
				nrot=nrot+1
			endif
		end do
	end do
	do ip=1,n
		b(ip)=b(ip)+z(ip)
		d(ip)=b(ip)
! Update d with the  sum of ta(pq),and  reinitialize z.
		z(ip)=0.
	end do
end do
print*,'too many iterations in jacobi'
return
end

end module