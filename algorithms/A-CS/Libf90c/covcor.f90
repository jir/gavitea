module cov_cor

implicit none
integer,parameter,private::tark=selected_real_kind(12,30)

contains

! Covariance and correlation matrix of a n x m input matrix and
subroutine covcor(a,n,m,cor,cov)
integer::i,j,k,n,m
real(kind=tark),dimension(m)::ma,eval,var
real(kind=tark),dimension(m,m)::evec,cov,cor
real(kind=tark),dimension(n,m)::a,am

! For each variable, subtract the mean of all pixels from the value of each pixel

do i=1,m
	ma(i)=sum(a(:,i))/n
end do
do k=1,n
	do i=1,m
		am(k,i)=a(k,i)-ma(i)
	end do
end do
cov=0
var=0
do i=1,m ! variables
	do k=1,n ! data points
		var(i)=var(i)+am(k,i)**2
	end do
	do j=1,m ! variables
		do k=1,n ! data points
			cov(i,j)=cov(i,j)+am(k,i)*am(k,j)
		end do
	end do
end do
cov=cov/(n-1)
do i=1,m
	do j=1,m
		cor(i,j)=cov(i,j)/sqrt(var(i)*var(j))
	end do
end do
end

end module