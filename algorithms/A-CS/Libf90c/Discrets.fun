test_suite Discrets

! DO NOT USE THE WORD T-E-S-T anywhere in the comments!

! The only input parameter is ntri, the number of triangle rows in a half hemisphere.
! For the general case, only the first, last and mid values of MU and PHI are checked.
! For the case where ntri=1, MU and PHI for all the 6 vertices are checked.

! CHANGE THE VALUE OF ntr2 ON LINE 19 TO CHECK ADDITIONAL INPUT VALUES.

! Global variables can be declared here
integer,parameter::tark=selected_real_kind(12,30),muphi=130000,itsize=260000
integer::ntr1,ntr2,nnod1,ntri1,nnod2,ntri2,IT1(itsize,3),IT2(itsize,3)
real(kind=tark)::MU1(muphi),PHI1(muphi),MU2(muphi),PHI2(muphi)

! Place code here that should be run before each test
setup
   real::cpui,cpuf
   ntr1=1 ! Do not change this, since value 1 is treated differently than the other values!
   ntr2=100 ! THIS CAN BE ARBITRARY !
! Run the code separately for ntr1 and ntr2
   call cpu_time(cpui)
   call TRIDS(MU1,PHI1,IT1,nnod1,ntri1,ntr1)
   call cpu_time(cpuf)
   print*,'CPU time for ntri1:',cpuf-cpui
   call cpu_time(cpui)
   call TRIDS(MU2,PHI2,IT2,nnod2,ntri2,ntr2)
   call cpu_time(cpuf)
   print*,'CPU time for ntri2:',cpuf-cpui
end setup
  
! Place code here that should be run after each test
teardown
end teardown

test TRIDS_output
  integer::ntri0,nnod0,i
  real(kind=tark),dimension(6),parameter::MU01_deg=(/0.,90.,90.,90.,90.,180./),&
                                 PHI01_deg=(/0.,0.,90.,180.,270.,0./)
  real(kind=tark),dimension(3),parameter::MU02_deg=(/0.,90.,180./),&
                                 PHI02_deg=(/0.,180.,0./)
  real(kind=tark),dimension(6)::MU11,MU10,PHI11,PHI10
  real(kind=tark),dimension(3)::MU21,MU20,PHI21,PHI20
  real(kind=tark)::rd

  rd=acos(-1.)/180.

! Case ntr1
  ntri0=8
  nnod0=6
  MU11=MU1(1:nnod1)
  PHI11=PHI1(1:nnod1)
  MU10=cos(MU01_deg*rd)
  PHI10=PHI01_deg*rd
  assert_real_equal(ntri1,ntri0)
  assert_real_equal(nnod1,nnod0)
  do i=1,nnod1
   assert_equal_within(MU10(i),MU11(i), 1e-6)
   assert_equal_within(PHI10(i),PHI11(i), 1e-6)
  end do

! Case ntr2
  ntri0=8*ntr2**2
  nnod0=4*ntr2**2+2
  MU21(1)=MU2(1)
  MU21(2)=MU2(nnod2/2+1)
  MU21(3)=MU2(nnod2)
  PHI21(1)=PHI2(1)
  PHI21(2)=PHI2(nnod2/2+1)
  PHI21(3)=PHI2(nnod2)
  MU20=cos(MU02_deg*rd)
  PHI20=PHI02_deg*rd
  assert_real_equal(ntri2,ntri0)
  assert_real_equal(nnod2,nnod0)
  do i=1,3
   assert_equal_within(MU20(i),MU21(i), 1e-6)
   assert_equal_within(PHI20(i),PHI21(i), 1e-6)
  end do
end test

end test_suite