module Gsg

use Specfunc
implicit none
integer,parameter,private::tark=selected_real_kind(12,30)
real(kind=tark),public::RGSN,SGSN

contains

subroutine RGSNSD(X,MU,PHI,ACF,BCF,rmax,beta,nthe,nphi,lmin,lmax)

! Discrete spherical-coordinate representation for a sample G-sphere.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version 2016-05-13.

integer,intent(in)::nthe,nphi,lmin,lmax
real(kind=tark),intent(in)::MU(0:180),PHI(0:360),ACF(0:256,0:256),&
       BCF(0:256,0:256),beta
integer::j1,j2
real(kind=tark)::r,nu
real(kind=tark),intent(out)::X(0:180,0:360,3),rmax

rmax=0.0d0
do j1=0,nthe
    nu=sqrt(1.0d0-MU(j1)**2)
    do j2=0,nphi
         r=RGSN(ACF,BCF,MU(j1),PHI(j2),beta,lmin,lmax)
         X(j1,j2,1)=r*nu*cos(PHI(j2))
         X(j1,j2,2)=r*nu*sin(PHI(j2))
         X(j1,j2,3)=r*MU(j1)
         if (r.gt.rmax) rmax=r
   end do
end do
end


function RGSN(ACF,BCF,mu,phi,beta,lmin,lmax)

! Radial distance in a given direction for a sample G-sphere.
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version 2016-05-13

integer,intent(in)::lmin,lmax
real(kind=tark),intent(in)::ACF(0:256,0:256),BCF(0:256,0:256),mu,phi,beta 

RGSN=exp(SGSN(ACF,BCF,mu,phi,lmin,lmax)-0.5d0*beta**2)
end



function SGSN(ACF,BCF,mu,phi,lmin,lmax)

! Logarithmic radial distance in a given direction for a sample G-sphere.
! Author: Johanna Torppa (based on copy by Karri Muinonen)
! Version 2016-05-13

integer,intent(in)::lmin,lmax
real(kind=tark),dimension(0:256,0:256),intent(in)::ACF,BCF
real(kind=tark),intent(in)::mu,phi
integer::l,m
real(kind=tark),dimension(0:256,0:256)::LEGP
real(kind=tark)::CPHI(256),SPHI(256)
       
if (lmax.eq.0) then
        SGSN=ACF(0,0)/sqrt(2.0d0)
        return
endif

! Precomputation of sines, cosines, and associated Legendre functions:

call LEGAN(LEGP,mu,lmax,0)
do m=1,lmax
        call LEGAN(LEGP,mu,lmax,m)
        CPHI(m)=cos(m*phi)
        SPHI(m)=sin(m*phi)
end do
LEGP(0,0)=1.0d0/sqrt(2.0d0)

! Sum up:

SGSN=0.0d0
do l=lmin,lmax
        SGSN=SGSN+LEGP(l,0)*ACF(l,0)
end do
do m=1,lmax
    do l=max(m,lmin),lmax
         SGSN=SGSN+LEGP(l,m)*(ACF(l,m)*CPHI(m)+BCF(l,m)*SPHI(m))
   end do
end do
end

end module
