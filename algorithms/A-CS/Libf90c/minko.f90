module minko

! Uses units 7-9

use vectprocs
use chn2
use jacobi_pca
use cov_cor
implicit none
integer,parameter,private::tark=selected_real_kind(12,30),&
		   nmax=6000,&
		   nedmax=500,&
		   maxpar=2000,&
		   ntr_m=3
real(kind=tark),parameter,private::tiny=1e-8,&
			   epsilon=.001 
real(kind=tark),dimension(0:nmax),private::x,y,z
integer,private::numfaces
real(kind=tark),public::gdot
common/points/x,y,z/nf/numfaces


contains

subroutine Minkowski(initarea,nor,imcmc,outdir)

integer::i,j,k,ncoef,posroute,numedges(nmax),r(nmax,nedmax),numvertices,maxind,first,iter,nwrite,&
	 imcmc,edge(nmax,nedmax),vert,n,l,temp,numvert(nmax),nrot,errc,niter
real(kind=tark),dimension(nmax)::initarea,d,pxk,area,xk,prearea,ek,newx,w
real(kind=tark),dimension(3)::anorm,bnorm,p1,p2,p0,cross,normal,rv,sv,addvect,ra,rb,cmass,avect,bvect,&
			      av,bv,cv,norm,eval
real(kind=tark)::dhelp,initm,m,prevolume,eta,initareasum,initarealen,&
		 lower,rx(3,nmax),sqlength,maxpxk,h(1200),max,volume,scoef,retemp,premaxpxk,&
		 arealen,alphak,tk,coeff,f,wmin,nor(nmax,3),verM(nmax,3),evec(3,3),cor(3,3),cov(3,3)
character(len=5)::ifile
character(len=200)::outdir

nwrite=0
iter=0
errc=0
! An auxiliary value initm (related to the maximum value of dot(a(i),b(j)),
! discarding values above 0.9999, corresponding to the normals of the i'th
! and j'th facet), is computed.

initm=0.
do i=1,numfaces
	do k=1,3
        anorm(k)=nor(i,k)
    end do
    do j=1,numfaces
        do k=1,3
        	bnorm(k)=nor(j,k)
        end do
        dhelp=1-(dot(anorm,bnorm))**2
        if (dhelp.gt.0.0001) then
           	m=1./sqrt(dhelp)
           	if (m.gt.initm) initm=m
        end if
    end do
end do
ncoef=0
posroute=1
prevolume=0.
eta=1.3
! The vector of the distances of the faces from the origin is initialized.
initareasum=0
do i=1,numfaces
	initareasum=initareasum+initarea(i)
end do
initarealen=sqrt(gdot(initarea,initarea))
do i=1,numfaces
 	d(i)=initarealen**2/initareasum
end do
! The origin is x(0),y(0),z(0)
x(0)=0.
y(0)=0.
z(0)=0.

! The polytope in R^3 is transformed into a polytope in dual space, i.e., 
! the faces are transformed into points x(i),y(i),z(i).
niter=0
10 niter=niter+1
do i=1,numfaces
	x(i)=nor(i,1)/d(i)
 	y(i)=nor(i,2)/d(i)
 	z(i)=nor(i,3)/d(i)
end do
call convhull(numedges,edge,nmax,nedmax,errc)
if(errc/=0) then 
!	print*,"Problem in Hull"
	goto 30
endif

! (convhull's arguments include x,y,z as input as well; they are given in 
! the constants_m module)
! The convex hull of the dual polytope is transformed into a polytope in 
! R^3, i.e., the faces surrounding a vertex in the dual space become 
! vertices surrounding a face in R^3.

k=1
! k counts the total number of vertices, vert the vertices for face i
do i=1,numfaces
	vert=0
        do n=1,numedges(i)
          	if(numedges(i).gt.nedmax) then
!			print*,'size of edge exceeded',nedmax
             	stop
          	endif
          	call vector(i,edge(i,n),p1) ! vector from test point i to the nth connected point
          	call vector(i,edge(i,modlo(n+1,numedges(i))),p2) ! vector to the next connected point
          	call vector(0,i,p0) ! vector from the centre to the test point i
          	call crossproduct(p1,p2,cross)
          	lower=dot(p0,cross)
! Coordinates of a vertex
			do l=1,3
           		if(k.gt.nmax) then
!             			print*,'size of rx exceeded',nmax
              			stop
           		endif
           		rx(l,k)=cross(l)/lower ! Transform back to R3 space
          	end do
! Check if this vertex has been computed earlier
          	do j=1,k-1
          		sqlength=0.
           		do l=1,3
					sqlength=sqlength+(rx(l,j)-rx(l,k))**2
           		end do
! The test allows for some rounding errors etc.
           		if (sqlength.lt.tiny) then
            			temp=j
            			if (n.gt.1) then
! Ignore this vertex if met earlier at this face
             				if(vert.gt.nedmax) then
!                				print*,'size of r exceeded',nedmax
                				stop
             				endif
             				if ((temp.eq.r(i,vert)).or.(temp.eq.r(i,1))) goto 70
            			end if
! If met earlier but not at this face, increase vert but not k
            			goto 60
           		end if
! End for j
          	end do
          	temp=k
          	k=k+1
60        	vert=vert+1
! r holds the vertices of facet i
          	if(vert.gt.nedmax) then
!            		print*,'size of r exceeded',nedmax
            		stop
          	endif
          	r(i,vert)=temp ! indices of vertices surrounding vertex i
! End for n
70      end do
	if(i.gt.nmax) then
!            	print*,'size of numvert exceeded'
            	stop
        endif
        numvert(i)=vert
! End for i
end do
numvertices=k-1
! Now the points of the dual space can be erased, and x,y,z will represent
! coordinates in R^3.
do i=1,numvertices
	x(i)=rx(1,i)
    y(i)=rx(2,i)
    z(i)=rx(3,i)
end do
maxpxk=0.
do i=1,numfaces
    h(i)=0.
! Compute 'vertices' and auxiliary values h for faces that have no vertices
    if (numvert(i).eq.0) then
      	do j=1,3
       		normal(j)=nor(i,j)
      	end do
      	max=0.
      	do l=1,numvertices
       		call vector(0,l,rv)
 ! find the vertex that defines the plane i to be farthest away from the origin
			if (dot(rv,normal).gt.max) then
            	max=dot(rv,normal)
            	maxind=l
           	end if
        end do
        numvert(i)=1
        r(i,1)=maxind
        do l=1,numvertices
            call vector(0,l,rv)
            if ((dot(rv,normal).eq.max).and.(l.ne.maxind)) then
            	numvert(i)=2
            	r(i,2)=l
           	end if
        end do
        h(i)=d(i)-max
    end if
! The circuits of the edges of each face are computed and the largest 
! circuit is seeked.
    pxk(i)=0
    do j=1,numvert(i)
        call vector(r(i,j),r(i,modlo((j+1),numvert(i))),sv)
        pxk(i)=pxk(i)+sqrt(dot(sv,sv))
    end do
    if (maxpxk.lt.pxk(i)) maxpxk=pxk(i)
end do
! Some auxiliary values are computed
volume=0.
do i=1,numfaces
    do j=1,3
      	addvect(j)=0.
    end do
    do n=1,numvert(i)
        call vector(0,r(i,n),ra)
        call vector(0,r(i,modlo(n+1,numvert(i))),rb)
        call crossproduct(ra,rb,cross)
        do j=1,3
           	addvect(j)=addvect(j)+cross(j)
        end do
    end do
    area(i)=sqrt(dot(addvect,addvect))/2.
    volume=volume+d(i)*area(i)/3.
end do
iter=iter+1
if (volume.ge.prevolume) then
        do j=1,3
          	cmass(j)=0.
        end do
        do i=1,numfaces
          	do n=1,numvert(i)-2
           		call vector(r(i,1),r(i,n+1),avect)
           		call vector(r(i,1),r(i,n+2),bvect)
           		call crossproduct(avect,bvect,cross)
           		call vector(0,r(i,1),av)
           		call vector(0,r(i,n+1),bv)
           		call vector(0,r(i,n+2),cv)
           		do j=1,3
            			cmass(j)=cmass(j)+d(i)*sqrt(dot(cross,cross))*(av(j)+bv(j)+cv(j))
           		end do
          	end do
        end do
        do j=1,3
          	cmass(j)=cmass(j)/(24*volume)
        end do
        do i=1,numfaces
          	do j=1,3
           		norm(j)=nor(i,j)
          	end do
          	xk(i)=d(i)-dot(norm,cmass)-h(i) ! correct the distance d for cmass
        end do
        scoef=initarealen**2/gdot(xk,initarea)
        do i=1,numfaces
          	xk(i)=scoef*xk(i) ! scale distances
          	area(i)=scoef**2*area(i) ! scale areas
        end do
        maxpxk=scoef*maxpxk ! scale max circuit
        volume=scoef**3*volume ! scale volume
        if (prevolume.ne.0) then
          	retemp=eta**ncoef*(arealen*sin(alphak))**2/(6*initm*premaxpxk)
        else
          	retemp=0.
        end if
        if (((volume-prevolume).ge.retemp).and.(posroute.eq.1)) then
          	ncoef=ncoef+1
        else
          	ncoef=ncoef-1
        end if
! else, i.e., if volume.lt.prevolume
else
        ncoef=ncoef-1
! No changes are made to xk:s.
        volume=prevolume
        maxpxk=premaxpxk
        do i=1,numfaces
          	area(i)=prearea(i)
        end do
end if
! If volume was larger than prevol, but improvement was negligible, goto 100
if(volume-prevolume.lt.1.0d-12.and.volume-prevolume.gt.1.0d-15) then
!    print*,"Stop iteration at vol-prevol [10^-15,10^-12]"
    goto 100
endif
prevolume=volume
premaxpxk=maxpxk
do i=1,numfaces
        prearea(i)=area(i)
end do

! The iteration step and the angle alphak are computed.
arealen=sqrt(gdot(area,area))
alphak=acos(gdot(area,initarea)/(arealen*initarealen))

! Test whether to stop iterating
if (alphak.lt.epsilon) then
!    print*,"Stop iteration at alphak < epsilon"
	goto 100    
elseif (niter>1000) then
!    print*,"Stop iteration at:"
!	print*,"vol-prevol > 10^-12 or < 10^-15",volume-prevolume
!	print*,"alphak>epsilon",alphak,epsilon
	goto 100
endif
! Writes the temporary results into the temporary file 'minkotmp'
if(nwrite*20+1.eq.iter) then
         nwrite=nwrite+1         
         open(7,file="minkotmp")
         write(7,*) numvertices, numfaces
         scoef=scoef*sqrt(initarealen/arealen)
         do i=1,numvertices
           	x(i)=x(i)-cmass(1)
           	y(i)=y(i)-cmass(2)
           	z(i)=z(i)-cmass(3)
           	write(7,110) scoef*x(i),scoef*y(i),scoef*z(i)
         end do
         do i=1,numfaces
            	write(7,*) numvert(i)
            	write(7,*) (r(i,j),j=1,numvert(i))
         end do
         close(7)
endif
tk=eta**ncoef*arealen*sin(alphak)/(3*initm*maxpxk)
if(tk.lt.1.d-10) then
!    print*,"tk<10^-10"
	goto 100
endif
coeff=gdot(area,initarea)/(initarealen**2)
do i=1,numfaces
        f=area(i)-coeff*initarea(i)
        ek(i)=f/(arealen*sin(alphak))
end do
do i=1,numfaces
        newx(i)=xk(i)+tk*ek(i)
        if (newx(i).le.0) goto 90
end do
do i=1,numfaces
        d(i)=newx(i)
end do
posroute=1
goto 10
90 posroute=-1
first=0
do i=1,numfaces
        w(i)=xk(i)/(coeff*initarea(i)-area(i))
        if ((w(i).gt.0).and.(first.eq.0)) then
          	wmin=w(i)
          	first=1
        end if
        if ((w(i).gt.0).and.(w(i).lt.wmin)) wmin=w(i)
end do
tk=0.9*arealen*wmin*sin(alphak)
do i=1,numfaces
        d(i)=xk(i)+tk*ek(i)
end do
goto 10

! The final vertices. Check if the axis ratios are realistic
100 scoef=scoef*sqrt(initarealen/arealen)
do i=1,numvertices
        x(i)=x(i)-cmass(1)
        y(i)=y(i)-cmass(2)
        z(i)=z(i)-cmass(3)
		verM(i,1)=scoef*x(i)
		verM(i,2)=scoef*y(i)
		verM(i,3)=scoef*z(i)
end do
call covcor(verM(1:numvertices,:),numvertices,3,cor,cov)
call jacobi(cov,3,3,eval,evec,nrot)
write(ifile,'(I5)') imcmc
ifile=adjustl(ifile)
!	open(unit=7,file=trim(outdir)//'/VertFac'//trim(ifile)//'.txt')
	open(unit=8,file=trim(outdir)//'/acs_vertices'//trim(ifile)//'.txt')
	open(unit=9,file=trim(outdir)//'/acs_facets'//trim(ifile)//'.txt')
30 if(eval(2)/eval(1)<0.2.or.eval(3)/eval(2)<0.2.or.errc/=0) then
!	write(7,*) "! Unrealistic shape"
	write(8,*) "! Unrealistic shape"
	write(9,*) "! Unrealistic shape"
!	close(7)
	close(8)
	close(9)
else
!	write(7,*) numvertices,numfaces
	write(9,*) numfaces
	do i=1,numvertices
 !       write(7,110) verM(i,1),verM(i,2),verM(i,3)
        write(8,110) verM(i,1),verM(i,2),verM(i,3)
	end do
	do i=1,numfaces
  !       write(7,*) numvert(i)
   !      write(7,*) (r(i,j),j=1,numvert(i))
         write(9,*) numvert(i)
         write(9,*) (r(i,j),j=1,numvert(i))
	end do
!	close(7)
	close(8)
	close(9)
endif
110 format(3(E23.16,1X))
end




! The dot product in gradient space

function gdot(avect,bvect)
integer::i
real(kind=tark)::avect(nmax),bvect(nmax)

gdot=0.
do i=1,numfaces
        gdot=gdot+avect(i)*bvect(i)
end do
end

end module