module Specfunc
! Special functions:
!
! BESMS:  modified spherical Bessel functions multiplied by exponential
! LEGA:   associated Legendre functions
! FACTRL: factorial function
! GAMMLN: logarithmic Gamma function 

implicit none
integer,parameter,private::tark=selected_real_kind(12,30),ITMAX=100
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0),EPS=3.0d-14
real(kind=tark),public::factrl,gammln

contains

subroutine BESMS(BESISE,x,n)

! Generates modified spherical Bessel functions multiplied by an
! exponential: i_0(x)*exp(-x),...,i_n(x)*exp(-x).
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::n
real(kind=tark),intent(in)::x
integer::j1,nini
real(kind=tark)::b,b0,b1,renorm
real(kind=tark),intent(out)::BESISE(0:256)

! Orders n=0 and n=1:

if (n.le.1) then
        BESISE(0)=exp(-x)*sinh(x)/x
        BESISE(1)=exp(-x)*(-sinh(x)/x**2)+cosh(x)/x
        return
endif

! Downward recurrence:

nini=max(n+4,int(1.5d0*x))
b1=0.0d0
b0=exp(-x)*2.0d0*x
do j1=nini,n,-1
        b=(2*j1+1)*b0/x+b1
        b1=b0
        b0=b
end do
BESISE(n)=b1
BESISE(n-1)=b0
do j1=n,2,-1
        BESISE(j1-2)=(2*j1-1)*BESISE(j1-1)/x+BESISE(j1)
end do

! Renormalization:

renorm=exp(-x)*(sinh(x)/x)/BESISE(0)
do j1=0,n
        BESISE(j1)=renorm*BESISE(j1)
end do
end



subroutine LEGAN(LEGPLM,x,lmax,m)

! The subroutine has been changed to agree with the sign convention
! in G. Arfken, Mathematical Methods for Physicists. It computes a sequence
! of normalized associated Legendre functions with given m.
!
! Something wrong here!!

integer,intent(in)::m,lmax
real(kind=tark),intent(in)::x
integer::i,l
real(kind=tark)::somx2,fact
real(kind=tark),intent(out)::LEGPLM(0:256,0:256)

if(m.lt.0 .or. m.gt.lmax .or. abs(x).gt.1.0d0) return

LEGPLM(m,m)=1.0d0
if(m.gt.0) then
    somx2=sqrt((1.0d0-x)*(1.0d0+x))
    fact=1.0d0
    do i=1,m
        LEGPLM(m,m)=LEGPLM(m,m)*fact*somx2
        fact=fact+2.0d0
   end do
endif
if(lmax.eq.m) then
    LEGPLM(lmax,m)=LEGPLM(m,m)
else
    LEGPLM(m+1,m)=x*(2*m+1)*LEGPLM(m,m)
    if(lmax.eq.m+1) then
        LEGPLM(lmax,m)=LEGPLM(m+1,m)
    else
   do l=m+2,lmax
      LEGPLM(l,m)=(x*(2*l-1)*LEGPLM(l-1,m)-(l+m-1)*LEGPLM(l-2,m))/(l-m)
   end do
    endif
endif

if (m.eq.0) then
       do 40 l=0,lmax
        LEGPLM(l,0)=LEGPLM(l,0)*sqrt((2*l+1)/2.0d0)
40     continue
else
    do l=m,lmax
        LEGPLM(l,m)=LEGPLM(l,m)*sqrt((2*l+1)/(2.0d0)*FACTRL(l-m)/FACTRL(l+m))
   end do
endif
END


subroutine LEGA(LEGPLM,x,lmax,m)

! The subroutine has been changed to agree with the sign convention
! in G. Arfken, Mathematical Methods for Physicists. It computes a sequence
! of associated Legendre functions with given m.
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-05-17

integer,intent(in)::m,lmax
real(kind=tark)::x
integer::i,l
real(kind=tark)::somx2,fact
real(kind=tark),intent(out)::LEGPLM(0:256,0:256)

if(m.lt.0 .or. m.gt.lmax .or. abs(x).gt.1.0d0) return

LEGPLM(m,m)=1.0d0
if(m.gt.0) then
    somx2=sqrt((1.0d0-x)*(1.0d0+x))
    fact=1.0d0
    do i=1,m
      LEGPLM(m,m)=LEGPLM(m,m)*fact*somx2
      fact=fact+2.0d0
   end do
endif
if(lmax.eq.m) then
    LEGPLM(lmax,m)=LEGPLM(m,m)
else
    LEGPLM(m+1,m)=x*(2*m+1)*LEGPLM(m,m)
    if(lmax.eq.m+1) then
      LEGPLM(lmax,m)=LEGPLM(m+1,m)
    else
      do l=m+2,lmax
         LEGPLM(l,m)=(x*(2*l-1)*LEGPLM(l-1,m)-(l+m-1)*LEGPLM(l-2,m))/(l-m)
      end do
    endif
endif
END



FUNCTION factrl(n)

integer,intent(in)::n
integer::j
integer,save::ntop=0
real(kind=tark),save::a(33)=(1.0d0)

if (n.lt.0) then
    call exit(90)
else if (n.le.ntop) then
    factrl=a(n+1)
else if (n.le.32) then
    do j=ntop+1,n
        a(j+1)=j*a(j)
    end do
    ntop=n
    factrl=a(n+1)
else
    factrl=exp(gammln(n+1.0d0))
endif
end


FUNCTION gammln(xx)
real(kind=tark),intent(in)::xx
integer::j
real(kind=tark)::ser,tmp,x,y
real(kind=tark),save::cof(6)=(/76.18009172947146d0,-86.50532032941677d0,&
            24.01409824083091d0,-1.231739572450155d0,0.1208650973866179d-2,&
            -0.5395239384953d-5/),stp=2.5066282746310005d0

print*,'gammln cof,stp',cof,stp
x=xx
y=x
tmp=x+5.5d0
tmp=(x+0.5d0)*log(tmp)-tmp
ser=1.000000000190015d0
do j=1,6
        y=y+1.d0
        ser=ser+cof(j)/y
end do
gammln=tmp+log(stp*ser/x)
end

end module