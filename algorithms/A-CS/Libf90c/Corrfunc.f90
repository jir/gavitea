module Corrfunc

! Correlation functions and related coefficients:
!
! CS2CF   : modified Gaussian correlation function

use Specfunc
implicit none
integer,parameter,private::tark=selected_real_kind(12,30)

contains

subroutine CS2CF(CSCF,ell,lmin,lmax)

! Returns the Legendre coefficients for the modified Gaussian correlation 
! function. Version 2002-12-16.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version 2016-05-13

integer,intent(in)::lmin,lmax
real(kind=tark),intent(in)::ell
integer::l
real(kind=tark)::BESISE(0:256),z,norm
real(kind=tark),intent(out)::CSCF(0:256)

z=1.0d0/ell**2
call BESMS(BESISE,z,lmax)

do l=0,lmin-1
        CSCF(l)=0.0d0
end do

norm=0.0d0
do l=lmin,lmax
        CSCF(l)=(2*l+1)*BESISE(l)
        norm=norm+CSCF(l)
end do

do l=lmin,lmax
        CSCF(l)=CSCF(l)/norm
end do
end

end module Corrfunc