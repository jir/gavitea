#===================================================================================
# Input: 

# arrays called "ais", "aes" and "acs" telling which algorithms have been run as: 
# [a,b,c] where a tells if the algorithms has been run with 1=run and 0=no run.
# b and c correspond to VOTable and csv output respectively with 1=output and 0=no output.
ais=[1,1,1]
aes=[1,1,1]
acs=[1,1,1]

# Job ID "job"
job=""

#path="/data/output/"+job # path to job output
path_es_out="../A-ES/Output/" # path to aes output
path_cs_out="../A-CS/Output/" # path to acs output
path_cs_in="../A-CS/Input/" # path to acs input
 
#==================================================================================


# File name array generation for A-IS
if ais[0]==1 : # if ais algorithm was run
	base=["ais_inputParameters","inputData","aisFitAll","ais_PeriodScan","ais_PoleScan"]	
	filelist_txt=[]
	for i in range(0,len(base)):
		filelist_txt.append(base[i]+".txt")
	if ais[1]==1: # if VOTable output was selected
		filelist_xml=[]
		for i in range(0,len(base)):
			filelist_xml.append(base[i]+".xml")
	if ais[2]==1: # If csv output was selected
		filelist_csv=[]
		for i in range(0,len(base)):
			filelist_csv.append(base[i]+".csv")
filelist_ais=filelist_txt+filelist_xml+filelist_csv

# File name array generation for A-ES
if aes[0]==1 : # if ais algorithm was run
	ifilefile=open(path_es_out+"aes_outf_name.txt",'r')
	ifile=ifilefile.readline()
	filelen=len(ifile)
	filebase=ifile[0:filelen-5]
	base=["aes_inputParameters","inputData",filebase,filebase+"_w"]	
	filelist_txt=[]
	for i in range(0,len(base)):
		filelist_txt.append(base[i]+".txt")
	if aes[1]==1: # if VOTable output was selected
		filelist_xml=[]
		for i in range(0,len(base)):
			filelist_xml.append(base[i]+".xml")
	if aes[2]==1: # If csv output was selected
		filelist_csv=[]
		for i in range(0,len(base)):
			filelist_csv.append(base[i]+".csv")
filelist_aes=filelist_txt+filelist_xml+filelist_csv

# File name array generation for A-CS
if acs[0]==1 : # if ais algorithm was run
# Read which method in A-CS was used for output
	ifilefile=open(path_cs_out+"acs_outf_name.txt",'r')
	ifile=ifilefile.readline()
	filelen=len(ifile)
	filebase=ifile[0:filelen-5]
# Read how many shape solutions are output
	inpar=open(path_cs_in+"acs_inputParameters.txt")
	if ifile[5:8]=="lsv" :
		pind=1
	else:
		pind=2
	for i in range(0,pind):
		param=inpar.readline()
		print(param)
	nmcmc=int(param[0:10])
	verfiles=nmcmc*[""]
	facfiles=nmcmc*[""]
	for i in range(0,nmcmc):
		verfiles[i]="acs_vertices"+str(i+1)+".txt"
		facfiles[i]="acs_facets"+str(i+1)+".txt"
	base=["acs_inputParameters","inputData",filebase,filebase+"_w",verfiles,facfiles]
	print(base)
	filelist_txt=[]
	for i in range(0,len(base)):
		filelist_txt.append(base[i]+".txt")
	if acs[1]==1: # if VOTable output was selected
		filelist_xml=[]
		for i in range(0,len(base)):
			filelist_xml.append(base[i]+".xml")
	if acs[2]==1: # If csv output was selected
		filelist_csv=[]
		for i in range(0,len(base)):
			filelist_csv.append(base[i]+".csv")
	
filelist_acs=filelist_txt+filelist_xml+filelist_csv
