program callspin
! N.B. nper defined to be below 200 always

use a_is
implicit none
integer,parameter::tark=selected_real_kind(12,30),maxobs=500
real(kind=tark),parameter::rd=4.*atan(1.)/180.0d0
real(kind=tark)::P0(1:12),tspan,cpui,cpuf,fline(13),dper

real(kind=tark)::LOBS(maxobs),LSIG(maxobs),XOBS(3,maxobs),XSUN(3,maxobs),TDT(maxobs),SB(2),car,&
				amtol,scper,P(1:12)
integer ntr1,ntr2,j0,j1,j2,j3,nobsi,nsim1,nsim2,isim,intr1,intr2,&
		mintri1,maxtri1,step1,mintri2,maxtri2,step2,maxper,nper
character(len=4)::strsim,isntr1,isntr2,setnr
character(len=5),dimension(12)::parn=(/'RPer','PLon','PLat','Phi0','Ella','Ellb','Ellc',&
										'Galb','ScG1','ScG2','Size','Tph0'/)
character(len=25)::outdir,output,dum

! Simulated lightcurves 1-10 are generated using a triaxial ellipsoid, and curves 11-22 using the
! spacecraft/radar derived nonconvex shapes of Eros, Gaspra and Kleopatra.

setnr="TestCov"
nsim1=10
nsim2=10
! Parameters that are sampled for testing purposes: number of triangulation rows
! in period (1) and pole scanning (2)
mintri1=2
maxtri1=2
step1=1
mintri2=6
maxtri2=6
step2=1
! The following basic parameters are provided by the user in the UI:
! Parameters that are not sampled:
maxper=500
! Advanced input parameters from UI
amtol=1.0d-3 ! simplex tolerance
scper=0.5 ! coefficient in computing the period scanning step: the smaller the more periods are sampled
car=0.3 ! Minimum allowed ellipsoid axis ratio [0,1]. Assume, that axis P(5)>P(6)>P(7).

! Define the id for and open the test output files
outdir='Output/Set'//trim(adjustl(setnr))
print*,outdir
outdir=adjustl(outdir)
open(unit=2,file=trim(outdir)//'_test.out',position="append")
open(unit=4,file=trim(outdir)//'_testTable.out')
write(4,*) "intr1,intr2,nobsi,amtol,scper,nper,cpuf-cpui"

! Loop over the simulated lightcurves
do isim=nsim1,nsim2
	write(strsim,'(I4)') isim
	strsim=adjustl(strsim)

! Input observations, and the illumination and observation geometry. Write file inputData.txt:
	open(unit=1,file='../TestSet/lc'//trim(strsim)//'.out')
	read (1,*) nobsi
	do j0=1,nobsi
		read(1,*) TDT(j0),LOBS(j0),XSUN(1:3,j0),XOBS(1:3,j0)
		LSIG(j0)=0.001
		TDT(j0)=TDT(j0)-2400000.5d0 ! Just to make the number smaller
	end do
	tspan=maxval(TDT(1:nobsi))-minval(TDT(1:nobsi))
	if (isim<=10) then ! ellipsoid simulations
	   read(1,*) dum
	   read(1,*) P0(1:12)
	   P0(1)=P0(1)/3600.
	   P0(2)=P0(2)/rd
       P0(3)=P0(3)/rd
	else ! nonconvex shape simulations
	   read(1,*) dum
	   read(1,*) dum
	   read(1,5) dum,P0(5:7)
	   read(1,1) dum,P0(8)
	   read(1,2) dum,P0(10)
	   read(1,3) dum,P0(1)
	   read(1,4) dum,P0(3)
	   read(1,4) dum,P0(2)
	endif
	close(1)
1   format(A6,F4.2)
2   format(A8,F6.2)
3   format(A16,F8.6)
4   format(A13,F6.2)
5   format(A17,F1.0,1X,F4.2,1X,F4.2)
! Write input data file for a_is
	open(1,file="inputData.txt")
	write(1,('(I4)')) nobsi
	write(1,('(A50)')) "time,br,br_err,sun_x,sum_y,sum_z,obs_x,obs_y,obs_z"
	do j0=1,nobsi
		write(1,*) TDT(j0),LOBS(j0),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
	end do
	close(1)
	P0(12)=TDT(1) ! in days. For reference; used as the epoch of the zero rotational phase in inversion

! Loop over the parameters that are sampled for testing the effect on the result
	do intr1=mintri1,maxtri1,step1 ! The number of pole triangulation rows for period search
! Define the period range for scanning
	    SB(1)=P0(1)*0.9 ! hours
        SB(2)=P0(1)*1.1 ! hours
	    ntr1=intr1
		write(isntr1,'(I4)') intr1
		isntr1=adjustl(isntr1)
		do intr2=mintri2,maxtri2,step2  ! The number of pole triangulation rows for pole search
			print*,"Simulation",isim,"ntri1:",intr1,"ntri2:",intr2
			ntr2=intr2
			write(isntr2,'(I4)') intr2
			isntr2=adjustl(isntr2)
	
			open(1,file="inputParameters.txt") ! Write input parameter file for a-is
! Write the input parameter file:
! Basic input
			write(1,10) SB(1),"Lower bound for rotation period"
			write(1,10) SB(2),"Upper bound for rotation period"
! Advanced input
			write(1,20) ntr1,"Number of triangle rows for pole in period scanning"
			write(1,20) ntr2,"Number of triangle rows for pole in pole scanning "
			write(1,10) amtol,"Downhill simplex tolerance"
			write(1,10) scper,"Resolution coefficient in period search"
			write(1,20) maxper,"Maximum number of scanned periods"
			write(1,10) car,"Minimum allowed axis ratio"
			close(1)
10 			format(F14.6,5X,A60)
20 			format(I5,5X,A60)

! AVI spin scanning:
			output=trim(outdir)//"_T"//trim(strsim)//"_"//trim(isntr1)//"_"//trim(isntr2)//"_"
			print*,"output base=",output
			call cpu_time(cpui)
			call Initial()
			call cpu_time(cpuf)

! Write output information in case wise files and a summary file

! Write output in a summary file including all the simulations in the simulation set
			open (unit=1,file='Output/a_is_FitAll.out')
			write(2,*) "Simulated dataset: ",isim,"npts",nobsi,"T range (years)=",tspan/360.
			if (isim<11) write(2,*) "Ellipsoid shape model"
			if (isim>=11) write(2,*) "Nonconvex, spacecraft/radar derived shape model"
			write(2,*) "Output/Set"//trim(adjustl(setnr))//"_T"//trim(strsim)//"_"//trim(isntr1)//"_"//trim(isntr2)//"_"
			write(2,*) "CPU:",cpuf-cpui
			write(2,'(8(1X,A6))') "ntr1","ntr2","car","amtol","scper","per1","per2"
			write(2,'(2(I6,1X),F6.2,1X,E8.2,1X,F6.2,2(1X,F6.2))') ntr1,ntr2,car,amtol,scper,SB
			read(1,*) P(1:12)
			print*,"Deviations of the results from the true spin and shape parameters:"
			print*,"Otherwise relative, but degrees for spin state"
			write(2,'(A8,3(1X,A10))') "Par","orig","fitted","dev"
			do j0=1,12
			   if(j0<4.or.(j0>4.and.j0<8)) then
				 if (j0>1.and.j0<5) then
				   write(6,'(A5,1X,I2,3(1X,F10.3),A4)') parn(j0),j0,P0(j0),P(j0),abs(P0(j0)-P(j0))," deg"
				   write(2,'(A5,1X,I2,3(1X,F10.3),A4)') parn(j0),j0,P0(j0),P(j0),abs(P0(j0)-P(j0))," deg"
				 else
				   write(6,'(A5,1X,I2,3(1X,F10.3),A4)') parn(j0),j0,P0(j0),P(j0),100*abs(P0(j0)-P(j0))/P0(j0),"%"
				   write(2,'(A5,1X,I2,3(1X,F10.3),A4)') parn(j0),j0,P0(j0),P(j0),100*abs(P0(j0)-P(j0))/P0(j0),"%"
				 endif
               endif
	    	end do
			write(2,*) "------------------------------------------------------------------------"
			close(1)
			dper=0.5d0*SB(1)**2/(365*24*(TDT(nobsi)-TDT(1)))
			dper=scper*dper
			nper=int((SB(2)-SB(1))/dper)+1
			if(nper>maxper) nper=maxper
			write(4,30) intr1,intr2,nobsi,amtol,scper,nper,cpuf-cpui
30          format(I3,I3,I4,F8.5,F6.3,I5,F10.5)
! Write simulation-wise output files
			open (unit=1,file='Output/a_is_FitAll.out')
			open (unit=3,file=trim(adjustl(output))//'a_is_FitAll.out')
			read(1,('(13(F14.6,1X),F10.6)')) fline
			write(3,('(13(F14.6,1X),F10.6)')) fline
			close(1)
			close(3)
			open (unit=1,file='Output/a_is_PoleScan.out')
			open (unit=3,file=trim(adjustl(output))//'a_is_PoleScan.out')
			do while(1==1)
			    read(1,('(13(E20.14,1X))'),END=100) fline
			    write(3,('(13(E20.14,1X))')) fline
			end do
100			close(1)
			close(3)
			open (unit=1,file='Output/a_is_PeriodScan.out')
			open (unit=3,file=trim(adjustl(output))//'a_is_PeriodScan.out')
			do while (1==1)
			    read(1,('(13(E20.14,1X))'),END=200) fline
			    write(3,('(13(E20.14,1X))')) fline
			end do
200			close(1)
			close(3)
		end do ! Loop over pole rows ntri2 pole search
	end do ! Loop over pole rows ntri1 period search
end do ! Loop over data sets
close(2)
end
