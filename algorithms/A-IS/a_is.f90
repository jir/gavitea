module a_is

! A-IS solves the lightcurve inversion problem using optimization and systematic
! scanning.
! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2016-02-25

! INPUT files, in the same directory as the code itself
! inputData.txt, inputParameters.txt, bf.dat

! OUTPUT files, written to subdirectory Output/
! ais_PeriodScan.txt, ais_PoleScan.txt, ais_FitAll.txt

use Le
use HG1G2
use Discrets
use flush_progress
implicit none
integer,parameter,private::tark=selected_real_kind(12,30),&
         maxobs=500,maxpar=12,&
         npar=12,muphi=130000,itsize=260000
real(kind=tark),parameter,private::pi=4.0d0*atan(1.0d0),tp=2.*pi,&
                rd=pi/180.0d0,sd=24.0d0*3600.0d0
public::fsync

contains

subroutine Initial()

integer::nobs,PID(maxpar),ntr1,ntr2,ndim,j0,j1,j2
integer::nd1,nd2,nd3,maxper
real(kind=tark),dimension(maxpar)::P,PD
real(kind=tark),dimension(maxobs)::LOBS,LSIG,LSIG0,TMJD
real(kind=tark),dimension(3,maxobs)::RILL,ROBS,XSUN,XOBS
real(kind=tark),dimension(0:360)::PHID1,PHID2,PHID3,AD1,AD2,AD3
real(kind=tark),dimension(361)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark)::SB(2),lrms,LOBSMN,scper,car,amtol,&
       clat,lsigr,dphi1a,dphi1b,dphi2a,dphi2b,dphi3a,dphi3b
character(len=70)::header
character(len=200)::indir,outdir
      
! INPUT DATA, i.e., preprocessed Gaia archive photometric asteroid data

! LOBS()   ! Photometric brightnesses. Not normalized
! LSIG0()   ! Observation error corresponding to the brightnesses, from the
!            Gaia archive
! XSUN()   ! Illumination direction corresponding to the brightnesses
! XOBS()   ! Observer direction corresponding to the brightnesses
! TMJD()   ! Epoch corresponding to the brightnesses, evenly increasing
! nobs      ! Number of the brightnesses

call get_command_argument(1,indir)
call get_command_argument(2,outdir)
open(1,file=trim(indir)//"/inputData.txt")
read(1,*) nobs
read(1,*) header
do j0=1,nobs
   read(1,*) TMJD(j0),LOBS(j0),LSIG0(j0),XSUN(1:3,j0),XOBS(1:3,j0)
end do
close(1)
! Basic PARAMETER INPUT from UI:
! SB(2) (AlgoSpec alias plu, default value=(1,15))
!      Period lower and upper bound (hours, float)

! Advanced PARAMETER INPUT from UI:
! amtol (AlgoSpec alias sxtol, default value=0.00001)
!      Downhill simplex tolerance (dimensionless, float)
! scper (AlgoSpec alias scper, default value=0.5)
!       Resolution coefficient in period search (dimensionless, float)
! ntr1 (AlgoSpec alias ntri1, default value=2)
!       Number of triangle rows per octant for pole discretization in
!      period scanning (dimensionless, integer)
! ntr2 (AlgoSpec alias ntri1, default value=10)
!       Number of triangle rows per octant for pole discretization in
!      pole scanning (dimensionless, integer)
! maxper (AlgoSpec alias maxp, default value=1000)
!      Maximum number of scanned periods
! car (AlgoSpec alias car, default value=0.4)
!      Minimum value for the axis ratio c/a, a>b>c

open(1,file=trim(indir)//"/ais_inputParameters.txt")
read(1,*) SB(1)
read(1,*) SB(2)
read(1,*) ntr1
read(1,*) ntr2
read(1,*) amtol
read(1,*) scper
read(1,*) maxper
read(1,*) car
close(1)
SB(1)=SB(1)*3600.0d0
SB(2)=SB(2)*3600.0d0

! Process input observations, and the illumination and observation geometry:
do j1=1,nobs
   LOBSMN=LOBSMN+LOBS(j1)
end do
LOBSMN=LOBSMN/nobs
do j1=1,nobs
   LOBS(j1)=LOBS(j1)/LOBSMN
   LSIG0(j1)=LSIG0(j1)/LOBSMN
   ROBS(1,j1)=sqrt(XOBS(1,j1)**2+XOBS(2,j1)**2+XOBS(3,j1)**2)
   ROBS(3,j1)=asin(XOBS(3,j1)/ROBS(1,j1))
   clat=cos(ROBS(3,j1))
   ROBS(2,j1)=acos(XOBS(1,j1)/(ROBS(1,j1)*clat))
   if (XOBS(2,j1)/clat.lt.0.0d0) ROBS(2,j1)=2.0d0*pi-ROBS(2,j1)
   RILL(1,j1)=sqrt(XSUN(1,j1)**2+XSUN(2,j1)**2+XSUN(3,j1)**2)
   RILL(3,j1)=asin(XSUN(3,j1)/RILL(1,j1))
   clat=cos(RILL(3,j1))
   RILL(2,j1)=acos(XSUN(1,j1)/(RILL(1,j1)*clat))
   if (XSUN(2,j1)/clat .lt.0.0d0) RILL(2,j1)=2.0d0*pi-RILL(2,j1)
end do
! Initialization of the H, G1, G2 phase function:
dphi3b=0.0d0
call PHIINI(PHID1,PHID2,PHID3,dphi1a,dphi1b,dphi2a,dphi2b,dphi3a,AD1,AD2,AD3,nd1,nd2,&
       nd3)
call LSPLINEDI(PHID1,dphi1a,dphi1b,AD1,XL1,YL12,YL2_12,nd1)
call LSPLINEDI(PHID2,dphi2a,dphi2b,AD2,XL2,YL22,YL2_22,nd2)
call LSPLINEDI(PHID3,dphi3a,dphi3b,AD3,XL3,YL32,YL2_32,nd3)

! PERIOD SCANNING

! MODEL PARAMETERS

! P(1)  = rotation period           (s;   given in d in ptd.in) 
! P(2)  = pole longitude            (rad; given in deg in ptd.in)
! P(3)  = pole latitude             (rad; given in deg in ptd.in)
! P(4)  = rotational phase at P(12) (rad; given in deg in ptd.in)
! P(5)  = ellipsoid axis a          (relative units)
! P(6)  = ellipsoid axis b          (relative units)
! P(7)  = ellipsoid axis c          (relative units) 
! P(8)  = geometric albedo
! P(9)  = G1 parameter
! P(10) = G2 parameter
! P(11) = ellipsoid axis a          (km)
! P(12) = epoch of rotational phase P(4) (MJD)

! Parameters to be optimized (1) or fixed/sampled (0):
do j0=1,npar
   PID(j0)=1
end do
PID(1)=0 ! period
PID(5)=0 ! ellipsoid axis a
PID(8)=0 ! geometric albedo
PID(10)=0 ! G2 parameter, for G12 system
PID(12)=0 ! epoch of the rotational phase P(4)
ndim=0
do j0=1,npar
   if (PID(j0).gt.0) ndim=ndim+1
end do

P(1)=SB(1)
P(2)=10.0
P(3)=10.0
P(4)=10.0
! angles are given in degrees, and converted here to radians. Also it is
! checked that the angle is between [0,2*pi]
do j0=2,4
   P(j0)=P(j0)*rd
end do
if (P(2).gt.tp)    P(2)=P(2)-tp*(int((P(2)-tp)/(tp))+1)
if (P(2).lt.0.0d0) P(2)=P(2)+tp*(int((tp-P(2))/(tp)))
if (P(4).gt.tp)    P(4)=P(4)-tp*(int((P(4)-tp)/(tp))+1)
if (P(4).lt.0.0d0) P(4)=P(4)+tp*(int((tp-P(4))/(tp)))
P(5)=1.0
P(6)=0.7
P(7)=0.5
P(8)=0.05
P(9)=0.8
P(10)=0.05
P(11)=60
P(12)=TMJD(1)

! Initial parameter deviations for the preconditioning of the
! simplex least-squares iteration:
! TMJD is given in days. sd converts to seconds
PD(1)= 0.5d-2*P(1)**2/(sd*(TMJD(nobs)-TMJD(1)))
! rd converts degrees to radians
PD(2)=1.0d-1*rd
PD(3)=1.0d-1*rd
PD(4)=1.0d-1*rd
PD(5)=1.0d-3
PD(6)=1.0d-3
PD(7)=1.0d-3
PD(8)=1.0d-4
PD(9)=1.0d-4
PD(10)=1.0d-4
PD(11)=1.0d-3

LSIG=LSIG0
call PERIOD(P,PD,LOBS,LSIG,RILL,ROBS,TMJD,SB,XL1,XL2,XL3,YL12,YL2_12,YL22,&
       YL2_22,YL32,YL2_32,dphi1a,dphi2a,scper,car,amtol,nobs,PID,&
            ndim,ntr1,nd1,nd2,nd3,maxper,outdir)

!print*,'PERIOD SCANNING COMPLETED'
!print*,'-------------------------'
! POLE SCANNING

!print*,'Start pole scanning'

! Parameters to be optimized (1) or sampled/fixed (0):
do j0=1,npar
   PID(j0)=1
end do
PID(2)=0 ! Pole lon
PID(3)=0 ! Pole lat
PID(5)=0 ! Axis a
PID(8)=0 ! Geom albedo
PID(10)=0 ! G2 parameter, not used
PID(12)=0 ! Epoch of 0 rot phase 
ndim=0
do j0=1,npar
   if (PID(j0).gt.0) ndim=ndim+1
end do
 
LSIG=LSIG0
call POLE(P,PD,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
          YL32,YL2_32,dphi1a,dphi2a,car,amtol,nobs,PID,ndim,ntr2,nd1,nd2,nd3,outdir)

!print*,'POLE SCANNING COMPLETED'
!print*,'-------------------------'

! BEST_FIT SOLUTION

!print*,'Start the computation of the best-fit solution'
    
! Parameters to be optimized (1) or sampled (0):
do j0=1,npar
   PID(j0)=1
end do
PID(5)=0
PID(8)=0
PID(10)=0
PID(12)=0
ndim=0
do j0=1,npar
   if (PID(j0).gt.0) ndim=ndim+1
end do

LSIG=LSIG0
call LS(P,PD,LOBS,LSIG,lrms,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
        YL32,YL2_32,dphi1a,dphi2a,car,amtol,nobs,PID,ndim,nd1,nd2,nd3,outdir)

!print*,'BEST_FIT SOLUTION COMPUTED'
!print*,'-------------------------'

end



subroutine PERIOD(P0,PD0,LOBS,LSIG,RILL,ROBS,TMJD,SB,XL1,XL2,XL3,YL12,YL2_12,&
        YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,scper,car,amtol,&
        nobs,PID,ndim,ntr,nd1,nd2,nd3,maxper,outdir)

! Scanning for rotation period using triaxial-ellipsoid optimization.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13

integer,intent(in)::nobs,PID(maxpar),maxper,nd1,nd2,nd3,ntr
real(kind=tark),intent(in)::RILL(3,maxobs),ROBS(3,maxobs),car,amtol,dphi1a,dphi2a,scper
real(kind=tark),dimension(maxobs),intent(in)::LOBS,LSIG,TMJD
real(kind=tark),dimension(361),intent(in)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32

integer::IT(itsize,3),NP(maxpar),ndim,ntri,nnod,npol,jper,jpol,j0,j1,j2,nper
real(kind=tark),dimension(maxpar)::P0,P1,PLS,P,PD0,PD1,PMN,PMNT
real(kind=tark),dimension(maxobs)::LCOM
real(kind=tark)::LOBSMN,lcommn,SB(2),MUN(muphi),PHIN(muphi),&
             dper,chisq,rms,rmsmn,rmsmnt
character(len=200)::outdir

! Resolution for the rotation period and the pole orientation:

dper=0.5d0*P0(1)**2/(sd*(TMJD(nobs)-TMJD(1)))
dper=scper*dper
nper=int((SB(2)-SB(1))/dper)+1
if(nper>maxper) then
   nper=maxper
   dper=(SB(2)-SB(1))/nper
endif
!print*,"Number of period samples=",nper

call TRIDS(MUN,PHIN,IT,npol,ntri,ntr)

! Parameters to be optimized for each trial period:

j0=0
do j1=1,npar
   if (PID(j1).gt.0) then
      j0=j0+1
      NP(j0)=j1
   endif
end do

! Simplex least-squares optimization:
call ELSI(P0,PD1,PD0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,&
     YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,nobs,NP,&
     ndim,nd1,nd2,nd3)
call ELS(PLS,P0,PD1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,&
    YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,amtol,nobs,NP,&
    ndim,nd1,nd2,nd3)

!print*,'Initial fit rms',rms
! Scan for rotation period:

open (unit=12,file=trim(outdir)//'/ais_PeriodScan.txt')
write(12,'(13(A10,11X))') "P","lon","lat","phi_0","a","b","c","alb","g1","g2","size","epoch_0","rms"
rmsmnt=1.0d12
open (10,file="./ais_per_progress.txt")
write(10,*) nper
do jper=0,nper
! Write progress information to file
   call flush_()
   rmsmn=1.0d12
   do jpol=1,npol
      P1(1)=SB(1)+jper*dper
      P1(2)=PHIN(jpol)
      P1(3)=0.5d0*pi-acos(MUN(jpol))
      do j1=4,npar
         P1(j1)=PLS(j1)
      end do
      call ELS(P,P1,PD1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,&
          YL12,YL2_12,YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,&
          rms,car,amtol,nobs,NP,ndim,nd1,nd2,nd3)
! Find parameters corresponding to the best-fit pole for each period.
      if (rms.le.rmsmn) then
         rmsmn=rms
         if (P(2).gt.tp)    P(2)=P(2)-tp*(int((P(2)-tp)/(tp))+1)
         if (P(2).lt.0.0d0) P(2)=P(2)+tp*(int((tp-P(2))/(tp)))
         if (P(4).gt.tp)    P(4)=P(4)-tp*(int((P(4)-tp)/(tp))+1)
         if (P(4).lt.0.0d0) P(4)=P(4)+tp*(int((tp-P(4))/(tp)))
         do j1=1,npar
            PMN(j1)=P(j1)
         end do
      endif
   end do
! Find the parameters corresponding to the best-fit pole and period 
   if (rmsmn.le.rmsmnt) then
      rmsmnt=rmsmn
      do j1=1,npar
         PMNT(j1)=PMN(j1)
      end do
   endif
   write (12,'(13(E20.14,1X))') PMN(1)/3600.0d0,(PMN(j1)/rd,j1=2,4),PMN(5:10),abs(PMN(11)),PMN(12),rmsmn
end do
close(12)
close(10)
! Best-fit parameters 
do j1=1,npar
   P0(j1)=PMNT(j1)
end do
end



subroutine POLE(P0,PD0,LOBS,LSIG,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,&
      YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,amtol,nobs,PID,&
      ndim,ntr,nd1,nd2,nd3,outdir)
 
! Scanning for pole orientation using triaxial-ellipsoid optimization.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13

integer::IT(itsize,3),nobs,PID(maxpar),NP(maxpar),ndim,ntr,ntri,nnod,npol,&
   jpol,j0,j1,j2
integer::nd1,nd2,nd3
real(kind=tark),dimension(maxobs)::LOBS,LSIG,LCOM,TMJD
real(kind=tark),dimension(maxpar)::P0,P1,PLS,P,PMN,PD0,PD1
real(kind=tark),dimension(361)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark)::LOBSMN,lcommn,RILL(3,maxobs),ROBS(3,maxobs),MUN(muphi),PHIN(muphi),&
       car,amtol,chisq,rms,rmsmn,sc,dphi1a,dphi2a
character(len=200)::outdir
 
! Pole orientations:

call TRIDS(MUN,PHIN,IT,npol,ntri,ntr)

! Parameters to be optimized for each trial period:

j0=0
do j1=1,npar
   if (PID(j1).gt.0) then
      j0=j0+1
      NP(j0)=j1
   endif
end do

! Simplex least-squares optimization:

call ELSI(P0,PD1,PD0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,&
     YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,nobs,NP,ndim,nd1,nd2,nd3)
call ELS(PLS,P0,PD1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
    YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,amtol,nobs,NP,ndim,nd1,nd2,nd3)

!print*,'Initial Least Square rms',rms

! Scan for pole orientation:

open (unit=12,file=trim(outdir)//"/ais_PoleScan.txt")
write(12,'(13(A10,11X))') "P","lon","lat","phi_0","a","b","c","alb","g1","g2","size","epoch_0","rms"

open (10,file="./ais_pole_progress.txt")
write(10,*) npol
rmsmn=1.0d12
do jpol=1,npol
! Write progress information to file
   call flush_()
   do j1=1,npar
      P1(j1)=PLS(j1)
   end do
   P1(2)=PHIN(jpol)
   P1(3)=0.5d0*pi-acos(MUN(jpol))

   call ELS(P,P1,PD1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,&
       YL22,YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,amtol,nobs,NP,&
       ndim,nd1,nd2,nd3)

   if (rms.le.rmsmn) then
      rmsmn=rms
      do j1=1,npar
         PMN(j1)=P(j1)
      end do
        endif
   write (12,'(13(E20.14,1X))') P(1)/3600.0d0,(P(j1)/rd,j1=2,4),P(5:10),abs(P(11)),P(12),rms
end do
close(12)
close(10)
do j1=1,npar
   P0(j1)=PMN(j1)
end do
end




subroutine LS(P0,PD0,LOBS,LSIG,lrms,RILL,ROBS,TMJD,XL1,XL2,XL3,YL12,YL2_12,YL22,&
         YL2_22,YL32,YL2_32,dphi1a,dphi2a,car,amtol,nobs,PID,ndim,&
         nd1,nd2,nd3,outdir)
 
! Downhill simplex optimization for the least-squares triaxial-ellipsoid model.

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13
             
integer::nobs,PID(maxpar),NP(maxpar),ndim,j0,j1,j2
integer nd1,nd2,nd3,jiter
real(kind=tark),dimension(maxobs)::LOBS,LSIG,LSIGC,LCOM,TMJD
real(kind=tark),dimension(maxpar)::PLS,P0,PD0,PD1
real(kind=tark),dimension(361)::XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,YL32,YL2_32
real(kind=tark)::RILL(3,maxobs),ROBS(3,maxobs),lrms,car,amtol,lcommn,&
       chisq,rms,sum1,sum2,dphi1a,dphi2a
character(len=10)::parfile
character(len=200)::outdir

! Initialize:
! Change this. Only one lightcurve!!!
lrms=1.0d0 ! Not used!

! Parameters to be optimized:

j0=0
do j1=1,npar
   if (PID(j1).gt.0) then
      j0=j0+1
      NP(j0)=j1
   endif
end do
! Least-squares parameters with input noise:

call ELSI(P0,PD1,PD0,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,&
     YL2_22,YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,nobs,NP,ndim,nd1,nd2,nd3)

call ELS(PLS,P0,PD1,LOBS,LSIG,LCOM,lcommn,TMJD,RILL,ROBS,XL1,XL2,XL3,YL12,YL2_12,YL22,YL2_22,&
    YL32,YL2_32,dphi1a,dphi2a,chisq,rms,car,amtol,nobs,NP,ndim,nd1,nd2,nd3)

!print*,real(PLS(1)/3600.0d0),real(PLS(2)/rd),real(PLS(3)/rd),real(rms)

open (unit=12,file=trim(outdir)//"/ais_FitAll.txt")
write(12,'(13(A7,8X))') "P","lon","lat","phi_0","a","b","c","alb","g1","g2","size","epoch_0","rms"

if (PLS(2).gt.tp)    PLS(2)=PLS(2)-tp*(int((PLS(2)-tp)/(tp))+1)
if (PLS(2).lt.0.0d0) PLS(2)=PLS(2)+tp*(int((tp-PLS(2))/(tp)))
if (PLS(4).gt.tp)    PLS(4)=PLS(4)-tp*(int((PLS(4)-tp)/(tp))+1)
if (PLS(4).lt.0.0d0) PLS(4)=PLS(4)+tp*(int((tp-PLS(4))/(tp)))
write (12,20) PLS(1)/3600.,(PLS(j1)/rd,j1=2,4),PLS(5:10),abs(PLS(11)),PLS(12),rms
20 format(13(F14.6,1X),F10.6)
close(12)

end
end module a_is