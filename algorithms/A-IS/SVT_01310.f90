program callspin


! Compile:
! f95 Libf90s/flush_progress.f90 Libf90s/Discrets.f90 Libf90s/Voper.f90 Libf90s/HG1G2.f90 Libf90s/Viewgeo.f90 Libf90s/Le.f90 a_is.f90 SVT_01310.f90 -o SVT_01310.o

! Test for test case GAVITEA-SVT_01310

use a_is
implicit none
integer,parameter::tark=selected_real_kind(12,30),maxobs=500
real(kind=tark),parameter::rd=4.*atan(1.)/180.0d0
real(kind=tark)::P0(1:12),tspan,cpui,cpuf,fline(13),dper

real(kind=tark)::LOBS(maxobs),LSIG(maxobs),XOBS(3,maxobs),XSUN(3,maxobs),TDT(maxobs),SB(2),car(2),&
				amtol(2),scper(2),P(1:12),pdev(2)
integer j0,j1,j2,j3,nobsi,isim,intr1,intr2,icar,imaxper,iamtol,iscper,nptot,&
		mintri1,maxtri1,mintri2,maxtri2,maxper1,maxper2,idev,nper,pas1,pas2,nfail1,nfail2
character(len=10)::strsim,isntr1,isntr2,setnr,isamtol,isscper,ismaxper,iscar,isdev
character(len=5),dimension(12)::parn=(/'RPer','PLon','PLat','Phi0','Ella','Ellb','Ellc',&
										'Galb','ScG1','ScG2','Size','Tph0'/)
character(len=45)::outdir,output,dum

! Simulated lightcurves 1-10 are generated using a triaxial ellipsoid, and curves 11-22 using the
! spacecraft/radar derived nonconvex shapes of Eros, Gaspra and Kleopatra.

setnr="01310"
isim=1
! Parameters that are sampled for testing purposes:
! Step=2 in do loop.
mintri1=2
maxtri1=4
! Step=6 in do loop.
mintri2=2
maxtri2=8
! Step=200 
maxper1=100
maxper2=400
! Advanced input parameters from UI
amtol(1)=1.0d-3 ! simplex tolerance
amtol(2)=1.0d-5 ! simplex tolerance
scper(1)=0.1 ! coefficient in computing the period scanning step: the smaller the more periods are sampled
!scper(2)=2. ! coefficient in computing the period scanning step: the smaller the more periods are sampled
car(1)=0.3 ! Minimum allowed ellipsoid axis ratio [0,1]. Assume, that axis P(5)>P(6)>P(7).
!car(2)=0.4 ! Minimum allowed ellipsoid axis ratio [0,1]. Assume, that axis P(5)>P(6)>P(7).
! pdev defines here the period range, that is sampled. It is the +- precentage deviation from the 
! correct period
pdev(1)=0.1
pdev(2)=0.3
! Define the id for and open the test output files
outdir='Output/Set'//trim(adjustl(setnr))
outdir=adjustl(outdir)
open(unit=2,file=trim(outdir)//'/'//trim(setnr)//'_test.out')
open(unit=4,file=trim(outdir)//'/'//trim(setnr)//'_testTable.out')
write(4,*) "intr1,intr2,car,amtol,scper,nper,cpuf-cpui"

nfail1=0
nfail2=0
! Loop over the simulated lightcurves
write(strsim,'(I4)') isim
strsim=adjustl(strsim)

! Input observations, and the illumination and observation geometry. Write file inputData.txt:
open(unit=1,file='../TestSet/lc'//trim(strsim)//'.out')
read (1,*) nobsi
do j0=1,nobsi
	read(1,*) TDT(j0),LOBS(j0),XSUN(1:3,j0),XOBS(1:3,j0)
	LSIG(j0)=0.001
	TDT(j0)=TDT(j0)-2400000.5d0 ! Just to make the number smaller
end do
tspan=maxval(TDT(1:nobsi))-minval(TDT(1:nobsi))
if (isim<=10) then ! ellipsoid simulations
   read(1,*) dum
   read(1,*) P0(1:12)
   P0(1)=P0(1)/3600.
   P0(2)=P0(2)/rd
   P0(3)=P0(3)/rd
else ! nonconvex shape simulations
   read(1,*) dum
   read(1,*) dum
   read(1,5) dum,P0(5:7)
   read(1,1) dum,P0(8)
   read(1,2) dum,P0(10)
   read(1,3) dum,P0(1)
   read(1,4) dum,P0(3)
   read(1,4) dum,P0(2)
endif
close(1)
1 format(A6,F4.2)
2 format(A8,F6.2)
3 format(A16,F8.6)
4 format(A13,F6.2)
5 format(A17,F1.0,1X,F4.2,1X,F4.2)
! Write input data file for a_is
open(1,file="Input/inputData.txt")
write(1,('(I4)')) nobsi
write(1,('(A50)')) "time,br,br_err,sun_x,sum_y,sum_z,obs_x,obs_y,obs_z"
do j0=1,nobsi
	write(1,*) TDT(j0),LOBS(j0),LSIG(j0),XSUN(1:3,j0),XOBS(1:3,j0)
end do
close(1)
P0(12)=TDT(1) ! in days. For reference; used as the epoch of the zero rotational phase in inversion

! Loop over the parameters that are sampled for testing the effect on the result
! Define the period range for scanning
nptot=0
do idev=1,2
 write(isdev,'(I4)') idev		
 isdev=adjustl(isdev)
 SB(1)=P0(1)*(1.-pdev(idev)) ! hours
 SB(2)=P0(1)*(1.+pdev(idev)) ! hours
 do iamtol=1,2
  write(isamtol,'(I4)') iamtol		
  isamtol=adjustl(isamtol)
  do iscper=1,1
   write(isscper,'(I4)') iscper		
   isscper=adjustl(isscper)
   do imaxper=maxper1,maxper2,200
    write(ismaxper,'(I4)') imaxper		
    ismaxper=adjustl(ismaxper)
    do icar=1,1
     write(iscar,'(I4)') icar		
     iscar=adjustl(iscar)
	 do intr1=mintri1,maxtri1,2 ! The number of pole triangulation rows for period search
		write(isntr1,'(I4)') intr1
		isntr1=adjustl(isntr1)
		do intr2=mintri2,maxtri2,6  ! The number of pole triangulation rows for pole search
			write(isntr2,'(I4)') intr2
			isntr2=adjustl(isntr2)
	        nptot=nptot+1
			open(1,file="Input/ais_inputParameters.txt") ! Write input parameter file for a-is
! Write the input parameter file:
! Basic input
			write(1,10) SB(1),"Lower bound for rotation period"
			write(1,10) SB(2),"Upper bound for rotation period"
! Advanced input
			write(1,20) intr1,"Number of triangle rows for pole in period scanning"
			write(1,20) intr2,"Number of triangle rows for pole in pole scanning "
			write(1,10) amtol(iamtol),"Downhill simplex tolerance"
			write(1,10) scper(iscper),"Resolution coefficient in period search"
			write(1,20) imaxper,"Maximum number of scanned periods"
			write(1,10) car(icar),"Minimum allowed axis ratio"
			close(1)
10 			format(F14.6,5X,A60)
20 			format(I5,5X,A60)

! AVI spin scanning:
			output=trim(outdir)//'/'//trim(setnr)//"_T"//trim(isdev)//"_"//trim(isamtol)//"_"//trim(isscper)//"_"//trim(ismaxper)//&
			       "_"//trim(iscar)//"_"//trim(isntr1)//"_"//trim(isntr2)
			call cpu_time(cpui)
			call Initial()
			call cpu_time(cpuf)

! Write output information in case wise files and a summary file

! Write output in a summary file including all the simulations in the simulation set
			open (unit=1,file='Output/ais_FitAll.txt')
			write(2,*) "Simulated dataset: ",isim,"npts",nobsi,"T range (years)=",tspan/360.
			if (isim<11) write(2,*) "Ellipsoid shape model"
			if (isim>=11) write(2,*) "Nonconvex, spacecraft/radar derived shape model"
			write(2,*) trim(output)
			write(2,*) "CPU:",cpuf-cpui
			write(2,'(8(1X,A6))') "ntr1","ntr2","car","amtol","scper","maxper","per1","per2"
			write(2,'(2(I6,1X),F6.2,1X,E8.2,1X,F6.2,1X,I4,2(1X,F6.2))') intr1,intr2,car(icar),amtol(iamtol),scper(iscper),imaxper,SB
			read(1,*) P(1:12)
			write(2,'(A8,3(1X,A10))') "Par","orig","fitted","dev"
			pas1=1
			pas2=1
			do j0=1,12
			   if(j0<4.or.(j0>4.and.j0<8)) then
				 if (j0>1.and.j0<5) then
				   write(2,'(A5,1X,I2,3(1X,F10.3),A4)') parn(j0),j0,P0(j0),P(j0),abs(P0(j0)-P(j0))," deg"
					if (isim<=10) then
						if (j0.ne.4.and.(abs(P0(j0)-P(j0))<10.or.abs(abs(P0(j0)-P(j0))-180.)<10.or.abs(abs(P0(j0)-P(j0))-360.)<10.)) then
						else
							pas1=0
						endif
					else
						if (j0.ne.4.and.(abs(P0(j0)-P(j0))<15.or.abs(abs(P0(j0)-P(j0))-180.)<15.or.abs(abs(P0(j0)-P(j0))-360.)<15.)) then
						else
							pas2=0
						endif				
					endif
				 else
					write(2,'(A5,1X,I2,3(1X,F10.3),A4)') parn(j0),j0,P0(j0),P(j0),100*abs(P0(j0)-P(j0))/P0(j0),"%"
					if (isim<=10.and.(j0==6.or.j0==7)) then
						if (abs(P0(j0)-P(j0))/P0(j0)>0.05) then
							pas1=0
						endif
					else if (isim>10.and.(j0==6.or.j0==7)) then
						if (abs(P0(j0)-P(j0))/P0(j0)>0.4) then
							pas2=0
						endif			
					endif
    			 endif
               endif
	    	end do
			if(pas1==0) then
				nfail1=nfail1+1
				print*,"Set ",idev,iamtol,iscper,imaxper,icar,intr1,intr2," Failed"
			elseif(pas2==0) then
				print*,"Set ",idev,iamtol,iscper,imaxper,icar,intr1,intr2," Failed"			
				nfail2=nfail2+1
			endif
			write(2,*) "------------------------------------------------------------------------"
			close(1)
			dper=0.5d0*SB(1)**2/(365*24*(TDT(nobsi)-TDT(1)))
			dper=scper(iscper)*dper
			nper=int((SB(2)-SB(1))/dper)+1
			if(nper>imaxper) nper=imaxper
			write(4,30) intr1,intr2,car(icar),amtol(iamtol),scper(iscper),nper,cpuf-cpui
30          format(I3,1X,I3,1X,F8.5,1X,F6.3,1X,F8.5,1X,I5,1X,F12.5)
! Write simulation-wise output files
			open (unit=1,file='Output/ais_FitAll.txt')
			open (unit=3,file=trim(adjustl(output))//'ais_FitAll.txt')
			read(1,('(13(F14.6,1X),F10.6)')) fline
			write(3,('(13(F14.6,1X),F10.6)')) fline
			close(1)
			close(3)
			open (unit=1,file='Output/ais_PoleScan.txt')
			open (unit=3,file=trim(adjustl(output))//'ais_PoleScan.txt')
			do while(1==1)
			    read(1,('(13(E20.14,1X))'),END=100) fline
			    write(3,('(13(E20.14,1X))')) fline
			end do
100			close(1)
			close(3)
			open (unit=1,file='Output/ais_PeriodScan.txt')
			open (unit=3,file=trim(adjustl(output))//'ais_PeriodScan.txt')
			do while (1==1)
			    read(1,('(13(E20.14,1X))'),END=200) fline
			    write(3,('(13(E20.14,1X))')) fline
			end do
200			close(1)
			close(3)
		end do ! Loop over pole rows ntri2 pole search
	 end do ! Loop over pole rows ntri1 period search
	end do
   end do
  end do
 end do
end do
close(2)
!print*,nfail1+nfail2,' false solutions of',nptot
print*,"========================================================"
if (nfail1<0.or.nfail2>0) then
	print*,"Test case SVT-10310 FAILED"
else
	print*,"Test case SVT-10310 PASSED"
endif
end
