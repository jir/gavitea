! Discrets_fun.f90 - a unit test suite for Discrets.f90
!
! funit generated this file from Discrets.fun

module Discrets_fun

 use Discrets

 implicit none

 logical :: noAssertFailed

 public :: test_Discrets

 private

 integer :: numTests          = 0
 integer :: numAsserts        = 0
 integer :: numAssertsTested  = 0
 integer :: numFailures       = 0



! DO NOT USE THE WORD T-E-S-T anywhere in the comments!

! The only input parameter is ntri, the number of triangle rows in a half hemisphere.
! For the general case, only the first, last and mid values of MU and PHI are checked.
! For the case where ntri=1, MU and PHI for all the 6 vertices are checked.

! CHANGE THE VALUE OF ntr1 ON LINE 18 TO CHECK ADDITIONAL INPUT VALUES.

! Global variables can be declared here
integer,parameter::tark=selected_real_kind(12,30),muphi=130000,itsize=260000
integer::ntr1,ntr2,nnod1,ntri1,nnod2,ntri2,IT1(itsize,3),IT2(itsize,3)
real(kind=tark)::MU1(muphi),PHI1(muphi),MU2(muphi),PHI2(muphi)

 contains

! Place code here that should be run before each test
  
! Place code here that should be run after each test

 subroutine TRIDS_output

  integer::ntri0,nnod0,i
  real(kind=tark),dimension(6),parameter::MU01_deg=(/0.,90.,90.,90.,90.,180./),&
                                 PHI01_deg=(/0.,0.,90.,180.,270.,0./)
  real(kind=tark),dimension(3),parameter::MU02_deg=(/0.,90.,180./),&
                                 PHI02_deg=(/0.,180.,0./)
  real(kind=tark),dimension(6)::MU11,MU10,PHI11,PHI10
  real(kind=tark),dimension(3)::MU21,MU20,PHI21,PHI20
  real(kind=tark)::rd

  rd=acos(-1.)/180.

! Case ntr1
  ntri0=8
  nnod0=6
  MU11=MU1(1:nnod1)
  PHI11=PHI1(1:nnod1)
  MU10=cos(MU01_deg*rd)
  PHI10=PHI01_deg*rd
  ! Assert_Real_Equal assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.( (ntri1 &
        +2*spacing(real(ntri1)) ) &
        .ge. &
        (ntri0) &
            .and. &
     (ntri1 &
      -2*spacing(real(ntri1)) ) &
      .le. &
       (ntri0) )) then
      print *, " *Assert_Real_Equal failed* in test TRIDS_output &
              &[Discrets.fun:55]"
      print *, "  ", "ntri0 (", &
 ntri0, &
  ") is not", &
 ntri1,&
 "within", &
  2*spacing(real(ntri1))
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Real_Equal assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.( (nnod1 &
        +2*spacing(real(nnod1)) ) &
        .ge. &
        (nnod0) &
            .and. &
     (nnod1 &
      -2*spacing(real(nnod1)) ) &
      .le. &
       (nnod0) )) then
      print *, " *Assert_Real_Equal failed* in test TRIDS_output &
              &[Discrets.fun:56]"
      print *, "  ", "nnod0 (", &
 nnod0, &
  ") is not", &
 nnod1,&
 "within", &
  2*spacing(real(nnod1))
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  do i=1,nnod1
  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((MU11(i) &
     +1e-6) &
     .ge. &
     (MU10(i)) &
             .and. &
     (MU11(i) &
     -1e-6) &
     .le. &
     (MU10(i)) )) then
      print *, " *Assert_Equal_Within failed* in test TRIDS_output &
              &[Discrets.fun:58]"
      print *, "  ", "MU10(i) (",MU10(i),") is not", &
 MU11(i),"within",1e-6
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((PHI11(i) &
     +1e-6) &
     .ge. &
     (PHI10(i)) &
             .and. &
     (PHI11(i) &
     -1e-6) &
     .le. &
     (PHI10(i)) )) then
      print *, " *Assert_Equal_Within failed* in test TRIDS_output &
              &[Discrets.fun:59]"
      print *, "  ", "PHI10(i) (",PHI10(i),") is not", &
 PHI11(i),"within",1e-6
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  end do

! Case ntr2
  ntri0=8*ntr2**2
  nnod0=4*ntr2**2+2
  MU21(1)=MU2(1)
  MU21(2)=MU2(nnod2/2+1)
  MU21(3)=MU2(nnod2)
  PHI21(1)=PHI2(1)
  PHI21(2)=PHI2(nnod2/2+1)
  PHI21(3)=PHI2(nnod2)
  MU20=cos(MU02_deg*rd)
  PHI20=PHI02_deg*rd
  ! Assert_Real_Equal assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.( (ntri2 &
        +2*spacing(real(ntri2)) ) &
        .ge. &
        (ntri0) &
            .and. &
     (ntri2 &
      -2*spacing(real(ntri2)) ) &
      .le. &
       (ntri0) )) then
      print *, " *Assert_Real_Equal failed* in test TRIDS_output &
              &[Discrets.fun:73]"
      print *, "  ", "ntri0 (", &
 ntri0, &
  ") is not", &
 ntri2,&
 "within", &
  2*spacing(real(ntri2))
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Real_Equal assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.( (nnod2 &
        +2*spacing(real(nnod2)) ) &
        .ge. &
        (nnod0) &
            .and. &
     (nnod2 &
      -2*spacing(real(nnod2)) ) &
      .le. &
       (nnod0) )) then
      print *, " *Assert_Real_Equal failed* in test TRIDS_output &
              &[Discrets.fun:74]"
      print *, "  ", "nnod0 (", &
 nnod0, &
  ") is not", &
 nnod2,&
 "within", &
  2*spacing(real(nnod2))
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  do i=1,3
  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((MU21(i) &
     +1e-6) &
     .ge. &
     (MU20(i)) &
             .and. &
     (MU21(i) &
     -1e-6) &
     .le. &
     (MU20(i)) )) then
      print *, " *Assert_Equal_Within failed* in test TRIDS_output &
              &[Discrets.fun:76]"
      print *, "  ", "MU20(i) (",MU20(i),") is not", &
 MU21(i),"within",1e-6
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((PHI21(i) &
     +1e-6) &
     .ge. &
     (PHI20(i)) &
             .and. &
     (PHI21(i) &
     -1e-6) &
     .le. &
     (PHI20(i)) )) then
      print *, " *Assert_Equal_Within failed* in test TRIDS_output &
              &[Discrets.fun:77]"
      print *, "  ", "PHI20(i) (",PHI20(i),") is not", &
 PHI21(i),"within",1e-6
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  end do

  numTests = numTests + 1

 end subroutine TRIDS_output


 subroutine funit_setup
   real::cpui,cpuf
   ntr1=1 ! Do not change this, since value 1 is treated differently than the other values!
   ntr2=100 ! THIS CAN BE ARBITRARY !
! Run the code separately for ntr1 and ntr2
   call cpu_time(cpui)
   call TRIDS(MU1,PHI1,IT1,nnod1,ntri1,ntr1)
   call cpu_time(cpuf)
   print*,'CPU time for ntri1:',cpuf-cpui
   call cpu_time(cpui)
   call TRIDS(MU2,PHI2,IT2,nnod2,ntri2,ntr2)
   call cpu_time(cpuf)
   print*,'CPU time for ntri2:',cpuf-cpui
  noAssertFailed = .true.
 end subroutine funit_setup


 subroutine funit_teardown
 end subroutine funit_teardown


 subroutine test_Discrets( nTests, nAsserts, nAssertsTested, nFailures )

  integer :: nTests
  integer :: nAsserts
  integer :: nAssertsTested
  integer :: nFailures

  continue

  call funit_setup
  call TRIDS_output
  call funit_teardown

  nTests          = numTests
  nAsserts        = numAsserts
  nAssertsTested  = numAssertsTested
  nFailures       = numFailures

 end subroutine test_Discrets

end module Discrets_fun
