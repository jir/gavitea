! HG1G2_fun.f90 - a unit test suite for HG1G2.f90
!
! funit generated this file from HG1G2.fun

module HG1G2_fun

 use HG1G2

 implicit none

 logical :: noAssertFailed

 public :: test_HG1G2

 private

 integer :: numTests          = 0
 integer :: numAsserts        = 0
 integer :: numAssertsTested  = 0
 integer :: numFailures       = 0



! DO NOT USE THE WORD T-E-S-T anywhere in the comments!

! Run this t-e-s-t as is; do not change parameter values

! Global variables can be declared here
integer,parameter::nalp=19,tark=selected_real_kind(12,30)
real(kind=tark),dimension(nalp)::phf,alp
real(kind=tark),dimension(nalp),parameter::phf0=(/1.0000000000,0.9761233907,0.9254049259,&
   0.8593859556,0.7839665802,0.7031228647,0.6198270018,0.5364333817,0.4548750387,&
   0.3767747599,0.3035121482,0.2362658359,0.1760407835,0.1236862287,0.0799075870,&
   0.0452743496,0.0202252859,0.0050718010,0.0000000114/)
real(kind=tark),dimension(0:360)::PHID,AD,PHID1,AD1,PHID2,AD2,PHID3,AD3
real(kind=tark)::dphia,dphib,dphia1,dphib1,dphic,g12(2)
integer::nd,nd1,nd2,nd3
integer,parameter::nd10=5,nd20=5,nd30=8
real(kind=tark),dimension(0:nd10),parameter::&
   PHID10=(/0.75000000000001,0.33486015890911,0.13410559027462,&
      0.051104755920422,0.021465687098915,0.0036396989035883/),&
   PHID20=(/0.92500000000000,0.62884169242961,0.31755494947326,&
      0.12716367086602,0.022373903442534,0.16505688814629E-03/),&
   AD10=(/7.5,30.0,60.0,90.0,120.0,150.0/),&
   AD20=(/7.5,30.0,60.0,90.0,120.0,150.0/)
real(kind=tark),dimension(0:nd30),parameter::&
   PHID30=(/1.0000000000000,0.83381184671447,0.57735423615992,&
      0.42144771677128,0.23174229543008,0.10348178395239,&
      0.61733473078202E-01,0.16107006299412E-01,0.00000000000000/),&
   AD30=(/0.0,0.3,1.0,2.0,4.0,8.0,12.0,20.0,30.0/)
real(kind=tark),parameter::dphia0=-1.9098593171027,dphib0=-0.091328612267489,&
   dphia10=-0.57295779513082,dphib10=-0.86573137805875E-07,dphic0=-0.10630097250969,&
   pi=4.*atan(1.)
   
 contains

! Place code here that should be run before each test
  
! Place code here that should be run after each test

 subroutine HG1G2_output

   integer::i
   real(kind=tark)::rd
   rd=4*atan(1.)/180.
! PHIINI
  ! Assert_Real_Equal assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.( (nd1 &
        +2*spacing(real(nd1)) ) &
        .ge. &
        (nd10) &
            .and. &
     (nd1 &
      -2*spacing(real(nd1)) ) &
      .le. &
       (nd10) )) then
      print *, " *Assert_Real_Equal failed* in test HG1G2_output &
              &[HG1G2.fun:117]"
      print *, "  ", "nd10 (", &
 nd10, &
  ") is not", &
 nd1,&
 "within", &
  2*spacing(real(nd1))
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Real_Equal assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.( (nd2 &
        +2*spacing(real(nd2)) ) &
        .ge. &
        (nd20) &
            .and. &
     (nd2 &
      -2*spacing(real(nd2)) ) &
      .le. &
       (nd20) )) then
      print *, " *Assert_Real_Equal failed* in test HG1G2_output &
              &[HG1G2.fun:118]"
      print *, "  ", "nd20 (", &
 nd20, &
  ") is not", &
 nd2,&
 "within", &
  2*spacing(real(nd2))
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_Real_Equal assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.( (nd3 &
        +2*spacing(real(nd3)) ) &
        .ge. &
        (nd30) &
            .and. &
     (nd3 &
      -2*spacing(real(nd3)) ) &
      .le. &
       (nd30) )) then
      print *, " *Assert_Real_Equal failed* in test HG1G2_output &
              &[HG1G2.fun:119]"
      print *, "  ", "nd30 (", &
 nd30, &
  ") is not", &
 nd3,&
 "within", &
  2*spacing(real(nd3))
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_True assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.(dot_product(AD1(0:nd1)-AD10*pi/180.,AD1(0:nd1)-AD10*pi/180.)<1.0d-7)) then
      print *, " *Assert_True failed* in test HG1G2_output &
              &[HG1G2.fun:120]"
      print *, "  ", "dot_product(AD1(0:nd1)-AD10*pi/180.,AD1(0:nd1)-AD10*pi/180.)<1.0d-7 is not true"
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_True assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.(dot_product(AD2(0:nd2)-AD20*pi/180.,AD2(0:nd1)-AD20*pi/180.)<1.0d-7)) then
      print *, " *Assert_True failed* in test HG1G2_output &
              &[HG1G2.fun:121]"
      print *, "  ", "dot_product(AD2(0:nd2)-AD20*pi/180.,AD2(0:nd1)-AD20*pi/180.)<1.0d-7 is not true"
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_True assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.(dot_product(AD3(0:nd3)-AD30*pi/180.,AD3(0:nd1)-AD30*pi/180.)<1.0d-7)) then
      print *, " *Assert_True failed* in test HG1G2_output &
              &[HG1G2.fun:122]"
      print *, "  ", "dot_product(AD3(0:nd3)-AD30*pi/180.,AD3(0:nd1)-AD30*pi/180.)<1.0d-7 is not true"
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_True assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.(dot_product(PHID1(0:nd1)-PHID10,PHID1(0:nd1)-PHID10)<1.0d-7)) then
      print *, " *Assert_True failed* in test HG1G2_output &
              &[HG1G2.fun:123]"
      print *, "  ", "dot_product(PHID1(0:nd1)-PHID10,PHID1(0:nd1)-PHID10)<1.0d-7 is not true"
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_True assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.(dot_product(PHID2(0:nd2)-PHID20,PHID2(0:nd1)-PHID20)<1.0d-7)) then
      print *, " *Assert_True failed* in test HG1G2_output &
              &[HG1G2.fun:124]"
      print *, "  ", "dot_product(PHID2(0:nd2)-PHID20,PHID2(0:nd1)-PHID20)<1.0d-7 is not true"
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
  ! Assert_True assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.(dot_product(PHID3(0:nd3)-PHID30,PHID3(0:nd1)-PHID30)<1.0d-7)) then
      print *, " *Assert_True failed* in test HG1G2_output &
              &[HG1G2.fun:125]"
      print *, "  ", "dot_product(PHID3(0:nd3)-PHID30,PHID3(0:nd1)-PHID30)<1.0d-7 is not true"
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
! PHILS
   do i=1,nalp
  ! Assert_Equal_Within assertion
  numAsserts = numAsserts + 1
  if (noAssertFailed) then
    if (.not.((phf0(i) &
     +1d-7) &
     .ge. &
     (phf(i)) &
             .and. &
     (phf0(i) &
     -1d-7) &
     .le. &
     (phf(i)) )) then
      print *, " *Assert_Equal_Within failed* in test HG1G2_output &
              &[HG1G2.fun:128]"
      print *, "  ", "phf(i) (",phf(i),") is not", &
 phf0(i),"within",1d-7
      print *, ""
      noAssertFailed = .false.
      numFailures    = numFailures + 1
    else
      numAssertsTested = numAssertsTested + 1
    endif
  endif
   end do
! LSPLINEDI
! LSPLINE
! PHIHG12

  numTests = numTests + 1

 end subroutine HG1G2_output


 subroutine funit_setup
   real(kind=tark)::cpui,cpuf,q,phihg,cpu_phiini,cpu_phils,cpu_lsplinedi,cpu_phihg,&
                    cpu_lspline
   integer::i,j,ind
   real(kind=tark),dimension(361)::XL,YL,YL2

! PHIINI (reads the scattering input parameters)
   call cpu_time(cpui)
   call phiini(PHID1,PHID2,PHID3,dphia,dphib,dphia1,dphib1,dphic,AD1,AD2,AD3,nd1,nd2,nd3)
   call cpu_time(cpuf)
   cpu_phiini=cpuf-cpui
   alp(1)=1.d-6
   do i=2,nalp-1
      alp(i)=(i-1)*10.*pi/180.
   end do
   alp(nalp)=179.999999*pi/180.
   cpu_phils=0.
   do i=1,nalp
! PHILS (phase function)
      call cpu_time(cpui)
      phf(i)=phils(alp(i))
      call cpu_time(cpuf)
      cpu_phils=cpu_phils+cpuf-cpui
   end do
! LSPLINEDI+SPLINE
   cpu_lsplinedi=0.
   cpu_lspline=0.
   cpu_phihg=0.
   do nd=3,6,1
      dphia=-5.+(nd-3.)*10.**(30.*nd/6.)
      dphib=-5.+(nd-3.)*10.**(30.*nd/6.)
      do ind=0,nd
         PHID(ind)=1.-1.*ind/nd
         AD(ind)=ind*180./nd
      end do
      call cpu_time(cpui)
      call LSPLINEDI(PHID,dphia,dphib,AD,XL,YL,YL2,nd)
      call cpu_time(cpuf)
      cpu_lsplinedi=cpu_lsplinedi+cpuf-cpui
      do i=1,nd+1
        if(isnan(XL(i))) print*,"Error XL at nd,i",nd,i
        if(isnan(YL(i))) print*,"Error YL at nd,i",nd,i
        if(isnan(YL2(i))) print*,"Error YL2 at nd,i",nd,i
      end do
! LSPLINE + SPLINT
      g12(1)=0.10
      g12(2)=0.3
      do j=1,2
         do i=1,nalp
            call cpu_time(cpui)
            q=LSPLINE(XL,YL,YL2,alp(i),nd)
            call cpu_time(cpuf)
            cpu_lspline=cpu_lspline+cpuf-cpui
           if(isnan(q)) print*,"Error Q at nd,j,i",nd,j,i
! PHIHG12 + PHIHG1G1_1/2/3 + LSPLINE + SPLINT
         if (alp(i)<=12.*pi/18.) then
              call cpu_time(cpui)
              phihg=PHIHG12(g12(j),alp(i),XL,XL*RAND(),XL*RAND(),YL,YL2,YL*RAND(),&
              YL2*RAND(),YL*RAND(),YL2*RAND(),dphia,dphib,nd,nd,nd)
              call cpu_time(cpuf)
              cpu_phihg=cpu_phihg+cpuf-cpui
              if(isnan(q)) print*,"Error Q at nd,j,i",nd,j,i
         endif
         end do
      end do
   end do
   print*,"cpu_phiini",cpu_phiini
   print*,"cpu_phils",cpu_phils/nalp
   print*,"cpu_lsplinedi",cpu_lsplinedi/4.
   print*,"cpu_lspline",cpu_lspline/(4.*2.*nalp)
   print*,"cpu_phihg",cpu_phihg/(4.*2.*nalp)
  noAssertFailed = .true.
 end subroutine funit_setup


 subroutine funit_teardown
 end subroutine funit_teardown


 subroutine test_HG1G2( nTests, nAsserts, nAssertsTested, nFailures )

  integer :: nTests
  integer :: nAsserts
  integer :: nAssertsTested
  integer :: nFailures

  continue

  call funit_setup
  call HG1G2_output
  call funit_teardown

  nTests          = numTests
  nAsserts        = numAsserts
  nAssertsTested  = numAssertsTested
  nFailures       = numFailures

 end subroutine test_HG1G2

end module HG1G2_fun
