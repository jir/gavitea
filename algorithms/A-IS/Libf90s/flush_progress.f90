
module flush_progress
public::fsync
contains
subroutine flush_()
 ! Declare the interface for POSIX fsync function
       interface
         function fsync (fd) bind(c,name="fsync")
         use iso_c_binding, only: c_int
           integer(c_int), value :: fd
           integer(c_int) :: fsync
         end function fsync
       end interface
     
       ! Variable declaration
       integer :: ret
     
       ! Opening unit 10
!       open (10,file="ais_period_progress.txt")
     
       write(10,"(A)",advance="no") "i"
     
       ! Flush and sync
       flush(10)
       ret = fsync(fnum(10))
     
       ! Handle possible error
       if (ret /= 0) stop "Error calling FSYNC"
end subroutine
end module