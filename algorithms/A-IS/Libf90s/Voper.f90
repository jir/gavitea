module voper

! VROTEU:  vector rotation using Euler angles
! VROTY:   vector rotation about the y-axis
! VROTZ:   vector rotation about the z-axis
! VPRO:    vector product
! SPRO:    scalar product

implicit none
integer,parameter,private::tark=selected_real_kind(12,30)

contains

   subroutine VROTEU(X,CA,SA)

! Vector rotation using Euler angles. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13

   real(kind=tark)::X(3),CA(3),SA(3)

   call VROTZ(X,CA(1),SA(1))
   call VROTY(X,CA(2),SA(2))
   call VROTZ(X,CA(3),SA(3))
   end subroutine VROTEU


   subroutine VROTY(X,c,s)

! Vector rotation about the y-axis. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13

   implicit none
   integer,parameter::tark=selected_real_kind(12,30)
   real(kind=tark)::X(3),c,s,q

   q   = c*X(3)+s*X(1)
   X(1)=-s*X(3)+c*X(1)
   X(3)=q
   end subroutine VROTY



   subroutine VROTZ(X,c,s)

! Vector rotation about the z-axis. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13

   implicit none
   integer,parameter::tark=selected_real_kind(12,30)
   real(kind=tark)::X(3),c,s,q

   q   = c*X(1)+s*X(2)
   X(2)=-s*X(1)+c*X(2)
   X(1)=q
   end subroutine VROTZ

   

   subroutine VPRO(XY,X,Y)

! Vector product. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13

   implicit none
   integer,parameter::tark=selected_real_kind(12,30)
   real(kind=tark)::XY(3),X(3),Y(3)

   XY(1)=X(2)*Y(3)-X(3)*Y(2)
   XY(2)=X(3)*Y(1)-X(1)*Y(3)
   XY(3)=X(1)*Y(2)-X(2)*Y(1)    
   end subroutine VPRO



   subroutine SPRO(XY,X,Y)

! Scalar product. 

! Author: Johanna Torppa (based on code by Karri Muinonen)
! Version: 2015-07-13

   implicit none
   integer,parameter::tark=selected_real_kind(12,30)
   real(kind=tark)::XY,X(3),Y(3)

   XY=X(1)*Y(1)+X(2)*Y(2)+X(3)*Y(3)    
   end subroutine SPRO     


end module voper