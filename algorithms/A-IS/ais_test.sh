
export FC=f95
export FCFLAGS='-fprofile-arcs -ftest-coverage'
cd Libf90s

# Test for test case GAVITEA-SVT_01340

echo "\n-----------------------"
echo "Running test case 01340"
echo "Unit tests for modeules Discrets and HG1G2"
echo "-----------------------\n"

rm -f Discrets_fun*
rm -f Discrets.o
rm -f Discrets.mod
funit Discrets
gcov Discrets.f90

rm -f HG1G2_fun*
rm -f HG1G2.o
rm -f HG1G2.mod
funit HG1G2
gcov HG1G2.f90

rm ../Input/inputData.txt
rm ../Input/ais_inputParameters.txt
cd ../

# Test for test case GAVITEA-SVT_01300

echo "\n-----------------------"
echo "Running test case 01300"
echo "Each simulation (i.e., test run) may take some time, of order minutes."
echo "Tests algorithm performance for 14 datasets with constant input parameters"
echo "-----------------------\n"

f95 Libf90s/flush_progress.f90 Libf90s/Discrets.f90 Libf90s/Voper.f90 Libf90s/HG1G2.f90 Libf90s/Viewgeo.f90 Libf90s/Le.f90 a_is.f90 SVT_01300.f90 -o SVT_01300.o
./SVT_01300.o

# Test for test case GAVITEA-SVT_01310

echo "\n-----------------------"
echo "Running test case 01310"
echo "Each simulation (i.e., test run) may take some time, from minutes to hours"
echo "Tests algorithm performance for 1 dataset with varying input parameters"
echo "-----------------------\n"

f95 Libf90s/flush_progress.f90 Libf90s/Discrets.f90 Libf90s/Voper.f90 Libf90s/HG1G2.f90 Libf90s/Viewgeo.f90 Libf90s/Le.f90 a_is.f90 SVT_01310.f90 -o SVT_01310.o
./SVT_01310.o
